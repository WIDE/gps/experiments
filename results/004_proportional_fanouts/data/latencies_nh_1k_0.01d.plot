
# Common headers for all the plots
set terminal pdf linewidth 5 size 6,4 font ',18'
set datafile separator ","
set pointsize 0.9
set border linewidth 0.3
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   'latencies_nh_1k_0.01d.pdf'
set key      left top reverse Left
set format y '%.0f'
set xlabel   ''
set ylabel   'Latency (#rounds)'
set xrange   [:]
set yrange   [0:10]
set xtics    -1
set ytics    -1

# Header for bars plots
set style data histogram
set style histogram cluster gap 1
set style fill solid border

plot \
	'n_1k.latencies.csv' using 2:xtic(1) linecolor rgb '#0266C8' title '1k N', \
	'h_1k_0.01d.latencies.csv' using 2:xtic(1) linecolor rgb '#B26500' title '1k_0.01d Hop: P+S', \
	'h_1k_0.01d.latencies.csv' using 3:xtic(1) linecolor rgb '#F90101' title '1k_0.01d Hop: P', \
	'h_1k_0.01d.latencies.csv' using 4:xtic(1) linecolor rgb '#01A901' title '1k_0.01d Hop: S', \
