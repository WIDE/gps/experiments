
# Common headers for all the plots
set terminal pdf linewidth 5 size 6,4 font ',18'
set datafile separator ","
set pointsize 0.9
set border linewidth 0.3
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   'dissemination_nd_100k_0.1d.pdf'
set key      left top reverse Left
set format y '%.0f%%'
set xlabel   'Time (#rounds)'
set ylabel   'CDF dissemination'
set xrange   [0:10]
set yrange   [0:100]
set xtics    1
set ytics    10

# Header for curves plots
set termoption dashed

plot \
	'n_100k_0.1d.rounds.csv' using 1:8 every ::1 with linespoints pointinterval 1 linecolor rgb '#0266C8' title '100k 0.1d N', \
	'd_100k_0.1d.rounds.csv' using 1:8 every ::1 with linespoints pointinterval 1 linecolor rgb '#B26500' title '100k 0.1d Dup: P+S', \
	'd_100k_0.1d.rounds.csv' using 1:9 every ::1 with linespoints pointinterval 1 linecolor rgb '#F90101' title '100k 0.1d Dup: P', \
	'd_100k_0.1d.rounds.csv' using 1:10 every ::1 with linespoints pointinterval 1 linecolor rgb '#01A901' title '100k 0.1d Dup: S', \
