
# Common headers for all the plots
set terminal pdf linewidth 2 size 6,4 font ',18'
set datafile separator ","
set pointsize 1
set border linewidth 0.1
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   'consistencies_gps_0.001d.pdf'
set key      right bottom reverse Left
set format y '%.3f%%'
set xlabel   'Time (rounds)'
set ylabel   'Consistency'
set xrange   [5:30]
set yrange   [30:]
set xtics    1
set ytics    


# Header for bars plots
# Variables for the box width and spaces between them
bw = 0.15
space = bw/5
set boxwidth bw

plot \
	'simple.consistencies.csv' using ($0+1.5*0*(space+bw)):6 with linespoints pointinterval 1 linecolor rgb '#000000' dashtype 1 pointtype 1 title 'Simple', \
	'simple.consistencies.csv' using ($0+1.5*0*(space+bw)):5:4:8:7:xtic(1) with candlesticks linecolor rgb '#000000' notitle whiskerbars, \
	'simple.consistencies.csv' using ($0+1.5*0*(space+bw)):6:6:6:6:xtic(1) with candlesticks linecolor rgb '#000000' dashtype 1 notitle whiskerbars, \
	'dup_0.001d.consistencies.csv' using ($0+1.5*1*(space+bw)):6 with linespoints pointinterval 1 linecolor rgb '#F90101' dashtype 2 pointtype 2 title 'GPS: d=0.001: P', \
	'dup_0.001d.consistencies.csv' using ($0+1.5*1*(space+bw)):5:4:8:7 with candlesticks linecolor rgb '#F90101' notitle whiskerbars, \
	'dup_0.001d.consistencies.csv' using ($0+1.5*1*(space+bw)):6:6:6:6 with candlesticks linecolor rgb '#F90101' dashtype 2 notitle whiskerbars, \
	'dup_0.001d.consistencies.csv' using ($0+1.5*2*(space+bw)):13 with linespoints pointinterval 1 linecolor rgb '#0266C8' dashtype 3 pointtype 4 title 'GPS: d=0.001: S', \
	'dup_0.001d.consistencies.csv' using ($0+1.5*2*(space+bw)):12:11:15:14 with candlesticks linecolor rgb '#0266C8' notitle whiskerbars, \
	'dup_0.001d.consistencies.csv' using ($0+1.5*2*(space+bw)):13:13:13:13 with candlesticks linecolor rgb '#0266C8' dashtype 3 notitle whiskerbars, \
	'dup_0.001d.consistencies.csv' using ($0+1.5*3*(space+bw)):20 with linespoints pointinterval 1 linecolor rgb '#01A901' dashtype 4 pointtype 8 title 'GPS: d=0.001: P+S', \
	'dup_0.001d.consistencies.csv' using ($0+1.5*3*(space+bw)):19:18:22:21 with candlesticks linecolor rgb '#01A901' notitle whiskerbars, \
	'dup_0.001d.consistencies.csv' using ($0+1.5*3*(space+bw)):20:20:20:20 with candlesticks linecolor rgb '#01A901' dashtype 4 notitle whiskerbars, \
