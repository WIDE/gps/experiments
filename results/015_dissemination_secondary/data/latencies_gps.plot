
# Common headers for all the plots
set terminal pdf linewidth 2 size 6,4 font ",18"
set datafile separator ","
set border linewidth 0.1
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   "latencies_gps.pdf"
set key      left top reverse Left
set format y "%.0f"
set xlabel   "Node class"
set ylabel   "Message latency (rounds)"
set xrange   [0.5:5]
set yrange   [0:10]
set xtics    
set ytics    1

# Headers for bar plots
# Variables for the box width and space between them"
set style data histograms
set style histogram cluster
set pointsize 1.5

# Variables for the box width and space between boxes/points"
bw = 0.15
space = 1
set boxwidth bw

plot \
"uniform.latencies.csv" using ($0+1.5*space*bw*0):2:xtic(1) with boxes linecolor rgb "#CCCCCC" fillstyle pattern 3 notitle, \
"uniform.latencies.csv" using ($0+1.5*space*bw*0):2:5:9 with yerrorbars linecolor rgb "#000000" pointtype 2 title "Uniform", \
"dup_0.1d.latencies.csv" using ($0+1.5*space*bw*0):2 with boxes linecolor rgb "#FD4D4D" fillstyle pattern 3 notitle, \
"dup_0.1d.latencies.csv" using ($0+1.5*space*bw*0):2:5:9 with yerrorbars linecolor rgb "#000000" pointtype 4 title "UPS: d=0.1", \
"dup_0.01d.latencies.csv" using ($0+1.5*space*bw*1):2 with boxes linecolor rgb "#4F9DEA" fillstyle pattern 3 notitle, \
"dup_0.01d.latencies.csv" using ($0+1.5*space*bw*1):2:5:9 with yerrorbars linecolor rgb "#000000" pointtype 6 title "UPS: d=0.01", \
"dup_0.001d.latencies.csv" using ($0+1.5*space*bw*2):2 with boxes linecolor rgb "#58C658" fillstyle pattern 3 notitle, \
"dup_0.001d.latencies.csv" using ($0+1.5*space*bw*2):2:5:9 with yerrorbars linecolor rgb "#000000" pointtype 8 title "UPS: d=0.001", \
