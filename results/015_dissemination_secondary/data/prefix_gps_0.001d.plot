
# Common headers for all the plots
set terminal pdf linewidth 2 size 6,4 font ",18"
set datafile separator ","
set border linewidth 0.1
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   "prefix_gps_0.001d.pdf"
set key      left top reverse Left
set format y "%.0f"
set xlabel   "Rounds"
set ylabel   "Longest prefix"
set xrange   [:]
set yrange   [0:]
set xtics    1
set ytics    

# Headers for curves plots
set pointsize 1

# Variables for the box width and space between boxes/points"
bw = 0.2
space = 0.25
set boxwidth bw

plot \
"uniform.prefix.csv" using ($0+1.5*space*bw*0):2 with linespoints pointinterval 1 linecolor rgb "#000000" dashtype 1 pointtype 2 title "Uniform", \
"uniform.prefix.csv" using ($0+1.5*space*bw*0):6:4:10:8:xtic(1) with candlesticks linecolor rgb "#000000" notitle whiskerbars, \
"uniform.prefix.csv" using ($0+1.5*space*bw*0):2:2:2:2:xtic(1) with candlesticks linecolor rgb "#000000" dashtype 1 notitle, \
"dup_0.001d.prefix.csv" using ($0+1.5*space*bw*1):2 with linespoints pointinterval 1 linecolor rgb "#FD4D4D" dashtype 2 pointtype 4 title "UPS: d=0.001: P", \
"dup_0.001d.prefix.csv" using ($0+1.5*space*bw*1):6:4:10:8 with candlesticks linecolor rgb "#FD4D4D" notitle whiskerbars, \
"dup_0.001d.prefix.csv" using ($0+1.5*space*bw*1):2:2:2:2 with candlesticks linecolor rgb "#FD4D4D" dashtype 2 notitle, \
"dup_0.001d.prefix.csv" using ($0+1.5*space*bw*2):11 with linespoints pointinterval 1 linecolor rgb "#4F9DEA" dashtype 3 pointtype 6 title "UPS: d=0.001: S", \
"dup_0.001d.prefix.csv" using ($0+1.5*space*bw*2):15:13:19:17 with candlesticks linecolor rgb "#4F9DEA" notitle whiskerbars, \
"dup_0.001d.prefix.csv" using ($0+1.5*space*bw*2):11:11:11:11 with candlesticks linecolor rgb "#4F9DEA" dashtype 3 notitle, \
"dup_0.001d.prefix.csv" using ($0+1.5*space*bw*3):20 with linespoints pointinterval 1 linecolor rgb "#58C658" dashtype 4 pointtype 8 title "UPS: d=0.001: P+S", \
"dup_0.001d.prefix.csv" using ($0+1.5*space*bw*3):24:22:28:26 with candlesticks linecolor rgb "#58C658" notitle whiskerbars, \
"dup_0.001d.prefix.csv" using ($0+1.5*space*bw*3):20:20:20:20 with candlesticks linecolor rgb "#58C658" dashtype 4 notitle, \
