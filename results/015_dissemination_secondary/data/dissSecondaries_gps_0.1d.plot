
# Common headers for all the plots
set terminal pdf linewidth 2 size 6,4 font ",18"
set datafile separator ","
set border linewidth 0.1
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   "dissSecondaries_gps_0.1d.pdf"
set key      right bottom reverse Left
set format y "%.0f%%"
set xlabel   "Rounds"
set ylabel   "CDF infected Secondary nodes"
set xrange   [4:19]
set yrange   [-15:100]
set xtics    1
set ytics    10

# Headers for curves plots
set pointsize 1

# Variables for the box width and space between boxes/points"
bw = 0.2
space = 0
set boxwidth bw

plot \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*0):2 with linespoints pointinterval 1 linecolor rgb "#000000" dashtype 1 pointtype 2 title "One curve per update", \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*1):11 with linespoints pointinterval 1 linecolor rgb "#FD4D4D" dashtype 2 pointtype 4 notitle, \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*2):20 with linespoints pointinterval 1 linecolor rgb "#4F9DEA" dashtype 3 pointtype 6 notitle, \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*3):29 with linespoints pointinterval 1 linecolor rgb "#58C658" dashtype 4 pointtype 8 notitle, \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*4):38 with linespoints pointinterval 1 linecolor rgb "#933B00" dashtype 5 pointtype 10 notitle, \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*5):47 with linespoints pointinterval 1 linecolor rgb "#3B0093" dashtype 6 pointtype 12 notitle, \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*6):56 with linespoints pointinterval 1 linecolor rgb "#000000" dashtype 1 pointtype 1 notitle, \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*7):65 with linespoints pointinterval 1 linecolor rgb "#FD4D4D" dashtype 2 pointtype 2 notitle, \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*8):74 with linespoints pointinterval 1 linecolor rgb "#4F9DEA" dashtype 3 pointtype 4 notitle, \
"dup_0.1d.dissSecondaries.csv" using ($0+1.5*space*bw*9):83 with linespoints pointinterval 1 linecolor rgb "#58C658" dashtype 4 pointtype 6 notitle, \
