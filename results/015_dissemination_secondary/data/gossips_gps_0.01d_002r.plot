
# Common headers for all the plots
set terminal pdf linewidth 2 size 6,4 font ",18"
set datafile separator ","
set pointsize 1
set border linewidth 0.1
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   "gossips_gps_0.01d_002r.pdf"
set key      right top reverse Left
set format y "%.0f%%"
set xlabel   "Number of unique messages sent"
set ylabel   "Number of nodes"
set xrange   [0.5:5]
set yrange   [1:]
set xtics    
set ytics    

# Headers for bar plots
# Variables for the box width and spaces between them"
bw = 0.2
space = 0.25
set boxwidth bw

plot \
"dup_0.01d.002r.gossips.csv" using ($0+1.5*space*bw*0):7 with points linecolor rgb "#000000" dashtype 1 pointtype 2 title "P to P", \
"dup_0.01d.002r.gossips.csv" using ($0+1.5*space*bw*0):6:4:10:8:xtic(1) with candlesticks linecolor rgb "#000000"  notitle whiskerbars, \
"dup_0.01d.002r.gossips.csv" using ($0+1.5*space*bw*0):7:7:7:7:xtic(1) with candlesticks linecolor rgb "#000000" dashtype 1 notitle, \
"dup_0.01d.002r.gossips.csv" using ($0+1.5*space*bw*1):16 with points linecolor rgb "#F90101" dashtype 2 pointtype 4 title "P to S", \
"dup_0.01d.002r.gossips.csv" using ($0+1.5*space*bw*1):15:13:19:17 with candlesticks linecolor rgb "#F90101"  notitle whiskerbars, \
"dup_0.01d.002r.gossips.csv" using ($0+1.5*space*bw*1):16:16:16:16 with candlesticks linecolor rgb "#F90101" dashtype 2 notitle, \
"dup_0.01d.002r.gossips.csv" using ($0+1.5*space*bw*2):25 with points linecolor rgb "#0266C8" dashtype 3 pointtype 8 title "S to S", \
"dup_0.01d.002r.gossips.csv" using ($0+1.5*space*bw*2):24:22:28:26 with candlesticks linecolor rgb "#0266C8"  notitle whiskerbars, \
"dup_0.01d.002r.gossips.csv" using ($0+1.5*space*bw*2):25:25:25:25 with candlesticks linecolor rgb "#0266C8" dashtype 3 notitle, \
