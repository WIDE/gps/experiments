
# Common headers for all the plots
set terminal pdf linewidth 5 size 6,4 font ',18'
set datafile separator ","
set pointsize 0.9
set border linewidth 0.3
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   'consistencies_sd_0.001d.pdf'
set key      right bottom reverse Left
set format y '%.3f%%'
set xlabel   'Time (rounds)'
set ylabel   'Consistency'
set xrange   [:]
set yrange   [:]

# Header for bars plots
set style data histogram
set style fill pattern border

# Variables for the box width and spaces between them
bw = 0.15
space = bw/5
set boxwidth bw

plot \
	'simple.consistencies.csv' using ($0+0*(space+bw)):5:4:8:7:xtic(1) with candlesticks linecolor rgb '#0266C8' title 'Simple' whiskerbars, \
	'simple.consistencies.csv' using ($0+0*(space+bw)):6:6:6:6 with candlesticks linecolor rgb '#0266C8' notitle, \
	'dup_0.001d.consistencies.csv' using ($0+1*(space+bw)):5:4:8:7 with candlesticks linecolor rgb '#B26500' title '0.001d Dup: P+S' whiskerbars, \
	'dup_0.001d.consistencies.csv' using ($0+1*(space+bw)):6:6:6:6 with candlesticks linecolor rgb '#B26500' notitle, \
	'dup_0.001d.consistencies.csv' using ($0+2*(space+bw)):12:11:15:14 with candlesticks linecolor rgb '#F90101' title '0.001d Dup: P' whiskerbars, \
	'dup_0.001d.consistencies.csv' using ($0+2*(space+bw)):13:13:13:13 with candlesticks linecolor rgb '#F90101' notitle, \
	'dup_0.001d.consistencies.csv' using ($0+3*(space+bw)):19:18:22:21 with candlesticks linecolor rgb '#01A901' title '0.001d Dup: S' whiskerbars, \
	'dup_0.001d.consistencies.csv' using ($0+3*(space+bw)):20:20:20:20 with candlesticks linecolor rgb '#01A901' notitle, \
