
# Common headers for all the plots
set terminal pdf linewidth 5 size 6,4 font ',18'
set datafile separator ","
set pointsize 0.9
set border linewidth 0.3
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   'dissemination_sd_0.01d.pdf'
set key      left top reverse Left
set format y '%.0f%%'
set xlabel   'Time (rounds)'
set ylabel   'CDF dissemination'
set xrange   [:]
set yrange   [0:100]
set xtics    1
set ytics    10

# Header for curves plots
set termoption dashed
set boxwidth 0.3

plot \
	'simple.dissemination.csv' using 1:6 every ::1 with lines linecolor rgb '#0266C8' title 'Simple', \
	'simple.dissemination.csv' using 1:5:4:8:7 with candlesticks linecolor rgb '#0266C8' notitle whiskerbars, \
	'simple.dissemination.csv' using 1:6:6:6:6 with candlesticks linecolor rgb '#0266C8' notitle, \
	'dup_0.01d.dissemination.csv' using 1:6 every ::1 with lines linecolor rgb '#B26500' title '0.01d Dup: P+S', \
	'dup_0.01d.dissemination.csv' using 1:5:4:8:7 with candlesticks linecolor rgb '#B26500' notitle whiskerbars, \
	'dup_0.01d.dissemination.csv' using 1:6:6:6:6 with candlesticks linecolor rgb '#B26500' notitle, \
	'dup_0.01d.dissemination.csv' using 1:13 every ::1 with lines linecolor rgb '#F90101' title '0.01d Dup: P', \
	'dup_0.01d.dissemination.csv' using 1:12:11:15:14 with candlesticks linecolor rgb '#F90101' notitle whiskerbars, \
	'dup_0.01d.dissemination.csv' using 1:13:13:13:13 with candlesticks linecolor rgb '#F90101' notitle, \
	'dup_0.01d.dissemination.csv' using 1:20 every ::1 with lines linecolor rgb '#01A901' title '0.01d Dup: S', \
	'dup_0.01d.dissemination.csv' using 1:19:18:22:21 with candlesticks linecolor rgb '#01A901' notitle whiskerbars, \
	'dup_0.01d.dissemination.csv' using 1:20:20:20:20 with candlesticks linecolor rgb '#01A901' notitle, \
