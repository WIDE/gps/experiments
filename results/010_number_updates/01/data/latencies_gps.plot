
# Common headers for all the plots
set terminal pdf linewidth 2 size 6,4 font ',18'
set datafile separator ","
set pointsize 1
set border linewidth 0.1
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   'latencies_gps.pdf'
set key      left top reverse Left
set format y '%.0f'
set xlabel   'Node class'
set ylabel   'Messages latency (rounds)'
set xrange   [0.5:5]
set yrange   [0:13]
set xtics    
set ytics    1


# Header for bars plots
# Variables for the box width and spaces between them
bw = 0.15
space = bw/5
set boxwidth bw

plot \
	'simple.latencies.csv' using ($0+1.5*0*(space+bw)):2 with points linecolor rgb '#000000' dashtype 1 pointtype 1 title 'Simple', \
	'simple.latencies.csv' using ($0+1.5*0*(space+bw)):5:4:8:7:xtic(1) with candlesticks linecolor rgb '#000000' notitle whiskerbars, \
	'simple.latencies.csv' using ($0+1.5*0*(space+bw)):2:2:2:2:xtic(1) with candlesticks linecolor rgb '#000000' dashtype 1 notitle whiskerbars, \
	'dup_0.1d.latencies.csv' using ($0+1.5*0*(space+bw)):2 with points linecolor rgb '#F90101' dashtype 2 pointtype 2 title 'GPS: d=0.1', \
	'dup_0.1d.latencies.csv' using ($0+1.5*0*(space+bw)):5:4:8:7:xtic(1) with candlesticks linecolor rgb '#F90101' notitle whiskerbars, \
	'dup_0.1d.latencies.csv' using ($0+1.5*0*(space+bw)):2:2:2:2:xtic(1) with candlesticks linecolor rgb '#F90101' dashtype 2 notitle whiskerbars, \
	'dup_0.01d.latencies.csv' using ($0+1.5*1*(space+bw)):2 with points linecolor rgb '#0266C8' dashtype 3 pointtype 4 title 'GPS: d=0.01', \
	'dup_0.01d.latencies.csv' using ($0+1.5*1*(space+bw)):5:4:8:7 with candlesticks linecolor rgb '#0266C8' notitle whiskerbars, \
	'dup_0.01d.latencies.csv' using ($0+1.5*1*(space+bw)):2:2:2:2 with candlesticks linecolor rgb '#0266C8' dashtype 3 notitle whiskerbars, \
	'dup_0.001d.latencies.csv' using ($0+1.5*2*(space+bw)):2 with points linecolor rgb '#01A901' dashtype 4 pointtype 8 title 'GPS: d=0.001', \
	'dup_0.001d.latencies.csv' using ($0+1.5*2*(space+bw)):5:4:8:7 with candlesticks linecolor rgb '#01A901' notitle whiskerbars, \
	'dup_0.001d.latencies.csv' using ($0+1.5*2*(space+bw)):2:2:2:2 with candlesticks linecolor rgb '#01A901' dashtype 4 notitle whiskerbars, \
