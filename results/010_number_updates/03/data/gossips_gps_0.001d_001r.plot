
# Common headers for all the plots
set terminal pdf linewidth 2 size 6,4 font ',18'
set datafile separator ","
set pointsize 1
set border linewidth 0.1
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"

# Header with variables
set output   'gossips_gps_0.001d_001r.pdf'
set key      right top reverse Left
set format y '%.3f%%'
set xlabel   'Number of unique messages sent'
set ylabel   'Number of nodes'
set xrange   [0.5:6]
set yrange   [1:]
set xtics    
set ytics    


# Header for bars plots
# Variables for the box width and spaces between them
bw = 0.15
space = bw/5
set boxwidth bw

plot \
	'dup_0.001d.001r.gossips.csv' using ($0+1.5*0*(space+bw)):6 with points linecolor rgb '#000000' dashtype 1 pointtype 1 title 'P to P', \
	'dup_0.001d.001r.gossips.csv' using ($0+1.5*0*(space+bw)):5:4:8:7:xtic(1) with candlesticks linecolor rgb '#000000' notitle whiskerbars, \
	'dup_0.001d.001r.gossips.csv' using ($0+1.5*0*(space+bw)):6:6:6:6:xtic(1) with candlesticks linecolor rgb '#000000' dashtype 1 notitle whiskerbars, \
	'dup_0.001d.001r.gossips.csv' using ($0+1.5*1*(space+bw)):13 with points linecolor rgb '#F90101' dashtype 2 pointtype 2 title 'P to S', \
	'dup_0.001d.001r.gossips.csv' using ($0+1.5*1*(space+bw)):12:11:15:14 with candlesticks linecolor rgb '#F90101' notitle whiskerbars, \
	'dup_0.001d.001r.gossips.csv' using ($0+1.5*1*(space+bw)):13:13:13:13 with candlesticks linecolor rgb '#F90101' dashtype 2 notitle whiskerbars, \
	'dup_0.001d.001r.gossips.csv' using ($0+1.5*2*(space+bw)):20 with points linecolor rgb '#0266C8' dashtype 3 pointtype 4 title 'S to S', \
	'dup_0.001d.001r.gossips.csv' using ($0+1.5*2*(space+bw)):19:18:22:21 with candlesticks linecolor rgb '#0266C8' notitle whiskerbars, \
	'dup_0.001d.001r.gossips.csv' using ($0+1.5*2*(space+bw)):20:20:20:20 with candlesticks linecolor rgb '#0266C8' dashtype 3 notitle whiskerbars, \
