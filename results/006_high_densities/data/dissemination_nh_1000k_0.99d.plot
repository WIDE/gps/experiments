
# Common headers for all the plots
set terminal pdf linewidth 5 size 6,4 font ',18'
set datafile separator ","
set pointsize 0.9
set border linewidth 0.3

# Header with variables
set output   'dissemination_nh_1000k_0.99d.pdf'
set key      left top reverse Left
set format y '%.0f%%'
set xlabel   'Time (#rounds)'
set ylabel   'CDF dissemination'
set xrange   [0:10]
set yrange   [0:100]
set xtics    1
set ytics    10

# Header for curves plots
set grid linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"
set termoption dashed

plot \
	'n_1000k.dissemination.csv' using 1:2 every ::1 with lines linecolor rgb '#0266C8' notitle, \
	'n_1000k.dissemination.csv' using 1:2:3 every ::1 with yerrorbars pointinterval 1 linecolor rgb '#0266C8' title '1000k Norm', \
	'h_1000k_0.99d.dissemination.csv' using 1:2 every ::1 with lines linecolor rgb '#B26500' notitle, \
	'h_1000k_0.99d.dissemination.csv' using 1:2:3 every ::1 with yerrorbars pointinterval 1 linecolor rgb '#B26500' title '1000k_0.99d Hop: P+S', \
	'h_1000k_0.99d.dissemination.csv' using 1:4 every ::1 with lines linecolor rgb '#F90101' notitle, \
	'h_1000k_0.99d.dissemination.csv' using 1:4:5 every ::1 with yerrorbars pointinterval 1 linecolor rgb '#F90101' title '1000k_0.99d Hop: P', \
	'h_1000k_0.99d.dissemination.csv' using 1:6 every ::1 with lines linecolor rgb '#01A901' notitle, \
	'h_1000k_0.99d.dissemination.csv' using 1:6:7 every ::1 with yerrorbars pointinterval 1 linecolor rgb '#01A901' title '1000k_0.99d Hop: S', \
