
# Common headers for all the plots
set terminal pdf linewidth 5 size 6,4 font ',18'
set datafile separator ","
set pointsize 0.9
set border linewidth 0.3

# Header with variables
set output   'latencies_nd_1000k_0.999d.pdf'
set key      left top reverse Left
set format y '%.0f'
set xlabel   ''
set ylabel   'Latency (#rounds)'
set xrange   [0:4]
set yrange   [0:10]
set xtics    -1
set ytics    -1

# Header for bars plots
set grid y linetype 1 linewidth 0.6 linecolor rgb "#bbbbbb"
set style data histogram
set style fill pattern border

# Variables for the box width and spaces between them
bw = 0.15
space = bw/5
set boxwidth bw

plot \
	'n_1000k.latencies.csv' using ($0+0*(space+bw)):2:3:xtic(1) with boxerrorbars  linecolor rgb '#0266C8' title '1000k Norm', \
	'd_1000k_0.999d.latencies.csv' using ($0+1*(space+bw)):2:3 with boxerrorbars  linecolor rgb '#B26500' title '1000k_0.999d Dup: P+S', \
	'd_1000k_0.999d.latencies.csv' using ($0+2*(space+bw)):4:5 with boxerrorbars  linecolor rgb '#F90101' title '1000k_0.999d Dup: P', \
	'd_1000k_0.999d.latencies.csv' using ($0+3*(space+bw)):6:7 with boxerrorbars  linecolor rgb '#01A901' title '1000k_0.999d Dup: S', \
