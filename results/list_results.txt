001_firsts
    - first latencies and dissemination plots for dup/normal and hop/normal
      for different densities (0.01 0.1 0.5) and different number of nodes
      (1k 10k 100k)
    - expected results for dup, and thresholds need to be fixed for hop

002_hop_thresholds_tests/equals_log_ceil_plus_one
                         equals_log_ceil_plus_two
                         equals_log_floor_plus_one
                         equals_log_floor_plus_two
                         strict_log_ceil_plus_one
                         strict_log_ceil_plus_two
                         strict_log_floor_plus_one
                         strict_log_floor_plus_two
    - try thresholds values for the pgossiphop based on the log of the number
      of primary nodes
    - equals/strict: msg.hop <= threshold or msg.hop < threshold
      ceil/floor: threshold1 = value of the log rounded up/down
      plus one/plus two: threshold2 = threshold1 + 1 / + 2
    - strict is better
      ceil and floor perform the same, ceil seems more natural
      +1/+2 doesn't make a difference, +1 is enough then, don't need +2
    - conclusion: threshold1 = ceil(log(primaries)), threshold2 = threshold1 + 1

003_one_million_nodes
    - 1M nodes with fanout of 10 and density [0.0001, 0.001, 0.01, 0.1, 0.5]
    - fanout of 10 is enough to hit 99% (99.99% in pgossipdup), but not 100%
    - pgossipdup behaves as seen with other network sizes and densities, the
      smallest density the best, and latency(S) = latency(N) + 1
    - pgossiphop also with the difference that it does not always hit 99%
    - simulations took ~2-3 minutes each

004_proportional_fanouts
    - as seen in hierarchical gossip [Kermarrec 2003], we achieve 100%
      dissemination by doing fanout = log2(nodes)
    - {nodes, fanout} = (1k, 10) (10k, 13) (100k, 17) (1000k, 20)
    - indeed it achieves 100% dissemination for 1000k nodes, compared to
      99% with a fanout of 10
    - simulations took 4-5 minutes each on a server

005_same_number_messages
    - how does pgossipdup behaves when he uses as many messages as ngossip?
    - well it doesn't really change the result, ngossip converges a bit faster
    - for normal: fanout = 11
      pgossiphop: fanout = 11
      pgossipdup: fanout = 10, d = 0.1

006_high_densities
    - to check if the formula for the latency of S in pgossipdup is right, we
      try with high densities (0.9, 0.99, 0.99) and we see that the secondary
      nodes reach 99% dissemination before the primary nodes
    - the results match the formula

007_change_rps/old_rps
               new_rps_with_rpsview
    - in the old RPS, neighbourhoods contain fanout nodes, so each gossip
      reaches the same nodes between two RPS calls
    - in the new RPS, neighbourhood contain 10*fanout nodes, and fanout nodes
      are picked from it randomly at each gossip
    - now 100% of the nodes receive at least one message, but the percentage
      of nodes receiving all the messages stay ~same
    - by default: plots contain box and whiskers, 1M nodes, pgossiphop not used

008_update_delays/{00,01,03,05,10}
    - 10 updates in total, 1 every "delay" rounds, see the impact on consistency
    - S nodes are more consistent than P nodes
      P and simple are equivalent
      the distribution of results of P is wider
      consistency slowly decreases with time (as updates are spread)
    => the higher the delay is, the more consistent the nodes are
    => the more P nodes, the less P are consistent and the more S are consistent
    - for delay = 0: ~0% consistency for one round for all nodes, it arrives
      sooner for the P nodes

009_rps_viewsize
    - see difference between a view size of 50 and of 100
    - some runs end up with 0 nodes receiving all msg, there are more with a
      view size of 50 than that of 100
    - from now on, default rps view size = 100

010_number_updates/{01,03,20}
    - 1 update every round for the first X rounds
    - the consistency plots are the ones expected (same as 10 updates), and
      the number of gossips between classes is the same from one round to
      another

011_msg_latency/{01-05,03-03}
    - time for a message to reach its target is uniform between X and Y
    - 01-05: plots are similar to previous ones, but with smoother curves and
      bigger boxes and wiskers, the evolution in the gossips plots is slower,
      the consistency is lower (60%)
    - 03-03: similar plots too, curves are not smoother but just "larger" as
      the points are more widely spread, consistency is even lower (40%)
    - 03-03: warning: latencies plot is wrong, all lat must be multiplied by 3

012_dup_send_s/{01,02,03,04,05,06,07,08,09,10}
    - send to P on 1st receipt, send to S on X receipt
    - higher X means higher latency for S
    - I thought consistency would follow a normal law, with the highest at 05
      but it's not exactly that. 04 and 05 are higher but 03 is lower than 02.
      From 07 on, consistency slowly decreases to worse than the one with 02
    - the pdf file contains the consistency for 0.1d for all values of X

013_both_scenarii/scenario{1,2}
    - scenario1: array of 10 distributed variables, iconsistency when there is
      a 1 after a 0 (e.g., [11101])
      scenario2: 1 distributed variable append only array, inconsistency when
      array of round r doesn't contain array of round r-1
    - with 10 updates from different sources, the two scenarii behave the same
    - the gossips plots between classes are similar between each round and
      between each density (higher variance with lower density)
    - from now on, only scenario 2 will be done

014_multiple_updates_per_round/02
    - instead of 1 update per round, do X per round for the first 10 rounds
    - overall the consistency is lower, and the difference between P and S is
      greater but not by a lot
    - number of gossips between classes increases as expected

015_dissemination_secondary
    - default plots with a new one for the dissemination of S nodes to explain
      why they are more consistent than P nodes (all S receive at once)
    - high variations with higher densities
    - better plot cosmetics (axis labels, space between points, ...)
    - NOTE: plots used for SRDS 2016

016_reruns_new_stats
    - reruns of 015 configurations with new statistical functions (e.g., CI)

017_blockchain_frequencies
    - uses the blockchain datasets
    - varies block creation frequencies, parameter is the directory name
    - used frequencies: 0.1 (x10) 0.01 (x100) 0.005 (x200)
