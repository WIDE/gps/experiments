#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


#####
##### Utils
#####

PLOT_PARAMS = dict(grid=True)
DIR_IN = "datasets/"

def describe(df):
    print(df.describe(percentiles=[0.05, 0.10, 0.25, 0.50, 0.75, 0.90, 0.95]))

def saveFig(filename):
    plt.savefig(filename, format="pdf", bbox_inches='tight', pad_inches=0.01)
    plt.close()
    print("Created figure {}".format(filename))


#####
##### Datasets
#####

### Node degree from power law
### deprecated, abandonned half way
# def degreePowerLaw():
    # from scipy.stats import powerlaw
    # import networkx as nx

    # n = 10000
    # fig, ax = plt.subplots(1, 1)
    # #g = nx.scale_free_graph(n, gamma=2.3)
    # seq = nx.random_powerlaw_tree_sequence(n, gamma=2.3, tries=10**4)
    # g = nx.configuration_model(seq)
    # print(len(g))
    # #nx.draw(g)
    # saveFig("tmp.pdf")


### Etherscan block size dataset
def etherscanBlockSize(fileIn):
    print("==== Parsing etherscan block size")
    df = pd.read_csv(DIR_IN + fileIn, index_col=0, parse_dates=True)
    dfsample = df["2020-01-01":"2020-12-31"]["Value"]
    describe(dfsample)


### Etherscan block time points -> PMF & plot to check normality
def etherscanBlockTime(fileIn, fileOut):
    print("==== Parsing etherscan block time")
    df = pd.read_csv(DIR_IN + fileIn, index_col=0, parse_dates=True)
    df = df.drop("UnixTimeStamp", 1)
    # df = df["Value"]
    # df = df["2020-01-01":"2020-12-31"]["Value"]
    df = df.apply(lambda x: round(1000*x)) # s -> ms
    df['Value'] = pd.to_numeric(df['Value'], downcast='integer') # force integer
    print("===== RAW")
    describe(df)
    df = df.sort_values(by="Value")
    ### Drop outliers if only use delays from 2020 which gets close to normal distrib
    if False:
        print("Dropping the two biggest values of 2020 (=outliers): {}".format(df.tail(2).values))
        df = df.drop(df.tail(2).index)
        print("===== -2 outliers")
        describe(df)
    pmf = df.value_counts(normalize=True, sort=False).sort_index().rename("PMF")
    # print(pmf)
    # cdf = pmf.cumsum().rename("CDF")
    # print(cdf)

    pmf.plot(**PLOT_PARAMS)
    saveFig(fileOut + ".pdf")

    csvOut = fileOut + ".csv"
    pmf.to_csv(csvOut, float_format="%.12f", index_label="delay-in-ms", header=["PMF"])
    print("Usable dataset exported to " + csvOut)


### Ethstats block propagation
### deprecated
def ethstatsLatency(fileIn, fileOut):
    print("==== Parsing ethstats block propagation")
    df = pd.read_csv(DIR_IN + fileIn, index_col=0)
    df.plot()
    saveFig(fileOut)


### KIT bitcoin latency CDF -> PMF
def kitBitcoinLatency(fileIn, fileOut):
    print("==== Parsing KIT latency")
    cdf = pd.read_csv(DIR_IN + fileIn, index_col=0)
    pmf = pd.DataFrame(index=cdf.index, columns=["PMF"])
    prev = 0
    for index, row in cdf.iterrows():
        pmf['PMF'][index] = row['CDF'] - prev
        prev = row['CDF']
    pmf['PMF'] = pmf['PMF'].apply(pd.to_numeric, downcast='float').fillna(0)
    print("== Original CDF vs CDF inferred from extracted PMF, check equal?")
    print(cdf['CDF'].equals(pmf['PMF'].cumsum()))
    describe(pmf)

    pmf.plot(**PLOT_PARAMS, logx=True)
    saveFig(fileOut + ".pdf")

    csvOut = fileOut + ".csv"
    pmf.to_csv(csvOut, float_format="%.12f",index_label="latency-in-ms")
    print("Usable dataset exported to " + csvOut)


### FC21 ethereum ping-pong latency points -> PMF
def kifferEthereumLatency(fileIn, fileOut):
    print("==== Parsing Kiffer ethereum latency")
    df = pd.read_json(DIR_IN + fileIn, lines=True)
    df = df.drop(["peer", "minimum"], 1)
    df['median'] = df['median'].apply(lambda x: round(1000*x)) # s -> ms
    df['median'] = pd.to_numeric(df['median'], downcast='integer')
    df = df.sort_values(by='median')
    describe(df)

    pmf = df.value_counts(normalize=True, sort=False).sort_index().rename("PMF").to_frame()
    pmf = pmf.rename_axis(index={"median": "latency-in-ms"})
    print(pmf)

    pmf.plot(**PLOT_PARAMS) #logx=True
    saveFig(fileOut + ".pdf")

    csvOut = fileOut + ".csv"
    pmf.to_csv(csvOut, float_format="%.12f")
    print("Usable dataset exported to " + csvOut)


### FC21 ethereum miner CDF -> PMF
def kifferEthereumMiner(fileIn, fileOut):
    print("==== Parsing Kiffer ethereum miner power")
    cdf = pd.read_csv(DIR_IN + fileIn, index_col=0, sep='\t')
    pmf = pd.DataFrame(index=cdf.index, columns=["PMF"])
    prev = 0
    for index, row in cdf.iterrows():
        pmf['PMF'][index] = row['all'] - prev
        prev = row['all']
        pmf['PMF'] = pmf['PMF'].apply(pd.to_numeric, downcast='float').fillna(0)
    print("== Original CDF vs CDF inferred from extracted PMF, check equal?")
    print(cdf['all'].equals(pmf['PMF'].cumsum()))
    # describe(pmf)

    pmf.plot(**PLOT_PARAMS, logx=True)
    saveFig(fileOut + ".pdf")

    csvOut = fileOut + ".csv"
    pmf["PMF"].to_csv(csvOut, float_format="%.12f") # same precision as input
    print("Usable dataset exported to " + csvOut)


### txprobe degree occurences -> PMF
def txprobeDegree(fileIn, fileOut):
    print("==== Parsing txprobe degree")
    df = pd.read_csv(DIR_IN + fileIn, index_col=0, sep='\t')
    occ = df["occurences"]
    describe(occ)
    # print(df)
    # print("mode " + occ.mode(dropna=True))
    sumOcc = occ.sum()
    pmf = df.apply(lambda x: x / sumOcc)
    pmf = pmf.rename(columns={"occurences": "PMF"})
    cdf = pmf.cumsum().rename(columns={"PMF": "CDF"})
    print(pmf.join(cdf))

    csvOut = fileOut + ".csv"
    pmf["PMF"].to_csv(csvOut, float_format="%.8f")
    print("Usable dataset exported to " + csvOut)


#####
##### Main
#####

if __name__ == "__main__":
    etherscanBlockSize("etherscan_block_size.csv")
    etherscanBlockTime("etherscan_block_time.csv", "block_delay_pmf")
    # ethstatsLatency("ethstats_block_propagation.csv", "ethstats_propagation.pdf")
    # kitBitcoinLatency("kit_latency_icmp_ping_2021-03-05.csv", "kit_latency_pmf")
    kifferEthereumLatency("fc21_kiffer_latency.jsonl", "kiffer_latency_pmf")
    kifferEthereumMiner("fc21_kiffer_ethereum_fig1a.tsv", "miners_pmf")
    txprobeDegree("fc19_txprobe_fig6a_manual.tsv", "outdegree_pmf")
