#! /usr/bin/env python3
# Launch all the simulations in a pool

import argparse
import os
import re
import subprocess

from multiprocessing import cpu_count, Pool, Value


# Read the arguments from the command line, use default value if none given
def initArgParser():
    parser = argparse.ArgumentParser(
        description = "launch all the simulations in a pool",
        formatter_class = argparse.ArgumentDefaultsHelpFormatter,
        )
    parser.add_argument(
        "cfgs",
        help    = "list of config files to execute in the pool",
        nargs   = "+",
        )
    parser.add_argument(
        "--compile",
        help    = "compile the simulator",
        action  = "store_true",
        default = False,
        )
    parser.add_argument(
        "--threads",
        help    = "number of threads to use",
        type    = int, metavar = "INT",
        default = cpu_count(),
        )
    parser.add_argument(
        "--runs",
        help    = "number of runs of simulations",
        type    = int, metavar = "INT",
        default = 25,
        )
    parser.add_argument(
        "--first",
        help    = "identifier of the first run",
        type    = int, metavar = "INT",
        default = 1,
        )
    parser.add_argument(
        "--database",
        help    = "each run will produce a database containing the outputs",
        action  = "store_true",
        default = False,
        )
    return parser.parse_args()

# Copy the root cfg to one specific for a run, and append database info in it
def createCfgRun(cfgRoot, cfgRun, db):
    # Remove the directory from the database name
    db = os.path.basename(db)

    with open(cfgRun, "w") as writer:
        # Copy the root cfg
        writer.write(open(cfgRoot, "r").read())
        # Append database name
        writer.write("""
init.db.dbUrl                       jdbc:sqlite:{}
""".format(db))

# Run a command in a subprocess and redirect the output to the log files
# command needs to be a list, i.e.: ["/bin/ls", "--all", "-l"]
def runCommand(command, logOut = None, logErr = None):
    logOutFile = None
    logErrFile = None
    try:
        if logOut != None:
            logOutFile = open(logOut, mode = "a")
        if logErr != None:
            logErrFile = open(logErr, mode = "a")

        res = subprocess.call(command, stdout = logOutFile, stderr = logErrFile)
        if res != 0:
            raise Exception("ERROR: see file {}".format(logErrFile.name))

    finally:
        if logOutFile != None:
            logOutFile.close()
        if logErrFile != None:
            logErrFile.close()

# Launch the run script with the given cfg and logfiles
def runPeersim(cfg, logOut, logErr):
    cmd = [dirBin + "/run.sh", cfg]
    runCommand(cmd, logOut, logErr)

# Print the number of simulations done
def callback(result):
    runsDone.value += 1
    doneLocal = runsDone.value
    appendedString = ""
    if isinstance(result, Exception):
        appendedString = " {}".format(result)
    print("{:03}/{:03} runs done".format(doneLocal, runsMax) + appendedString)


# Read the arguments from the command line
args = initArgParser()

# Check if all the given files exist and have the good name format (*.cfg)
for cfg in args.cfgs:
    if not os.path.isfile(cfg):
        raise Exception("File doesn't exist: " + cfg)
    if not re.match("^.*\.cfg$", cfg):
        raise Exception("File doesn't end by \".cfg\": " + cfg)

# Get the absolute path of the bin directory
dirBin = os.path.abspath(os.path.dirname(__file__))

# Compile the simulator if asked
if args.compile:
    runCommand([dirBin + "/compile.sh"])
    print("Compilation done")

# Variables to display the progress of the pool execution
runsDone = Value("i", 0)
runsMax = len(args.cfgs) * args.runs

# Create the thread pool and fill it
poule = Pool(args.threads)
for cfg in args.cfgs:
    for i in range(args.first, args.first+args.runs):
        # Filename taken from the cfg file + run number
        logOut = re.sub(".cfg$", ".out.{:03}".format(i), cfg)
        logErr = re.sub(".cfg$", ".err.{:03}".format(i), cfg)

        # If the simulations produce databases, then we need to create
        # a cfg file per run
        if args.database:
            cfgRun = re.sub(".cfg$", ".in.{:03}".format(i), cfg)
            db     = re.sub(".cfg$", ".db.{:03}".format(i), cfg)
            createCfgRun(cfg, cfgRun, db)
        else:
            cfgRun = cfg

        poule.apply_async(runPeersim, args = (cfgRun, logOut, logErr),
            callback = callback, error_callback = callback)

# Close the pool and wait for it to finish
poule.close()
poule.join()

print("Pool execution done")
