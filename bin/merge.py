#! /usr/bin/env python3
# Merge several simulation outputs and create a csv for each plot

import argparse
import functools
import io
import math
import os
import re

import numpy as np
import pandas as pd
import scipy.stats as st


# List of operations to perform on each distribution
OPERATIONS_SERIES = [
    ("mean", lambda s: s.mean()        ),
    ("std",  lambda s: s.std()         ),
    ("sem",  lambda s: s.sem()         ),
    ### Do the CI level multiplication in the graph script for genericity (CI 95, 99, etc)
    ### https://stackoverflow.com/questions/53519823/confidence-interval-in-python-dataframe/53522680#53522680
    ("cistd",lambda s: s.std() / np.sqrt(s.count())), #(1 + 0.95) *
    ("cisem",lambda s: s.sem() / np.sqrt(s.count())), #(1 + 0.95) *
    ("min",  lambda s: s.min()         ),
    ("05th", lambda s: s.quantile(0.05)),
    ("25th", lambda s: s.quantile(0.25)),
    ("50th", lambda s: s.quantile(0.50)),
    ("75th", lambda s: s.quantile(0.75)),
    ("95th", lambda s: s.quantile(0.95)),
    ("max",  lambda s: s.max()         ),
    ]
OPERATIONS_SORTED_LIST = [
    ("mean", lambda x: sum([v for v in x]) / len(x) if len(x) > 0 else np.nan),
    ("std",  lambda x: np.std(x)                    if len(x) > 0 else np.nan),
    ("sem",  lambda x: st.sem(x)                    if len(x) > 0 else np.nan),
    ("cistd",lambda x: np.std(x) / np.sqrt(len(x))  if len(x) > 0 else np.nan),
    ("cisem",lambda x: st.sem(x) / np.sqrt(len(x))  if len(x) > 0 else np.nan),
    ("min",  lambda x: x[0]                         if len(x) > 0 else np.nan),
    ("05th", lambda x: quantile(x, 0.05)            if len(x) > 0 else np.nan),
    ("25th", lambda x: quantile(x, 0.25)            if len(x) > 0 else np.nan),
    ("50th", lambda x: quantile(x, 0.50)            if len(x) > 0 else np.nan),
    ("75th", lambda x: quantile(x, 0.75)            if len(x) > 0 else np.nan),
    ("95th", lambda x: quantile(x, 0.95)            if len(x) > 0 else np.nan),
    ("max",  lambda x: x[len(x) - 1]                if len(x) > 0 else np.nan),
    ]

# Read the arguments from the command line, use default value if none given
def initArgParser():
    parser = argparse.ArgumentParser(
        description = "Merge several simulation outputs and create a csv for each plot")
    parser.add_argument(
        "inputs",
        metavar = ".out file",
        help    = "list of .out csv files to merge (ex: simulationId.out.*)",
        nargs   = "+",
        )
    return parser.parse_args()

# quantile function with ordered list (much faster than np.quantile)
# From https://stackoverflow.com/questions/2374640/how-do-i-calculate-percentiles-with-python-numpy
def quantile(N, percent, key=lambda x:x):
    """
    Find the quantile of a list of values.

    @parameter N - is a list of values. Note N MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of N.

    @return - the quantile of the values
    """
    if not N:
        return None
    k = (len(N)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(N[int(k)])
    d0 = key(N[int(f)]) * (c-k)
    d1 = key(N[int(c)]) * (k-f)
    return d0+d1

# Apply 100-v to all consistencies measures to make them inconsistency measures
def transformDataframeForInconsistencies(df):
    for column in ["consist", "consistP", "consistS"]:
        df[column] = df[column].apply(lambda x: 100.0 - x if x > 0.0 else 0.0)
    return df

# Split a .out file into three dataframes, one for each contained csv
def splitCsvIntoDataframes(filename):
    # One buffer per contained csv
    bufferRounds    = io.StringIO("")
    bufferLatencies = io.StringIO("")
    bufferGossips   = io.StringIO("")
    bufferDissSec   = io.StringIO("")

    buffer = None
    with open(filename) as file:
        for line in file:
            # Skip every matched lines, and direct towards the right buffer
            if re.match("^$", line):
                continue
            if re.match("^========== ROUNDS", line):
                buffer = bufferRounds
                continue
            if re.match("^========== LATENCIES", line):
                buffer = bufferLatencies
                continue
            if re.match("^========== GOSSIPS BETWEEN CLASSES", line):
                buffer = bufferGossips
                continue
            if re.match("^========== DISSEMINATION SECONDARIES", line):
                buffer = bufferDissSec
                continue
            # Append the line to the correct buffer
            print(line, file = buffer, end = "")

    # Repositionate the cursor of each buffer
    bufferRounds.seek(0)
    bufferLatencies.seek(0)
    bufferGossips.seek(0)
    bufferDissSec.seek(0)

    # Return one dataframe per contained csv
    return (pd.read_csv(bufferRounds),
        pd.read_csv(bufferLatencies),
        pd.read_csv(bufferGossips),
        pd.read_csv(bufferDissSec))

# Create a dataframe with rounds as rows, and different stats as columns
def fillPlotDataframe(df, dfGrouped, columsNames):
    for column, name in columsNames:
        # Dataframe contains on column per operation (mean, std, ..)
        for suffix, function in OPERATIONS_SERIES:
            agg = function(dfGrouped[column])
            agg.name = name + " " + suffix
            df = df.join(agg)
    return df

# round, P mean, P std, ..., S mean, S std, ..., P+S mean, P+S std, ...
# 0,
# 1,
# 2,
def disseminationCsv(dfRoundsGrouped, inputFilename, isPriorityGossip):
    # Column names for the dissemination dataframe
    columnNames = [("recvAll", "Uniform")]
    if isPriorityGossip:
        columnNames = [("recvAllP", "P"), ("recvAllS", "S"), ("recvAll", "P+S")]

    # Create the dataframe needed for the dissemination CDF plot
    dfDissemination = pd.DataFrame(index = dfRoundsGrouped.first().index)
    dfDissemination = fillPlotDataframe(dfDissemination, dfRoundsGrouped, columnNames)

    # Compute the output filename from the first input file and write the csv to it
    disseminationFilename = re.sub("\.out\.[0-9]+", ".dissemination.csv", inputFilename)
    dfDissemination.to_csv(disseminationFilename)
    print("Generated " + disseminationFilename)

# round, P mean, P std, ..., S mean, S std, ..., P+S mean, P+S std, ...
# 0,
# 1,
# 2,
def consistenciesCsv(dfRoundsGrouped, inputFilename, isPriorityGossip):
    # Column names for the consistencies df
    columnNames = [("consist", "Uniform")]
    if isPriorityGossip:
        columnNames = [("consistP", "P"), ("consistS", "S"), ("consist", "P+S")]

    # Create the dataframe needed for the consistencies CDF plot
    dfConsistencies = pd.DataFrame(index = dfRoundsGrouped.first().index)
    dfConsistencies = fillPlotDataframe(dfConsistencies, dfRoundsGrouped, columnNames)

    # Compute the output filename from the first input file and write the csv to it
    consistenciesFilename = re.sub("\.out\.[0-9]+", ".consistencies.csv", inputFilename)
    dfConsistencies.to_csv(consistenciesFilename)
    print("Generated " + consistenciesFilename)
    return dfConsistencies

# round, P mean, P std, ..., S mean, S std, ..., P+S mean, P+S std, ...
# 0,
# 1,
# 2,
def longestPrefixCsv(dfRoundsGrouped, inputFilename, isPriorityGossip):
    # Column names for the consistencies df
    columnNames = [("prefix", "Uniform")]
    if isPriorityGossip:
        columnNames = [("prefixP", "P"), ("prefixS", "S"), ("prefix", "P+S")]

    # Create the dataframe needed for the consistencies CDF plot
    dfPrefix = pd.DataFrame(index = dfRoundsGrouped.first().index)
    dfPrefix = fillPlotDataframe(dfPrefix, dfRoundsGrouped, columnNames)

    # Compute the output filename from the first input file and write the csv to it
    prefixFilename = re.sub("\.out\.[0-9]+", ".prefix.csv", inputFilename)
    dfPrefix.to_csv(prefixFilename)
    print("Generated " + prefixFilename)

# class, mean, std, ...
# Uniform,
# P,
# S,
# P+S,
def latenciesCsv(dfLatenciesSum, inputFilename, isPriorityGossip):
    # Row names for the latencies store and dataframe
    index = ["Uniform", "P", "S", "P+S"]

    # Initialise the latencies store
    latenciesPerClass = {}
    for classe in index:
        latenciesPerClass[classe] = []
    # Fill the store
    for (classe, latency), row in dfLatenciesSum.iterrows():
        # Doing list.extend((1,) * 10) will do the same as list.append(1) 10 times
        latenciesPerClass[classe].extend((latency,) * row.nodes)

    # Sort every list in the store
    for classe in index:
        latenciesPerClass[classe].sort()

    # Create the dataframe needed for the latencies plot
    dfLatencies = pd.DataFrame(index = index)
    for column, function in OPERATIONS_SORTED_LIST:
        # Add one column to the dataframe for each operation (mean, std, ...)
        # Do not perform the operations if the class has no latencies stored for it
        dfTmp = pd.DataFrame(
            {column: [function(latenciesPerClass[classe])
            if len(latenciesPerClass[classe]) > 0 else np.nan
            for classe in index]},
            index = index)
        dfLatencies = dfLatencies.join(dfTmp)

    # Compute the output filename from the first input file and write the csv to it
    latenciesFilename = re.sub("\.out\.[0-9]+", ".latencies.csv", inputFilename)
    dfLatencies.to_csv(latenciesFilename)
    print("Generated " + latenciesFilename)

# class, mean, std, ...
# Uniform,
# P,
# S,
# P+S,
def minConsistenciesCsv(dfRoundsList, inputFilename, isPriorityGossip):
    if len(dfRoundsList) == 0:
        return

    # Row names for the dataframe
    index = ["Uniform", "P", "S", "P+S"]

    # Column names for the consistencies store
    columnNames = [("consist", "Uniform")]
    if isPriorityGossip:
        columnNames = [("consistP", "P"), ("consistS", "S"), ("consist", "P+S")]

    # Initialise the latencies store (array per class with all the min consist per run)
    minConsistPerClass = {}
    for classe in index:
        minConsistPerClass[classe] = []

    # Fill the store with the max inconsist for each class for each run
    for dfRound in dfRoundsList:
        for column, classe in columnNames:
            minConsistPerClass[classe].append(max(dfRound.loc[:, column]))

    # Sort every list in the store
    for classe in index:
        minConsistPerClass[classe].sort()

    # Create the dataframe needed for the latencies plot
    dfminConsistencies = pd.DataFrame(index = index)
    for column, function in OPERATIONS_SORTED_LIST:
        # Add one column to the dataframe for each operation (mean, std, ...)
        # Do not perform the operations if the class has no latencies stored for it
        dfTmp = pd.DataFrame(
            {column: [function(minConsistPerClass[classe])
            if len(minConsistPerClass[classe]) > 0 else np.nan
            for classe in index]},
            index = index)
        dfminConsistencies = dfminConsistencies.join(dfTmp)

    # Compute the output filename from the first input file and write the csv to it
    minConsistenciesFilename = re.sub("\.out\.[0-9]+", ".minConsistencies.csv", inputFilename)
    dfminConsistencies.to_csv(minConsistenciesFilename)
    print("Generated " + minConsistenciesFilename)

# gossips, p2p mean, p2p std, ..., p2s mean, p2s std, ..., s2s mean, s2s std, ...
# 0,
# 1,
# 2,
def gossipsRoundsCsv(dfGossipsGrouped, inputFilename, isPriorityGossip):
    # There is no classes in uniform gossip, so skip this csv
    if not isPriorityGossip:
        return

    # Aggregate the values in the grouped dataframe
    dfGossips = pd.DataFrame(index = dfGossipsGrouped.first().index)
    dfGossips = fillPlotDataframe(dfGossips, dfGossipsGrouped, [("nodes", "")])

    dfGossipsPerRound = []
    # For each round in the dataframe, create a distinct dataframe and append
    # it to the list
    for rounde in dfGossips.index.levels[0]:
        dfGossipsOneRound = pd.DataFrame(index = dfGossips.index.levels[2])

        # For each direction (p2p, p2s, s2p, s2s) in the dataframe
        for direction in dfGossips.index.levels[1]:
            dfTmp = dfGossips.loc[rounde, :].loc[direction, :]

            # Add the direction to each column name and join it to the current
            # round dataframe
            newColumns = []
            for oldColumn in dfTmp.columns:
                newColumns.append(direction + oldColumn)
            dfTmp.columns = newColumns
            dfGossipsOneRound = dfGossipsOneRound.join(dfTmp)

        # Append the current round dataframe to the list of all rounds dataframes
        dfGossipsPerRound.append(dfGossipsOneRound)

    # Compute the output filename from the first input file and write the csv to it
    gossipsFilename = re.sub("\.out\.[0-9]+", ".{:03}r.gossips.csv", inputFilename)
    for i, df in enumerate(dfGossipsPerRound):
        filename = gossipsFilename.format(i)
        df.to_csv(filename)
    print("Generated " + gossipsFilename
        + " with {:03} in " + "{}".format(dfGossips.index.levels[0].tolist()))

# round, messageId 0 recv by S, mId 1 recv by S, mId 2 recv by S, ...
# 0,
# 1,
# 2,
def dissemSecondCsv(dfDissSecGrouped, inputFilename, isPriorityGossip):
    # There is no secondaries in uniform gossip, so skip this csv
    if not isPriorityGossip:
        return

    columnNames = []
    for column in dfDissSecGrouped.first().columns:
        columnNames.append((column, column))

    # Create the dataframe needed for the dissemination CDF plot
    dfDissSec = pd.DataFrame(index = dfDissSecGrouped.first().index)
    dfDissSec = fillPlotDataframe(dfDissSec, dfDissSecGrouped, columnNames)

    # Compute the output filename from the first input file and write the csv to it
    dissSecFilename = re.sub("\.out\.[0-9]+", ".dissSecondaries.csv", inputFilename)
    dfDissSec.to_csv(dissSecFilename)
    print("Generated " + dissSecFilename)

# Compute nb gossips per node class, for the network overhead table in the paper
def numberGossipsCsv(dfGossipsList, inputFilename):
    dfGrouped = None
    column = "nbGossips"
    for df in dfGossipsList:
        # Sum "nodes" values between all rounds
        df = df.groupby(["class2class", "gossips"]).sum()
        # Flatten index, compute weighted number of gossips
        df = df.reset_index()
        df[column] = df["gossips"] * df["nodes"]
        df = df.groupby("class2class").sum()
        # Cleanup for output
        df = df.drop(["round", "gossips", "nodes"], axis=1)
        # print(df)
        dfGrouped = pd.concat([dfGrouped, df])

    # Compute some group & variability metrics between the runs
    dfGrouped = dfGrouped.groupby(["class2class"])
    df = pd.DataFrame(index = dfGrouped.first().index)
    df = fillPlotDataframe(df, dfGrouped, [(column, "")])

    # Compute the output filename from the first input file and write the csv to it
    gossipsFilename = re.sub("\.out\.[0-9]+", ".nbGossips.csv", inputFilename)
    df.to_csv(gossipsFilename)
    print("Generated " + gossipsFilename)


# Display stats on non-zero values in inconsistencies results, used to compare blockchain results
def computeBlockchainEvalStats(dfConsistencies):
    # threshold = 1.9  # delay = 0.005
    # threshold = 0.19 # delay = 0.01
    threshold = 0    # delay = 0.05

    # Only keep values above the threshold in the mean columns
    s = dfConsistencies.filter(regex='mean$', axis=1)
    s = s[s >= threshold]
    print("Considering inconsistency values above threshold {}".format(threshold))
    print(s.describe())


if __name__ == "__main__":
    # Read the arguments from the command line
    args = initArgParser()

    # Check if all the given files exist and have the good name format (*.out.*)
    for csv in args.inputs:
        if not os.path.isfile(csv):
            raise Exception("File doesn\'t exist: " + csv)
        if not re.match("^.*\.out\.[0-9]+$", csv):
            raise Exception("File doesn\'t contain \".out.\" in its name: " + csv)

    # Concatenate all the dataframes of each output file, one dataframe per csv type
    dfRoundsList     = []
    dfGossipsList    = []
    dfLatenciesSum   = None
    dfGossipsGrouped = None
    dfDissSecGrouped = None
    for csv in args.inputs:
        print("Parsing input " + csv)
        dfRound, dfLatencies, dfGossips, dfDissSec = splitCsvIntoDataframes(csv)
        dfRound = transformDataframeForInconsistencies(dfRound)
        dfRoundsList.append(dfRound)
        dfGossipsList.append(dfGossips)
        dfLatenciesSum   = pd.concat([dfLatenciesSum, dfLatencies])
        dfGossipsGrouped = pd.concat([dfGossipsGrouped, dfGossips])
        dfDissSecGrouped = pd.concat([dfDissSecGrouped, dfDissSec])

    # Extract different columns if it's a uniform or priority gossip
    isPriorityGossip = (dfRoundsList[0]["nodesS"][0] != 0)

    # Group/aggregate outputs
    print("Grouping dataframes")
    dfRoundsGrouped = pd.concat(dfRoundsList).groupby("round")
    dfLatenciesSum = dfLatenciesSum.groupby(["class", "latency"]).aggregate(np.sum)
    dfGossipsGrouped = dfGossipsGrouped.groupby(["round", "class2class", "gossips"])
    dfDissSecGrouped = dfDissSecGrouped.groupby("round")

    # Create all the csv needed for the plots
    print("Generating file for dissemination")
    disseminationCsv(dfRoundsGrouped,  args.inputs[0], isPriorityGossip)
    print("Generating file for consistencies")
    dfConsistencies = consistenciesCsv(dfRoundsGrouped,  args.inputs[0], isPriorityGossip)
    print("Generating file for longest prefix")
    longestPrefixCsv(dfRoundsGrouped,  args.inputs[0], isPriorityGossip)
    print("Generating file for latencies")
    latenciesCsv    (dfLatenciesSum,   args.inputs[0], isPriorityGossip)
    print("Generating file for minimum consistencies")
    minConsistenciesCsv(dfRoundsList,  args.inputs[0], isPriorityGossip)
    # print("Generating file for nb of gossips/rounds")
    # gossipsRoundsCsv(dfGossipsGrouped, args.inputs[0], isPriorityGossip)
    print("Generating file for dissemination among secondary nodes")
    dissemSecondCsv (dfDissSecGrouped, args.inputs[0], isPriorityGossip)
    # print("Generating file for nb of gossip broadcasts per node class")
    # numberGossipsCsv(dfGossipsList, args.inputs[0])
    print("Printing inconsistency numbers for blockchain runs")
    computeBlockchainEvalStats(dfConsistencies)
