#! /usr/bin/env python3
# Generate all the config files needed to plot all the graphs and run them all

import argparse
import math
import os

simulationIdMaxLength = 64

# Read the arguments from the command line, use default value if none given
def initArgParser():
    parser = argparse.ArgumentParser(
        description = "Generate a config file to perform one simulation under "
        + "the given parameters",
        formatter_class = argparse.ArgumentDefaultsHelpFormatter,
        )
    parser.add_argument(
        "directory",
        help = "directory to put the generated file in",
        )
    parser.add_argument(
        "protocol",
        help = "possible values: u|uniform, d|dup|duplicate, h|hop",
        )
    parser.add_argument(
        "simulationId",
        help = "unique id to differentiate the simulations results "
        + "(1 <= length <= {})".format(simulationIdMaxLength)
        )
    parser.add_argument(
        "--database",
        help    = "each run will produce a database containing the outputs",
        action  = "store_true",
        default = False,
        )
    parser.add_argument(
        "--nodes",
        help = "number of nodes in the network",
        type = int, metavar = "INT",
        default = 1000000,
        )
    parser.add_argument(
        "--density",
        help = "fraction of primary nodes in the network",
        type = float, metavar = "FLOAT",
        default = 0.1,
        )
    parser.add_argument(
        "--endTime",
        help = "maximum number of rounds allowed before the simulation stops",
        type = int, metavar = "INT",
        default = 25,
        )
    parser.add_argument(
        "--latencyMin",
        type = int, metavar = "INT",
        default = 1,
        )
    parser.add_argument(
        "--latencyMax",
        help = "the latency of each message is uniformly taken between min and max",
        type = int, metavar = "INT",
        default = 1,
        )
    parser.add_argument(
        "--step",
        help = "number of rounds between two activations of the controls classes",
        type = int, metavar = "INT",
        default = 1,
        )
    parser.add_argument(
        "--stepRps",
        help = "number of rounds between two activations of the Random Peer Sampler",
        type = int, metavar = "INT",
        default = 10,
        )
    parser.add_argument(
        "--dbBatchSize",
        help = "buffer size for the database insertions",
        type = int, metavar = "INT",
        default = 1000000,
        )
    parser.add_argument(
        "--fanout",
        help = "number of nodes to send messages to at each retransmission",
        type = int, metavar = "INT",
        default = 10,
        )
    parser.add_argument(
        "--rpsViewSize",
        help = "number of nodes to store in the view of the Random Peer Sampling",
        type = int, metavar = "INT",
        default = 100,
        )
    parser.add_argument(
        "--scenario",
        help = "scenario 1: array [11101], scenario 2: append only string",
        type = int, metavar = "INT",
        default = 2,
        )
    parser.add_argument(
        "--sourceClass",
        help = "class of node that send an initial message, possible values: all, p (primaries), s (secondaries)",
        metavar = "STR",
        default = "all",
        )
    parser.add_argument(
        "--sources",
        help = "number of nodes that send an initial message",
        type = int, metavar = "INT",
        default = 10,
        )
    parser.add_argument(
        "--variables",
        help = "number of replicated variables",
        type = int, metavar = "INT",
        default = 10,
        )
    parser.add_argument(
        "--delayUpdates",
        help = "delay between each update",
        type = float, metavar = "FLOAT",
        default = 1.0,
        )
    parser.add_argument(
        "--alpha",
        help = "number of S nodes included in the P neighbourhoods (diversity mechanism)",
        type = int, metavar = "INT",
        default = 0,
        )
    parser.add_argument(
        "--beta",
        help = "number of P nodes included in the S neighbourhoods (diversity mechanism)",
        type = int, metavar = "INT",
        default = 0,
        )
    parser.add_argument(
        "--noTags",
        help = "pgossipdup: desactivate the tags",
        dest = "useTags",
        action = "store_false",
        default = True,
        )
    parser.add_argument(
        "--dupSendP",
        help = "pgossipdup: send to P when received dupSendP identical messages",
        type = int, metavar = "INT",
        default = 1,
        )
    parser.add_argument(
        "--dupSendS",
        help = "pgossipdup: send to S when received dupSendS identical messages",
        type = int, metavar = "INT",
        default = 2,
        )
    parser.add_argument(
        "--hopThreshold1",
        type = int, metavar = "INT",
        )
    parser.add_argument(
        "--hopThreshold2",
        help = """pgossiphop: X < hopThreshold1 < Y < hopThreshold2 < Z
        If message hop = X: the message is gossiped to P nodes
        If message hop = Y: the message is gossiped to both P&S
        If message hop = Z: the message is gossiped to S nodes
        Default values are proportional to the number of P nodes""",
        type = int, metavar = "INT",
        )
    return parser.parse_args()


# Read the arguments from the command line
args = initArgParser()

# Verify the protocol value and rename it to fill the peersim cfg file
if args.protocol in ["u", "uniform"]:
    args.protocol = "ugossip"
elif args.protocol in ["d", "dup", "duplicate"]:
    args.protocol = "pgossipdup"
elif args.protocol in ["h", "hop"]:
    args.protocol = "pgossiphop"
else:
    raise argparse.ArgumentTypeError("The given protocol has an unexpected "
        + "value: " + args.protocol)

# Verify the simulation id value (non empty string and not too long)
if len(args.simulationId) == 0 or len(args.simulationId) > simulationIdMaxLength:
    raise argparse.ArgumentTypeError("The given simulation id needs to have a "
        + "length between 1 and {}".format(simulationIdMaxLength))

# Verify the scenario value
if args.scenario not in [1, 2]:
    raise argparse.ArgumentTypeError("The given scenario has an unexpected "
        + "value: " + args.scenario)

# Set the default hop thresholds
# hopThreshold1 = ceil(log(nodes * density) / log(fanout))
# hopThreshold2 = hopThreshold1 + 1
if args.hopThreshold1 == None:
    args.hopThreshold1 = math.ceil(math.log(args.nodes * args.density, args.fanout))
if args.hopThreshold2 == None:
    args.hopThreshold2 = args.hopThreshold1 + 1

# Add 1 round to the simulation time because the stats are produced at the
# beginning of each round and not at the end (see java code)
# Add another round because the simulation finishes at endTime - 1
args.endTime += 2

# Create directory if necessary
if not os.path.isdir(args.directory):
    if os.path.exists(args.directory):
        raise Exception("File exists and is not a directory: " + args.directory)
    else:
        os.makedirs(args.directory, exist_ok = True)

# Generate the config file path and verify that it doesn't exist
pathConfig = "{}/{}.cfg".format(args.directory, args.simulationId)
if os.path.exists(pathConfig):
    raise Exception("The given simulation id has already been used, "
        + "file {} already exists".format(pathConfig))

with open(pathConfig, "w") as writer:
    # Add the parameters at the top of the file
    writer.write("""
#############
# Parameters
#############
NETWORK_SIZE     {}
DENSITY          {}
END_TIME         {}
LATENCY_MIN      {}
LATENCY_MAX      {}
STEP             {}
STEP_RPS         {}
DB_BATCH_SIZE    {}
SOURCES          {}
VARIABLES        {}
DELAY_UPDATES    {}
SCENARIO         {}

#######################
# Parameters of gossip
#######################
FANOUT           {}
RPS_VIEW_SIZE    {}
ALPHA            {}
BETA             {}
DUP_SEND_P       {}
DUP_SEND_S       {}
HOP_THRESHOLD_1  {}
HOP_THRESHOLD_2  {}
""".format(args.nodes, args.density, args.endTime, args.latencyMin, args.latencyMax,
    args.step, args.stepRps, args.dbBatchSize, args.sources, args.variables,
    args.delayUpdates, args.scenario, args.fanout, args.rpsViewSize, args.alpha,
    args.beta, args.dupSendP, args.dupSendS, args.hopThreshold1, args.hopThreshold2))

    # Add the common chunks of the file
    writer.write("""
#######################################
# Common to uniform and priority gossip
#######################################
simulation.endtime                  END_TIME
network.size                        NETWORK_SIZE

init.sch                            CDScheduler
init.sch.protocol                   {0}

init.src                            SourceSetter
init.src.protocol                   {0}
init.src.sources                    SOURCES
init.src.variables                  VARIABLES
init.src.delay                      DELAY_UPDATES
init.src.sourceClass                {1}

control.obs                         Observer
control.obs.step                    STEP
control.obs.protocol                {0}

protocol.transport                  UniformRandomTransport
protocol.transport.mindelay         LATENCY_MIN
protocol.transport.maxdelay         LATENCY_MAX

protocol.neighboursP                NeighboursLinkable
protocol.neighboursS                NeighboursLinkable
protocol.neighboursPS               NeighboursLinkable
""".format(args.protocol, args.sourceClass))

    # Add the chunk specific to uniform gossip
    if args.protocol == "ugossip":
        writer.write("""
################
# Uniform gossip
################
control.srps                        UniformRandomPeerSampler
control.srps.step                   STEP_RPS
control.srps.viewSize               RPS_VIEW_SIZE
control.srps.linkableP              neighboursP

protocol.{0}                        UniformGossip
protocol.{0}.step                   STEP
protocol.{0}.linkableP              neighboursP
protocol.{0}.transport              transport
protocol.{0}.fanout                 FANOUT
protocol.{0}.scenario               SCENARIO
""".format(args.protocol))

    # Add the chunk common to both priority gossips
    if args.protocol in ["pgossipdup", "pgossiphop"]:
        writer.write("""
##################
# Priority gossip
##################
init.prim                           PrimarySetter
init.prim.protocol                  {0}
init.prim.density                   DENSITY

control.prps                        PriorityRandomPeerSampler
control.prps.step                   STEP_RPS
control.prps.viewSize               RPS_VIEW_SIZE
control.prps.protocol               {0}
control.prps.linkableP              neighboursP
control.prps.linkableS              neighboursS
control.prps.linkablePS             neighboursPS
control.prps.alpha                  ALPHA
control.prps.beta                   BETA
""".format(args.protocol))

    # Add the chunk specific to priority gossip using duplicates
    if args.protocol == "pgossipdup":
        writer.write("""
protocol.{0}                        PriorityGossipDuplicate
protocol.{0}.step                   STEP
protocol.{0}.linkableP              neighboursP
protocol.{0}.linkableS              neighboursS
protocol.{0}.transport              transport
protocol.{0}.fanout                 FANOUT
protocol.{0}.scenario               SCENARIO
protocol.{0}.tagsActivated          {1}
protocol.{0}.dupSendP               DUP_SEND_P
protocol.{0}.dupSendS               DUP_SEND_S
""".format(args.protocol, "{}".format(args.useTags).lower()))

    # Add the chunk specific to priority gossip using hops
    if args.protocol == "pgossiphop":
        writer.write("""
protocol.{0}                        PriorityGossipHop
protocol.{0}.step                   STEP
protocol.{0}.linkableP              neighboursP
protocol.{0}.linkableS              neighboursS
protocol.{0}.linkablePS             neighboursPS
protocol.{0}.transport              transport
protocol.{0}.fanout                 FANOUT
protocol.{0}.scenario               SCENARIO
protocol.{0}.hopThreshold1          HOP_THRESHOLD_1
protocol.{0}.hopThreshold2          HOP_THRESHOLD_2
""".format(args.protocol))

    # Add the chunck specific to the database (if asked in the options)
    if args.database:
        writer.write("""
###########
# Database
###########
init.db                             DatabaseHandler
init.db.batchSize                   DB_BATCH_SIZE
# init.db.dbUrl                     generatedByRunAllScript
""")
        print("/!\\ Remember to add --database when calling 02_run_all.py")

print("Generated config file: {}".format(pathConfig))
