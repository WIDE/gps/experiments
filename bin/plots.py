#! /usr/bin/env python3
# Create the .plot and .pdf of all the graphs from the csv contained in the
# given directory

import argparse
import math
import os
import sys


# Read the arguments from the command line, use default value if none given
def initArgParser():
    parser = argparse.ArgumentParser(
        description = "Create the .plot and .pdf of all the graphs from the csv"
        + " contained in the given directory",
        formatter_class = argparse.ArgumentDefaultsHelpFormatter,
        )
    parser.add_argument(
        "directory",
        help = "directory to put the generated file in",
        )
    return parser.parse_args()


# Read the arguments from the command line
args = initArgParser()

# Change to the given directory, if it exists, to find the rest of the paths
if not os.path.isdir(args.directory):
    raise Exception("File is not a directory: " + args.directory)
os.chdir(args.directory)

# Titles suffixes for priority gossip and titles for the gossips plots
titlesPrioritySuffixes = [": P", ": S", ": P+S"]
titlesGossipsClass = ["P to P", "P to S", "S to S"]

# Map filename -> [title, isPriority]
mapFilenameTitlePriority = {}

class ClassIncrementor:
    def __init__(self, template, values):
        self.index = -1
        self.template = template
        self.values = values
    def next(self):
        self.index = (self.index + 1) % len(self.values)
        out = self.current()
        return out
    def current(self):
        return self.template.format(self.values[self.index - 1])

colorsCurves = ClassIncrementor("linecolor rgb \"{}\"", ["#000000", "#FD4D4D", "#4F9DEA", "#58C658", "#933B00", "#3B0093"])
colorsBars = ClassIncrementor("linecolor rgb \"{}\"", ["#CCCCCC", "#FD4D4D", "#4F9DEA", "#58C658"])
dashTypes = ClassIncrementor("dashtype {}", [1, 2, 3, 4, 5, 6])
pointTypes = ClassIncrementor("pointtype {}", [1, 2, 4, 6, 8, 10, 12])

def producePlot(userArgs):
    # Default values for args
    args = {
        "withWhiskerBars" : True,
        "plotType"        : "",
        "inputExtension"  : "",
        "output"          : "graph",
        "keyPlace"        : "left top reverse Left",
        "files"           : [],
        "titles"          : {},
        "colors"          : None,
        "colorStart"      : 0,
        "dashTypes"       : dashTypes,
        "dashTypesStart"  : 0,
        "pointTypes"      : pointTypes,
        "pointTypeStart"  : 1,
        "xrange"          : "[:]",
        "yrange"          : "[:]",
        "xlabel"          : "",
        "ylabel"          : "",
        "xtics"           : "",
        "ytics"           : "",
        "yFormat"         : "",
        "wildcard"        : "",
    }
    # Overwrite default arguments
    args.update(userArgs)

    # If one of the file doesn"t exist, abort the production of this plot
    for file in args["files"]:
        filename = file + args["inputExtension"]
        if not os.path.isfile(filename):
            print("Missing file: \"{}\"".format(filename), file = sys.stderr)
            print("Abort plot:   \"{}.plot\"".format(args["output"]), file = sys.stderr)
            print("", file = sys.stderr)
            return

    # Pick the color and reset the counters
    if args["plotType"] == "curves":
        args["colors"] = colorsCurves
    if args["plotType"] == "bars":
        args["colors"] = colorsBars
    args["colors"].index = args["colorStart"]
    args["dashTypes"].index = args["dashTypesStart"]
    args["pointTypes"].index = args["pointTypeStart"]

    # Overwrite default titles
    titles = {}
    for k, v in mapFilenameTitlePriority.items():
        titles[k] = v[0]
    for k, v in args["titles"].items():
        titles[k] = v

    # Add the common header to the.plot file
    with open(args["output"] + ".plot", "w") as writer:
        writer.write(
"""
# Common headers for all the plots
set terminal pdf linewidth 2 size 6,4 font \",18\"
set datafile separator \",\"
set border linewidth 0.1
set grid linetype 1 linewidth 0.6 linecolor rgb \"#bbbbbb\"
""")

        writer.write(
"""
# Header with variables
set output   \"{}.pdf\"
set key      {}
set format y \"{}\"
set xlabel   \"{}\"
set ylabel   \"{}\"
set xrange   {}
set yrange   {}
set xtics    {}
set ytics    {}
"""         .format(args["output"], args["keyPlace"], args["yFormat"],
            args["xlabel"], args["ylabel"], args["xrange"], args["yrange"],
            args["xtics"], args["ytics"]))

        writer.write(args["wildcard"])

    with open(args["output"] + ".plot", "a") as writer:
        # Headers for curves plots
        if args["plotType"] == "curves":
            writer.write(
"""
# Headers for curves plots
set pointsize 1
""")

        # Headers for bar plots
        if args["plotType"] == "bars":
            writer.write(
"""
# Headers for bar plots
# Variables for the box width and space between them"
set style data histograms
set style histogram cluster
set pointsize 1.5
""")

        # Set the boxwidth and space between them, different depending on the plot
        boxwidth = 0.2
        space = 0.25
        if args["plotType"] == "bars":
            boxwidth = 0.15
            space = 1
        if args["plot"] == "dissSecondaries":
            space = 0

        writer.write(
"""
# Variables for the box width and space between boxes/points"
bw = {}
space = {}
set boxwidth bw

plot \\
"""         .format(boxwidth, space))

        # End of header, start the plot lines
        plotLineCounter = 0

        # Add xtic(1) only for the first plot line
        xtic = ":xtic(1)"

        for file in args["files"]:
            # Different ranges for the bars/uniform gossip and the other plots
            columnRange = None
            if args["plotType"] == "bars" or not mapFilenameTitlePriority[file][1]:
                columnRange = [2]
            elif args["plot"] == "dissSecondaries":
                # 10 groups of columns, first one at 2, last one at 2+9*(10-1) = 83
                columnRange = list(range(2, 2+9*(10-1)+1, 9))
            else:
                # 3 groups of columns, first one at 2, last one at 2+9*(3-1) = 20
                columnRange = list(range(2, 2+9*(3-1)+1, 9))

            for i, colMean in enumerate(columnRange):
                colStd    = colMean + 1
                colMin    = colMean + 2
                col5th    = colMean + 3
                colQ1     = colMean + 4
                colMedian = colMean + 5
                colQ3     = colMean + 6
                col95th   = colMean + 7
                colMax    = colMean + 8

                # Choose the title
                title = None
                if args["plot"] == "gossips":
                    # In gossips plot, title are in [PtoP, PtoS, StoS]
                    title = "title \"{}\"".format(titlesGossipsClass[plotLineCounter])
                elif args["plot"] == "dissSecondaries":
                    # Too many curves in dissSecondaries, print only one generic title
                    title = "notitle"
                    if plotLineCounter == 0:
                        title = "title \"One curve per update\""
                else:
                    # In bars plot, title is just the density
                    title = "title \"{}".format(titles[file])
                    if not args["plotType"] == "bars" and mapFilenameTitlePriority[file][1]:
                        # If plot round based and priority gossip, add P/S/P+S to the title
                        title += titlesPrioritySuffixes[i]
                    title += "\""

                # Use the mean in every plot
                colMain = colMean

                if args["plotType"] == "curves":
                    # Curve between each points
                    writer.write(
"""\"{}{}\" using ($0+1.5*space*bw*{}):{} with linespoints pointinterval {} {} {} {} {}, \\
"""                     .format(file, args["inputExtension"], plotLineCounter,
                            colMain, args["xtics"], args["colors"].next(),
                            args["dashTypes"].next(), args["pointTypes"].next(), title))

                    if args["withWhiskerBars"]:
                        # Box and whisker bars with min, q1, q3, max
                        writer.write(
"""\"{}{}\" using ($0+1.5*space*bw*{}):{}:{}:{}:{}{} with candlesticks {} notitle whiskerbars, \\
"""                         .format(file, args["inputExtension"], plotLineCounter,
                                colQ1, colMin, colMax, colQ3, xtic,
                                args["colors"].current()))

                        # Small box just for the main column (= horizontal dash)
                        writer.write(
"""\"{}{}\" using ($0+1.5*space*bw*{}):{}:{}:{}:{}{} with candlesticks {} {} notitle, \\
"""                         .format(file, args["inputExtension"], plotLineCounter,
                                colMain, colMain, colMain, colMain, xtic,
                                args["colors"].current(), args["dashTypes"].current()))

                if args["plotType"] == "bars":
                    # Boxes of different colors, with different points for the mean
                    writer.write(
"""\"{}{}\" using ($0+1.5*space*bw*{}):{}{} with boxes {} fillstyle pattern 3 notitle, \\
"""                     .format(file, args["inputExtension"],
                            plotLineCounter, colMain, xtic, args["colors"].next()))

                    # Add the error bars if asked, points otherwise
                    if args["withWhiskerBars"]:
                        writer.write(
"""\"{}{}\" using ($0+1.5*space*bw*{}):{}:{}:{} with yerrorbars linecolor rgb "#000000" {} {}, \\
"""                         .format(file, args["inputExtension"],
                                plotLineCounter, colMain, col5th, col95th,
                                args["pointTypes"].next(), title))
                    else:
                        writer.write(
"""\"{}{}\" using ($0+1.5*space*bw*{}):{} with points linecolor rgb "#000000" {} {}, \\
"""                         .format(file, args["inputExtension"],
                                plotLineCounter, colMain, args["colors"].next(),
                                args["pointTypes"].next(), title))

                # Increment plot counter to have a shift, stop printing xtic
                plotLineCounter += 1
                xtic = ""

                # Reset plot counter if it's the uniform gossip file so that
                # the columns [P, S, P+S] are not shifted in bar plots
                if args["plotType"] == "bars" and not mapFilenameTitlePriority[file][1]:
                    plotLineCounter = 0

    callGNuplot(args["output"])


# Launch a gnuplot process with the generated .plot to create the pdf
def callGNuplot(filename):
    os.system("""
gnuplot < {1}.plot
if [[ $? -eq 0 ]]; then
    echo Plot ready: {0}/{1}.pdf
else
    # Gnuplot creates empty pdf file even if it fails
    rm {1}.pdf
    echo Plot failed: {0}/{1}.pdf
fi
""".format(args.directory, filename))


# Common args between plots of the same type
commonArgs = {
    "dissemination" : {
        "plot"           : "dissemination",
        "plotType"       : "curves",
        "inputExtension" : ".dissemination.csv",
        "xlabel"         : "Rounds",
        "ylabel"         : "CDF fully infected nodes",
        "yrange"         : "[0:100]",
        "xrange"         : "[10:20]",
        "xtics"          : 1,
        "ytics"          : 10,
        "yFormat"        : "%.0f%%",
    },

    "consistencies" : {
        "plot"           : "consistencies",
        "plotType"       : "curves",
        "inputExtension" : ".consistencies.csv",
        "xlabel"         : "Rounds",
        "ylabel"         : "Percentage of inconsistent nodes",
        "xrange"         : "[:20]",
        "yrange"         : "[0:8]",
        "yFormat"        : "%.0f%%",
        "xtics"          : 1,
        "keyPlace"       : "left top reverse Left",
    },

    "prefix" : {
        "plot"           : "prefix",
        "plotType"       : "curves",
        "inputExtension" : ".prefix.csv",
        "xlabel"         : "Rounds",
        "ylabel"         : "Longest prefix",
        "xrange"         : "[:]",
        "yrange"         : "[0:]",
        "xtics"          : 1,
        "yFormat"        : "%.0f",
    },

    "latencies" : {
        "plot"           : "latencies",
        "plotType"       : "bars",
        "inputExtension" : ".latencies.csv",
        "xlabel"         : "Node class",
        "ylabel"         : "Message latency (rounds)",
        "xrange"         : "[0.5:5]",
        "yrange"         : "[0:10]",
        "ytics"          : 1,
        "yFormat"        : "%.0f",
    },

    "minConsistencies" : {
        "plot"           : "minConsistencies",
        "plotType"       : "bars",
        "inputExtension" : ".minConsistencies.csv",
        "xlabel"         : "Node class",
        "ylabel"         : "Maximum percentage of inconsistent nodes",
        "xrange"         : "[0.5:5]",
        "yrange"         : "[0:10]",
        "ytics"          : 1,
        "yFormat"        : "%.0f%%",
    },

    "gossips" : {
        "plot"           : "gossips",
        "plotType"       : "bars",
        "inputExtension" : ".gossips.csv",
        "xlabel"         : "Number of unique messages sent",
        "ylabel"         : "Percentage of nodes",
        "xrange"         : "[0.5:5]", # TODO: find the end of the range?
        "yrange"         : "[1:]",
        "yFormat"        : "%.0f%%",
        "keyPlace"       : "right top reverse Left",
        # "wildcard"       : "set logscale y"
    },

    "dissSecondaries" : {
        "plot"           : "dissSecondaries",
        "plotType"       : "curves",
        "inputExtension" : ".dissSecondaries.csv",
        "xlabel"         : "Rounds",
        "ylabel"         : "CDF infected Secondary nodes",
        "yrange"         : "[-15:100]",
        "xrange"         : "[4:19]",
        "xtics"          : 1,
        "ytics"          : 10,
        "yFormat"        : "%.0f%%",
        "keyPlace"       : "right bottom reverse Left",
        "withWhiskerBars": False,
    },
}


densities = [0.1, 0.01, 0.001]
roundsGossips = []
roundsGossips.extend(range(1, 50))

# Give a title to each file and store if it's priority gossip
mapFilenameTitlePriority["uniform"] = ["Uniform", False]
for density in densities:
    mapFilenameTitlePriority["dup_{}d".format(density)] = ["UPS: d={}".format(density), True]

for density in densities:
    # for plot in ["dissemination", "consistencies", "prefix"]:
    for plot in ["dissemination", "consistencies"]:
        producePlot({**commonArgs[plot], **{
            "output" : "{}_gps_{}d".format(plot, density),
            "files" : ["uniform", "dup_{}d".format(density)],
            }})

    for plot in ["dissSecondaries"]:
        producePlot({**commonArgs[plot], **{
            "output" : "{}_gps_{}d".format(plot, density),
            "files" : ["dup_{}d".format(density)],
            }})

    # for plot in ["gossips"]:
    #     for ronde in roundsGossips:
    #         ronde = "{:03}".format(ronde)
    #         file = "dup_{}d.{}r".format(density, ronde)

    #         # Skip non existing files
    #         if not os.path.isfile(file + commonArgs[plot]["inputExtension"]):
    #             continue

    #         mapFilenameTitlePriority[file] = ["UPS: d={}".format(density), True]

    #         producePlot({**commonArgs[plot], **{
    #             "output" : "{}_gps_{}d_{}r".format(plot, density, ronde),
    #             "files" : [file],
    #             }})

for plot in ["latencies", "minConsistencies"]:
    # Compare uniform vs all the densities versions
    files = ["uniform"]
    for density in densities:
        files.append("dup_{}d".format(density))

    producePlot({**commonArgs[plot], **{
        "output" : "{}_gps".format(plot),
        "files" : files,
        }})
