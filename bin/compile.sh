#! /bin/bash
# Compile the sources

# Find the project directory absolute path
cd "$(dirname $0)"/..
dirProject="${PWD}"
cd "${OLDPWD}"

# Add the simulator directory to the classpath
export CLASSPATH="${CLASSPATH}:${dirProject}/src"

# Add the necessary libraries to the classpath
for jar in "${dirProject}"/lib/*.jar; do
    echo "${CLASSPATH}" | grep -q "${jar}"
    [[ $? -ne 0 ]] && export CLASSPATH="${CLASSPATH}:${jar}"
done

javac --release 8 $(find "${dirProject}"/src -name '*.java')
