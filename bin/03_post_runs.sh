#! /bin/bash
# Once the runs are over, makes everything necessary to produce the plots

function usage() {
    message="\
Usage: $(basename $0) <directory> [options...]
Run all the post-runs scripts to generate the plots

Parameters:
    <directory>
        directory containing the results

Script options:
    -h, --help
        display this message
"

    echo "${message}" >&2
    exit 2
}

# Display the error message and exits the script
function error() {
    [[ -n "$1" ]] && echo "$1" >&2
    exit 2
}


# First check if only the help is asked
case "$1" in
    -h|--help|help|"") usage;;
esac

# Verify the number of arguments
if [[ $# -lt 1 ]]; then
    error "Missing at least one argument out of: directory"
fi

# Find the absolute path of the scripts
cd "$(dirname $0)"
scriptMerge="${PWD}/merge.py"
scriptPlots="${PWD}/plots.py"
cd "${OLDPWD}"

# Assign the firsts arguments
pathDir="$1"

# Verify that the directory exists
if [[ ! -d "${pathDir}" ]]; then
    error "The given directory needs to be an existing directory: ${pathDir}"
fi

# For every config file, merge the outputs and create all the csv for the plots
for cfg in "${pathDir}"/*.cfg; do
    simulationId=$(basename "${cfg}" .cfg)
    "${scriptMerge}" "${pathDir}/${simulationId}".out.*
done

# Create all the plots
"${scriptPlots}" "${pathDir}"

# Clean the directory by only letting the pdfs in it, move the rest to subdirs
# Output of the runs (*.out.*, *.err.*) -> raw_data
# Everything else but the .pdf (.cfg, .csv, .plot) -> data
pathData="${pathDir}/data"
pathRawData="${pathDir}/raw_data"
mkdir -p "${pathData}"   || error "Could not create ${pathData}"
mkdir -p "${pathRawData}"  || error "Could not create ${pathRawData}"

mv "${pathDir}"/*.{cfg,csv,plot} "${pathData}"/
mv "${pathDir}"/*.{out,err}.*    "${pathRawData}"/

echo "=="
echo "*.cfg, *.csv, *.plot moved to ${pathData}"
echo "*.out.*, *.err.* moved to ${pathRawData}"
