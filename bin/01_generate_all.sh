#! /bin/bash
# Generate all the config files needed to plot all the graphs

function usage() {
    message="\
Usage: $(basename $0) <directory> [options...]
Generate all the config files needed to plot all the graphs

Parameters:
    <directory>
        path to the directory that will contain the results (created if needed)

Script options:
    -h, --help
        display this message

    All the options from the script that generates config files
    All the given options are applied to all the generated config files
"

    echo "${message}" >&2
    exit 2
}

# Display the error message, a generic message and exits the script
function error() {
    [[ -n "$1" ]] && echo "$1" >&2
    exit 2
}


# First check if only the help is asked
case "$1" in
    -h|--help|help|"") usage;;
esac

# Verify the number of arguments
if [[ $# -lt 1 ]]; then
    error "Missing at least one argument out of: directory"
fi

# Find the absolute path of the script that generates cfg
cd "$(dirname $0)"
scriptGenerateCfg="${PWD}/generate_cfg.py"
cd "${OLDPWD}"

# Assign the firsts arguments
pathDir="$1"
shift
commonOptions="--scenario 2 --variables 1 --sources 10 $@"

# Generate all the cfg
"${scriptGenerateCfg}" "${pathDir}" "uniform" "uniform" ${commonOptions}

for density in 0.1 0.01 0.001; do
    "${scriptGenerateCfg}" "${pathDir}" "duplicate" "dup_${density}d" \
        --density "${density}" ${commonOptions}
done
