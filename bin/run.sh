#! /bin/bash
# Run a simulation with the given configuration file

# Verify the number of arguments
if [[ $# -lt 1 ]]; then
    echo "Missing argument: configuration file" >&2
    exit 2
fi

# Find the project directory absolute path
cd "$(dirname $0)"/..
dirProject="${PWD}"
cd "${OLDPWD}"

# Switch to the directory containing the config file
cd "$(dirname $1)"
cfgFile="$(basename $1)"

# Construct classpath from libraries and sources
classPath=$(find -L "${dirProject}/lib/" -name "*.jar" | tr [:space:] :)
classPath="${dirProject}/src:${classPath}"

# Heapsize of 60G max and GC single threaded so that a run takes only 1 thread
jvm="-Xms1G -Xmx60G -XX:+UseSerialGC"

#prof="-Xprof"
#prof="-agentlib:hprof=file=cpu.txt,cpu=samples,depth=200,interval=10"
#prof="-agentlib:hprof=file=heap.txt,heap=sites,depth=200"

time java ${prof} ${jvm} -classpath "${classPath}" peersim.Simulator "${cfgFile}"
