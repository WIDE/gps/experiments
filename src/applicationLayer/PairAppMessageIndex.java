package applicationLayer;

// Used between the network layer (gossip abstract) and the application
// layer (UC) to forward updates to the right data in the data array
public class PairAppMessageIndex {
    private int index;
    private Object appMsg;


    public PairAppMessageIndex(int index, Object appMsg) {
        this.index = index;
        this.appMsg = appMsg;
    }

    /*
     * Getters & setters
     */

    public int getIndex() {
        return index;
    }

    public Object getAppMessage() {
        return appMsg;
    }

}
