package applicationLayer;

import java.util.ArrayList;
import java.util.List;

public class DataType {
    private static int scenario = UCAbstract.SCENARIO_NONE;

    // T: S x U -> S
    public static Object transition(Object state, Update update) {
        switch (scenario) {
        case UCAbstract.SCENARIO_1:
            // Simply return the update value
            return update.getValue();

        case UCAbstract.SCENARIO_2:
            // Append the update to the current state
            if (!(state instanceof List)) {
                throw new RuntimeException("Error: DataType.transition():"
                        + "state has the wrong type");
            }
            @SuppressWarnings("unchecked")
            List<Object> list = new ArrayList<>((List<Object>) state);
            list.add(update.getValue());
            return list;

        case UCAbstract.SCENARIO_NONE:
        default:
            throw new RuntimeException("Error: DataType.transition():"
                    + "the scenario is unset/unknown");
        }
    }

    // G: S x Qi -> Qo
    public static Object generation(Object state, Query input) {
        return state;
    }

    /*
     * Getters & setters
     */

    public static int getScenario() {
        return scenario;
    }

    public static void setScenario(int scenario) {
        DataType.scenario = scenario;
    }

}
