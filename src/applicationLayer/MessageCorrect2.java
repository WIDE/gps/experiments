package applicationLayer;

// Used in UCk (algo 3)
public class MessageCorrect2 extends MessageCorrect {
    private long vtime;

    public MessageCorrect2(long[] clocks, long vtime, long source, Object state) {
        super(clocks, source, state);
        this.vtime = vtime;
    }

    public long getVtime() {
        return vtime;
    }

}
