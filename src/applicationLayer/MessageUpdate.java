package applicationLayer;

// Used in UCInfinity and UCZero (algos 1 and 2) as message1
public class MessageUpdate implements Comparable<MessageUpdate> {
    private long clock;
    private long source;
    private int sourceInt;
    private Update update;


    public MessageUpdate(long clock, long source, Update update) {
        this.clock = clock;
        this.source = source;
        this.sourceInt = (int) source;
        this.update = update;
    }

    /*
     * Implementation of interfaces
     */

    // Sorted on (clock, source)
    @Override
    public int compareTo(MessageUpdate o) {
        // First order on clock
        if (this.clock < o.clock) return -1;
        if (this.clock > o.clock) return 1;

        // Then order on source
        if (this.source < o.source) return -1;
        if (this.source > o.source) return 1;

        // TODO: is it normal that it happens?
        // Clocks and source are equals, this shouldn't happen
        //        System.err.println("Warning: " + this.getClass().getName()
        //                + ".compareTo(): two items are identical");
        return 0;
    }

    /*
     * Getters & setters
     */

    public long getClock() {
        return clock;
    }

    public long getSource() {
        return source;
    }

    public int getSourceInt() {
        return sourceInt;
    }

    public Update getUpdate() {
        return update;
    }

}
