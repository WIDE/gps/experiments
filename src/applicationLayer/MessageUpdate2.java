package applicationLayer;

// Used in UCk (algo 3)
public class MessageUpdate2 extends MessageUpdate {
    private long vtime;

    public MessageUpdate2(long vtime, long clock, long source, Update update) {
        super(clock, source, update);
        this.vtime = vtime;
    }

    public long getVtime() {
        return vtime;
    }

}
