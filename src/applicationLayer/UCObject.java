package applicationLayer;

public interface UCObject {
    void update(Update update);
    Object query(Query input);
    void onReceive(Object msg);
}
