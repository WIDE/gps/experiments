package applicationLayer;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import gossip.GossipAbstract;
import peersim.core.Network;

// Algorithm 2: UC[0], algorithm from Matthieu Perrin 2015-04-14
public class UCZero extends UCAbstract {
    private Object state;
    private long[] clocks;
    private long leader;
    private SortedSet<MessageUpdate> pendings;


    public UCZero(long id, int dataIndex, GossipAbstract network, int scenario,
            Object initialState) {
        super(id, dataIndex, network, scenario, initialState);

        this.state = initialState;

        // clocks <- [0, .., 0]
        this.clocks = new long[Network.size()];
        for (int i = 0; i < Network.size(); i++) {
            this.clocks[i] = 0;
        }
        this.leader = getSelf();
        this.pendings = new TreeSet<>();
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public void update(Update update) {
        clocks[getSelfInt()]++;
        broadcast(new MessageUpdate(clocks[getSelfInt()], getSelf(), update));
    }

    @Override
    public Object query(Query input) {
        return DataType.generation(state, input);
    }

    @Override
    public void onReceive(Object msg) {
        if (msg instanceof MessageUpdate) {
            onReceiveMessageUpdate((MessageUpdate) msg);
        } else if (msg instanceof MessageCorrect) {
            onReceiveMessageCorrect((MessageCorrect) msg);
        } else {
            throw new RuntimeException("Error: " + this.getClass().getName()
                    + ".onReceive(): the message is not of the right class");
        }
    }

    /*
     * Private/Protected methods
     */

    // Expected to receive: (clock, source, update)
    private void onReceiveMessageUpdate(MessageUpdate msg) {
        if (msg.getClock() > clocks[msg.getSourceInt()]) {
            pendings.add(msg);
        }

        Iterator<MessageUpdate> iterator = pendings.iterator();
        while (iterator.hasNext()) {
            MessageUpdate item = iterator.next();

            // FIFO-deliver messages
            if (item.getClock() == clocks[item.getSourceInt()] + 1) {
                // Apply the update and remove it from the pendings set
                iterator.remove();
                clocks[item.getSourceInt()] = item.getClock();
                state = DataType.transition(state, item.getUpdate());
                leader = getSelf();

                broadcast(new MessageCorrect(clocks, getSelf(), state));
            }
        }
    }

    // Expected to receive: (clocks[], source, state)
    private void onReceiveMessageCorrect(MessageCorrect msg) {
        // not (j < leader)
        if (msg.getSource() >= leader) {
            return;
        }

        // not (\forall k, clock[k] <= cl[k])
        long[] cl = msg.getClocks();
        for (int k = 0; k < clocks.length; k++) {
            if (clocks[k] > cl[k]) {
                return;
            }
        }

        // The source becomes the new leader
        leader = msg.getSource();
        clocks = msg.getClocks();
        state = msg.getState();

        // Drop out-of-date non-delivered messages
        Iterator<MessageUpdate> iterator = pendings.iterator();
        while (iterator.hasNext()) {
            MessageUpdate item = iterator.next();
            if (item.getClock() <= clocks[msg.getSourceInt()]) {
                iterator.remove();
            }
        }
    }

}
