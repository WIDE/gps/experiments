package applicationLayer;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import gossip.GossipAbstract;
import peersim.core.Network;

// Algorithm 3: UC[k], algorithm from Matthieu Perrin 2015-04-14
public class UCk extends UCAbstract {
    private int k;

    private Object rstate;
    private long vtime;
    private long rvtime;
    private long[] clocks;
    private long[] rclocks;
    private SortedSet<MessageUpdate2> pendings;
    private SortedSet<MessageUpdate> updates;
    private long leader;
    private boolean sent;


    public UCk(long id, int dataIndex, GossipAbstract network, int scenario,
            Object initialState, int k) {
        super(id, dataIndex, network, scenario, initialState);

        this.k = k;
        this.rstate = initialState;
        this.vtime = 0;
        this.rvtime = 0;
        this.leader = getSelf();
        this.sent = false;

        this.pendings = new TreeSet<>();
        this.updates = new TreeSet<>();

        // clock <- [0, .., 0]
        // rclock <- [0, .., 0]
        this.clocks = new long[Network.size()];
        this.rclocks = new long[Network.size()];
        for (int i = 0; i < Network.size(); i++) {
            this.clocks[i] = 0;
            this.rclocks[i] = 0;
        }
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public void update(Update update) {
        vtime++;
        broadcast(new MessageUpdate2(vtime, clocks[getSelfInt()], getSelf(), update));
    }

    @Override
    public Object query(Query input) {
        Object state = rstate;

        // Apply updates sorted on: clock, source
        Iterator<MessageUpdate> iterator = updates.iterator();
        while (iterator.hasNext()) {
            MessageUpdate item = iterator.next();
            state = DataType.transition(state, item.getUpdate());
        }

        return DataType.generation(state, input);
    }

    @Override
    public void onReceive(Object msg) {
        if (msg instanceof MessageUpdate2) {
            onReceiveMessageUpdate((MessageUpdate2) msg);
        } else if (msg instanceof MessageCorrect2) {
            onReceiveMessageCorrect((MessageCorrect2) msg);
        } else {
            throw new RuntimeException("Error: " + this.getClass().getName()
                    + ".onReceive(): the message is not of the right class");
        }
    }

    /*
     * Private/Protected methods
     */

    // Expected to receive: (t, clock, source, update)
    private void onReceiveMessageUpdate(MessageUpdate2 msg) {
        pendings.add(msg);
        FIFODeliver();
    }

    // Expected to receive: (clocks, t, source, state)
    private void onReceiveMessageCorrect(MessageCorrect2 msg) {
        long[] cl = msg.getClocks();
        long t = msg.getVtime();
        long j = msg.getSource();
        Object s = msg.getState();

        // Current process is late compared to source process
        if (rvtime < t) {
            record(t + k);
        }

        // See if rclocks is equal/smaller/greater than cl
        boolean clocksEqual  = true;
        boolean clocksSmaller = true;
        boolean clocksGreater = true;
        for (int i = 0; i < rclocks.length; i++) {
            if (rclocks[i] == cl[i]) {
                clocksSmaller = false;
                clocksGreater = false;
            } else if (rclocks[i] > cl[i]) {
                clocksEqual  = false;
                clocksSmaller = false;
            } else if (rclocks[i] < cl[i]) {
                clocksEqual  = false;
                clocksGreater = false;
            }
        }
        if (!clocksEqual && !clocksGreater && !clocksSmaller) {
            System.err.println("==== UCk: no comparison possible between clocks"); // The state will be updated later
        }

        if (clocksSmaller || (clocksEqual && j < leader)) {
            rclocks = cl;
            rvtime = t;
            rstate = s;
            leader = j;
            sent = true;
            vtime = Math.max(vtime, t);

            // clock <- max(clock, rclock)
            boolean shouldUpdate = false;
            for (int i = 0; i < rclocks.length; i++) {
                if (rclocks[i] > clocks[i]) {
                    shouldUpdate = true;
                    // break; // TODO
                }

                if (shouldUpdate && rclocks[i] < clocks[i]) {
                    System.err.println("==== UCk: should not update");
                }
            }
            if (shouldUpdate) {
                clocks = rclocks;
            }

            // Drop old updates
            Iterator<MessageUpdate2> iterator = pendings.iterator();
            while (iterator.hasNext()) {
                MessageUpdate2 item = iterator.next();
                if (item.getClock() <= t) {
                    iterator.remove();
                }
            }

            FIFODeliver();

        } else if ((clocksGreater || (clocksEqual && leader < j))
                && (j != getSelf()) && (!sent)) {
            // current process' state is better
            broadcast(new MessageCorrect2(rclocks, rvtime, getSelf(), rstate));
            sent = true;
        } else {
            System.err.println("==== UCK: end else");
        }
    }

    private void FIFODeliver() {
        boolean conflict = false;

        // Drop already delivered messages
        Iterator<MessageUpdate2> iterator = pendings.iterator();
        while (iterator.hasNext()) {
            MessageUpdate2 item = iterator.next();
            if (item.getClock() <= clocks[item.getSourceInt()]) {
                iterator.remove();
            } else {
                // break; // TODO: ordered so we can break?
            }
        }

        // Insert pendings in updates
        iterator = pendings.iterator();
        while (iterator.hasNext()) {
            MessageUpdate2 item = iterator.next();
            if (item.getClock() == clocks[item.getSourceInt()] + 1) {
                iterator.remove();
                clocks[item.getSourceInt()] = item.getClock();
                vtime = Math.max(vtime, item.getVtime());
                updates.add(new MessageUpdate(item.getVtime(), item.getSource(),
                        item.getUpdate()));

                // conflict: update not in rstate
                conflict = conflict || (item.getVtime() <= rvtime);
            }
        }

        record(vtime);

        if (conflict) {
            broadcast(new MessageCorrect2(rclocks, rvtime, getSelf(), rstate));
            sent = true;
        }
    }

    private void record(long t) {
        // Maintain |updates| <= 2kn
        if (k > 0) {
            rvtime = Math.max(rvtime, k * ((long) Math.floor(t / k) - 1));
        } else {
            rvtime = Math.max(rvtime, t);
        }

        Iterator<MessageUpdate> iterator = updates.iterator();
        while (iterator.hasNext()) {
            MessageUpdate item = iterator.next();
            if (item.getClock() <= rvtime) {
                iterator.remove();
                rstate = DataType.transition(rstate, item.getUpdate());
                rclocks[item.getSourceInt()]++;
                leader = getSelf();
                sent = false;
            } else {
                // break; // TODO: ordered so we can break?
            }
        }
    }

}
