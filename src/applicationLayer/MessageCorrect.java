package applicationLayer;

// Used in UCZero (algo 2) as message2
public class MessageCorrect {
    private long[] clocks;
    private long source;
    private int sourceInt;
    private Object state;


    public MessageCorrect(long[] clocks, long source, Object state) {
        this.clocks = clocks;
        this.source = source;
        this.sourceInt = (int) source;
        this.state = state;
    }

    /*
     * Getters & setters
     */

    public long[] getClocks() {
        return clocks;
    }

    public long getSource() {
        return source;
    }

    public int getSourceInt() {
        return sourceInt;
    }

    public Object getState() {
        return state;
    }

}
