package applicationLayer;

public class Update {
    private Object value;

    public Update(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

}
