package applicationLayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import gossip.GossipAbstract;

// Algorithm 1: UC[infinity], algorithm from Matthieu Perrin 2015-04-14
// Tried to stick to the algo and variables' names
public class UCInfinity extends UCAbstract  {
    private Object initialState;
    private long clock;
    private List<MessageUpdate> updates;
    private boolean sorted;


    public UCInfinity(long id, int dataIndex, GossipAbstract network,
            int scenario, Object initialState) {
        super(id, dataIndex, network, scenario, initialState);

        this.initialState = initialState;

        this.clock = 0;
        this.updates = new ArrayList<>(); //TODO: use arraylist in other UC too
        this.sorted = true;
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public void update(Update update) {
        clock++;
        broadcast(new MessageUpdate(clock, getSelf(), update));
    }

    @Override
    public Object query(Query input) {
        // Sort the updates collection if a message has been received
        if (!sorted) {
            sorted = true;
            Collections.sort(updates);
        }

        clock++;
        Object state = initialState;

        Iterator<MessageUpdate> iterator = updates.iterator();
        while (iterator.hasNext()) {
            MessageUpdate item = iterator.next();
            state = DataType.transition(state, item.getUpdate());
        }
        state = DataType.generation(state, input);

        // Store the current state to compare it with the next one
        setPreviousState(state);

        return state;
    }

    @Override
    public void onReceive(Object msg) {
        // Flag the update collection as not sorted
        sorted = false;

        if (msg instanceof MessageUpdate) {
            onReceiveMessage((MessageUpdate) msg);
        } else {
            throw new RuntimeException("Error: " + this.getClass().getName()
                    + ".onReceive(): the message is not of the right class");
        }
    }

    /*
     * Private/Protected methods
     */

    // Expected to receive: (clock, source, update)
    private void onReceiveMessage(MessageUpdate msg) {
        clock = Math.max(clock, msg.getClock());
        updates.add(msg);
    }

}
