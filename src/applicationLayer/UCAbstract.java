package applicationLayer;

import gossip.GossipAbstract;

public abstract class UCAbstract implements UCObject {
    // Possible scenarii
    public static final int SCENARIO_NONE = 0; // For temporary usage
    public static final int SCENARIO_1 = 1;    // Array of 1 and 0 [1110100]
    public static final int SCENARIO_2 = 2;    // Append only string
    // Avoid setting the scenario multiple times
    private static boolean firstCall = true;

    // Node id
    private long self;
    private int selfInt;
    // Index in the data array
    private int dataIndex;
    // Network layer
    private GossipAbstract network;

    // Previous value of the UC-variable
    private Object previousState;


    public UCAbstract(long id, int dataIndex, GossipAbstract network,
            int scenario, Object initialState) {
        this.self = id;
        this.selfInt = (int) self;
        this.dataIndex = dataIndex;
        this.network = network;
        this.previousState = initialState;

        // No need to set the scenario more than once
        if (firstCall) {
            firstCall = false;
            DataType.setScenario(scenario);
        }
    }

    /*
     * Private/Protected methods
     */

    protected void broadcast(Object msg) {
        network.sendNewMessage(new PairAppMessageIndex(dataIndex, msg));
    }

    /*
     * Getters & setters
     */

    public long getSelf() {
        return self;
    }

    public int getSelfInt() {
        return selfInt;
    }

    public Object getPreviousState() {
        return previousState;
    }

    protected void setPreviousState(Object previousState) {
        this.previousState = previousState;
    }
}
