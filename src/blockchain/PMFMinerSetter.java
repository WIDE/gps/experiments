package blockchain;

import java.util.ArrayList;
import java.util.List;

import controls.SourceSetter;
import gossip.GossipAbstract;
import peersim.config.Configuration;

public class PMFMinerSetter extends SourceSetter {
    // Parameters from config file
    private static final String PAR_MINER_FILE = "minerFile";
    private static final String PAR_DELAY_FILE = "delayFile";
    private static final String PAR_DELAY_MULT = "delayMultiplier";
    private static String minerFile;
    private static String delayFile;
    private static double delayMultiplier;

    private static List<Integer> generatedDelays;
    private static List<Integer> generatedSources;

    public PMFMinerSetter(String prefix) {
        super(prefix);
        minerFile = Configuration.getString(prefix + "." + PAR_MINER_FILE);
        delayFile = Configuration.getString(prefix + "." + PAR_DELAY_FILE);
        delayMultiplier = Configuration.getDouble(prefix + "." + PAR_DELAY_MULT, 1.0);
    }

    /*
     * Implementation of interfaces
     */

    // Only executed at init time
    @Override
    public boolean execute() {
        generatedSources = PMFHelper.sampleFromPMFFile("PMF sources", minerFile, getNumberSources(), false);
        generatedDelays = PMFHelper.sampleFromPMFFile("PMF delays", delayFile, getNumberSources(), false);
        modifyDelays();
        boolean result = super.execute();
        generatedDelays = null; // force garbage collection
        generatedSources = null; // force garbage collection
        return result;
    }

    /*
     * Private/Protected methods
     */

    // Select actual sources from the source pool
    @Override
    protected List<GossipAbstract> selectSources(List<GossipAbstract> sourcePool) {
        List<GossipAbstract> sources = new ArrayList<>();
        for (int s = 0; s < getNumberSources(); s++) {
            int r = generatedSources.get(s) - 1; // miner id starts at 1 in the dataset file
            // TODO: maybe scale out the miner id? Right now it only works if the size of the source pool = number of miners in the dataset but I don't know how to scale out properly, it's tricky
            sources.add(sourcePool.get(r));
        }
        return sources;
    }

    // Select rounds updates are being created at
    @Override
    protected List<Integer> selectRounds() {
        List<Integer> rounds = new ArrayList<>();
        int r = 0; // First update on first round
        for (int s = 0; s < getNumberSources(); s++) {
            rounds.add(r);
            r += generatedDelays.get(s);
        }
        return rounds;
    }

    private void modifyDelays() {
        List<Integer> newDelays = new ArrayList<Integer>();
        for (Integer v: generatedDelays) {
            newDelays.add((int) (v * delayMultiplier));
        }
        generatedDelays = newDelays;
        System.err.println("PMF delays: applied multiplier " + delayMultiplier);
    }
}
