package blockchain;

import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import peersim.core.CommonState;

public class RandomSamplerThread implements Runnable {
    private List<Entry<Integer, Double>> pmf;
    private double totalWeight;
    private List<Integer> generatedRandoms;
    private long nbRandoms;
    private Random r;
    private AtomicLong counterPool;
    private long nbRandomsPool;
    private long counterStep;

    public RandomSamplerThread(List<Entry<Integer, Double>> pmf,
            double totalWeight, List<Integer> generatedRandoms, long nbRandoms,
            long randomSeed, AtomicLong counterPool, long nbRandomsPool) {
        this.pmf = pmf;
        this.totalWeight = totalWeight;
        this.generatedRandoms = generatedRandoms;
        this.nbRandoms = nbRandoms;
        this.r = new Random(randomSeed); // I'm guessing CommonState.r.nextX isn't threadsafe
        this.counterPool = counterPool;
        this.nbRandomsPool = nbRandomsPool;
        this.counterStep = (long) Math.floor(0.05 * nbRandomsPool);
    }

    // Sample data from a PMF bruteforce-style
    // Inspired from https://stackoverflow.com/a/6737362
    @Override
    public void run() {
        int index = r.nextInt(pmf.size());
        long i = 0;
        while (i < nbRandoms) {
            Entry<Integer, Double> e = pmf.get(index);
            if (r.nextDouble() * totalWeight - e.getValue() <= 0) {
                generatedRandoms.add(e.getKey());
                i++;
                if (i % counterStep == 0) {
                    long ratio = counterPool.addAndGet(counterStep);
                    ratio = Math.round(100 * ratio / nbRandomsPool);
                    System.err.println("  " + ratio + "% values generated");
                }
            }
            index = r.nextInt(pmf.size());
        }
    }
}
