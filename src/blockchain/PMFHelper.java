package blockchain;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;

public class PMFHelper implements Control {
    private static final String PAR_NB_THREADS = "nbThreads";
    private static final String PAR_TIMEOUT = "timeout";
    private static final String PAR_DATASET_DIR = "datasetDir";
    private static int nbThreads;
    private static int timeout;
    private static String datasetDir;


    public PMFHelper(String prefix) {
        nbThreads = Configuration.getInt(prefix + "." + PAR_NB_THREADS);
        timeout = Configuration.getInt(prefix + "." + PAR_TIMEOUT);
        datasetDir = Configuration.getString(prefix + "." + PAR_DATASET_DIR);
    }

    /*
     * Implementation of interfaces
     */

    // Only executed at init time
    @Override
    public boolean execute() {
        return false; // Continue the simulation
    }


    /*
     * Public methods
     */

    // Read a file storing a two-column PMF of format "int,float" and generate a number of random values following the PMF
    public static List<Integer> sampleFromPMFFile(String prefix, String inputFile, long nbSamples, boolean computeInParallel) {
        // Parse input csv file and fill the PMF map
        Map<Integer, Double> pmf = new HashMap<>();
        pmf = PMFHelper.parseCSVFile(datasetDir + "/" + inputFile);

        // Increase probability of successful sampling
        double totalWeight = PMFHelper.normalizePMF(pmf);

        // Generate the degrees by randomly sampling the PMF (should be quick)
        System.err.println(prefix + ": need to samples " + nbSamples + " values");
        ArrayList<Entry<Integer, Double>> pmfEntries = new ArrayList<>(pmf.entrySet());
        List<Integer> samples = new ArrayList<>();
        if (computeInParallel) {
            samples = PMFHelper.randomSamplerParallel(pmfEntries, totalWeight, nbSamples);
        } else {
            samples = PMFHelper.randomSamplerSerial(pmfEntries, totalWeight, nbSamples);
        }
        System.err.println(prefix + ": generated " + samples.size() + " values");

        System.err.println(prefix + ": sanity check on samples:");
        PMFHelper.statsOnGeneratedValues(samples);

        return samples;
    }

    /*
     * Private/Protected methods
     */

    // Parses a csv file with two column of format "int,float"
    private static Map<Integer, Double> parseCSVFile(String inputFile) {
        Map<Integer, Double> pmf = new HashMap<>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(inputFile));
            br.readLine(); // Skip the header
            String line = "";
            while ((line = br.readLine()) != null) {
                String[] row = line.split(",");
                if (row.length != 2) {
                    throw new IOException("Number of columns != 2 in the csv file: " + inputFile);
                }
                pmf.put(Integer.valueOf(row[0]), Double.valueOf(row[1]));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return pmf;
    }

    // Normalize PMF values using the mode to increase the probability of successful sampling
    private static double normalizePMF(Map<Integer, Double> pmf) {
        double mode = 0.0;
        double totalWeight = 0.0;
        for (double weight : pmf.values()) {
            totalWeight += weight;
            if (weight > mode) {
                mode = weight;
            }
        }
        double normalizationFactor = totalWeight / mode;
        for (Integer key : pmf.keySet()) {
            pmf.put(key, pmf.get(key) * normalizationFactor);
        }
        return totalWeight;
    }

    // Returns the wanted number of samples from the PMF, run in a single thread
    private static List<Integer> randomSamplerSerial(List<Entry<Integer, Double>> pmf,
            double totalWeight, long nbSamplesTotal) {
        List<Integer> generated = new ArrayList<>();
        long counterStep = (long) Math.floor(0.05 * nbSamplesTotal);
        int index = CommonState.r.nextInt(pmf.size());

        long i = 0;
        while (i < nbSamplesTotal) {
            Entry<Integer, Double> e = pmf.get(index);
            if (CommonState.r.nextDouble() * totalWeight - e.getValue() <= 0) {
                generated.add(e.getKey());
                i++;
                if (counterStep > 0 && i % counterStep == 0) {
                    long ratio = Math.round(100 * i / nbSamplesTotal);
                    System.err.println("  " + ratio + "% values generated");
                }
            }
            index = CommonState.r.nextInt(pmf.size());
        }
        return generated;
    }

    // Returns the wanted number of samples from the PMF using multiple threads
    private static List<Integer> randomSamplerParallel(ArrayList<Entry<Integer, Double>> pmfEntries,
            double totalWeight, long nbSamplesTotal) {
        ExecutorService pool = Executors.newFixedThreadPool(nbThreads);
        // Extra computation per thread to avoid rounding issues above
        long nbRandomsPerThreads = nbSamplesTotal / nbThreads + 1;
        // To display progress
        AtomicLong counterTotal = new AtomicLong(0);

        List<List<Integer>> generatedPerThread = new ArrayList<>();
        for (int i = 0; i < nbThreads; i++) {
            List<Integer> threadOutput = new ArrayList<>();
            generatedPerThread.add(threadOutput);
            Runnable thread = new RandomSamplerThread(pmfEntries, totalWeight,
                    threadOutput, nbRandomsPerThreads, CommonState.r.nextLong(),
                    counterTotal, nbSamplesTotal);
            pool.execute(thread);
        }

        // Wait for and aggregate results
        shutdownAndAwaitTermination(pool);
        List<Integer> generated = new ArrayList<>();
        for (List<Integer> threadOutput : generatedPerThread) {
            generated.addAll(threadOutput);
        }
        return generated;
    }

    // http://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html
    private static void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(timeout, TimeUnit.MILLISECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(timeout, TimeUnit.MILLISECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    // Mostly for debug, to check the generated samples correctly extrapolate the input PMF
    private static void statsOnGeneratedValues(List<Integer> unsortedDataset) {
        // Sort for percentile computations
        List<Integer> dataset = new ArrayList<>(unsortedDataset);
        dataset.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1-o2;
            }
        });

        double mean = 0.0;
        double stddev = 0.0;
        double mode = 0.0;
        int countermode = 0;
        int currentvalue = 0;
        int countervalue = 0;
        for (Integer v: dataset) {
            // For mode
            if (currentvalue != v) {
                currentvalue = v;
                countervalue = 1;
            } else {
                countervalue++;
            }

            if (countervalue > countermode) {
                countermode = countervalue;
                mode = v;
            }
            mean += v;
        }
        mean /= dataset.size();
        for (Integer v: dataset) {
            stddev += Math.pow(v - mean, 2);
        }
        stddev = Math.sqrt( stddev / (dataset.size() - 1) );
        System.err.println("  mean: \t" + mean);
        System.err.println("  stdev:\t" + stddev);
        System.err.println("  mode: \t" + mode);
        System.err.println("  50th: \t" + dataset.get((int) Math.ceil(0.50 * dataset.size()) - 1));
        System.err.println("  90th: \t" + dataset.get((int) Math.ceil(0.90 * (dataset.size())) - 1));

        //        System.out.println("values and the #occurences/value: ");
        //        currentvalue = -1;
        //        countervalue = 0;
        //        int diffValues = -1;
        //        for (int v: dataset) {
        //            if (currentvalue != v) {
        //                if (currentvalue != -1) {
        //                    System.out.println("" + currentvalue + "\t" + countervalue);
        //                }
        //                currentvalue = v;
        //                countervalue = 1;
        //                diffValues++;
        //            } else {
        //                countervalue++;
        //            }
        //        }
        //        System.out.println("" + currentvalue + "\t" + countervalue);
        //        System.out.println("#### " + diffValues);
    }
}
