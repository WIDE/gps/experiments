package blockchain;

import java.util.List;

import controls.PriorityRandomPeerSampler;
import peersim.config.Configuration;
import peersim.core.Network;
import utils.NeighboursLinkable;

public class PMFOutdegreeSetter extends PriorityRandomPeerSampler {
    // Parameters from config file
    private static final String PAR_INPUT_FILE = "degreeFile";
    private static String inputFile;

    public PMFOutdegreeSetter(String prefix) {
        super(prefix);
        inputFile = Configuration.getString(prefix + "." + PAR_INPUT_FILE);
    }

    /*
     * Implementation of interfaces
     */

    // Only executed at init time
    @Override
    public boolean execute() {
        super.fillNodesCollections();
        List<Integer> samples = PMFHelper.sampleFromPMFFile("PMF degrees", inputFile, Network.size(), false);
        fillAllNeighbourhoods(samples);
        return false; // Continue the simulation
    }

    /*
     * Private/Protected methods
     */

    // Create new neighbourhoods from scratch, a node has the same random outdegree towards primaries and secondaries
    protected void fillAllNeighbourhoods(List<Integer> generated) {
        for (int i = 0; i < Network.size(); i++) {
            NeighboursLinkable neighboursP = (NeighboursLinkable) Network.get(i).getProtocol(getLinkableIdP());
            fillOneNeighbourhood(neighboursP, NeighboursLinkable.TYPE_PRIMARY, generated.get(i), getPrimaryNodes());

            if (getLinkableIdS() >= 0) {
                NeighboursLinkable neighboursS = (NeighboursLinkable) Network.get(i).getProtocol(getLinkableIdS());
                fillOneNeighbourhood(neighboursS, NeighboursLinkable.TYPE_SECONDARY, generated.get(i), getSecondaryNodes());
            }
        }
    }
}
