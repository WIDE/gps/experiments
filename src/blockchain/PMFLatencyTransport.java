package blockchain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import peersim.config.Configuration;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDSimulator;
import peersim.transport.Transport;

public class PMFLatencyTransport implements Control, Transport {
    // Parameters from config file
    private static final String PAR_INPUT_FILE = "latencyFile";
    private static String inputFile;

    // Fixed latencies between pairs of nodes, filled at random at bootstrap
    // Latencies between nodes A and B are symetric, so only need to store a triangular matrix
    // Map<Node id src, map<Node id dst, latency>>
    private static Map<Long, Map<Long, Integer>> latencies;


    public PMFLatencyTransport(String prefix) {
        inputFile = Configuration.getString(prefix + "." + PAR_INPUT_FILE);
    }

    // Constructor by copy
    public PMFLatencyTransport(PMFLatencyTransport dolly) {
        // No structure to copy, they're all static
    }

    // Redefine clone() to have proper collection cloning and no exception
    @Override
    public Object clone() {
        return new PMFLatencyTransport(this);
    }

    /*
     * Implementation of interfaces
     */

    // Only executed at init time
    @Override
    public boolean execute() {
        long nbRandomLatencies = (int) Math.ceil((Network.size() * (Network.size() + 1)) / 2);
        List<Integer> samples = PMFHelper.sampleFromPMFFile("PMF latencies", inputFile, nbRandomLatencies, true);
        initLatencyStore(samples);
        return false; // Continue the simulation
    }

    @Override
    public long getLatency(Node src, Node dst) {
        // The latency matrix is a triangle since latencies are symmetric
        Long id1 = src.getID();
        Long id2 = dst.getID();
        if (src.getID() > dst.getID()) {
            id1 = dst.getID();
            id2 = src.getID();
        }
        return latencies.get(id1).get(id2);
    }

    // Taken from UniformRandomTransport.java
    @Override
    public void send(Node src, Node dst, Object msg, int pid) {
        long delay = getLatency(src, dst);
        EDSimulator.add(delay, msg, dst, pid);
    }

    /*
     * Private/Protected methods
     */

    private void initLatencyStore(List<Integer> generated) {
        latencies = new HashMap<>();
        int arrayIndex = 0;
        for (int i = 0; i < Network.size(); i++) {
            Long id1 = Network.get(i).getID();
            latencies.put(id1, new HashMap<>());
            for (int j = i; j < Network.size(); j++) {
                Long id2 = Network.get(j).getID();
                int latency = 0;
                if (i != j) {
                    latency = generated.get(arrayIndex);
                    arrayIndex++;
                }
                latencies.get(id1).put(id2, latency);
            }
        }
    }
}
