package controls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import applicationLayer.UCAbstract;
import gossip.GossipAbstract;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import utils.Global;
import utils.IncrementableCounter;
import utils.TupleRowMessageTable;

public class Observer implements Control {
    // Parameters from config file
    private static final String PAR_PROTOCOL_ID = "protocol";
    private static final String PAR_MANUAL_STEP_ID = "stepManual";
    private static int protocolId;
    private static int step;

    private static int scenario;
    private int sources;
    private int variables;

    private boolean isUniformGossip;
    private double density;
    private int numberPrimary;
    private int numberSecondary;

    // Contains the accumulated csvs to output at the end
    private List<String> nbGossipsBetweenClassesCsv;
    private List<String> disseminationSecondariesCsv;

    // Stores the message hashes, having a list makes them stay in the same
    // order from one round to another
    private List<Long> listMsgHashes;

    // To avoid running the whole computation if nothing happened, just print the previous stats
    private static boolean somethingHappened;
    private String statsLine;

    public Observer(String prefix) {
        protocolId = Configuration.getPid(prefix + "." + PAR_PROTOCOL_ID);
        step = Configuration.getInt(prefix + "." + PAR_MANUAL_STEP_ID);

        nbGossipsBetweenClassesCsv = new ArrayList<>();
        disseminationSecondariesCsv = new ArrayList<>();
        listMsgHashes = new ArrayList<>();
        somethingHappened = false;
        statsLine = "";
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public boolean execute() {
        // First round
        if (CommonState.getIntTime() == 0) {
            initVars();
            // Print the rounds csv header
            System.out.println("========== ROUNDS");
            System.out.println("round,"
                    + "nodes,nodesP,nodesS,"
                    + "recvOne,recvOneP,recvOneS,"
                    + "recvAll,recvAllP,recvAllS,"
                    + "msgSent,msgSentP,msgSentS,"
                    + "msgRecv,msgRecvP,msgRecvS,"
                    + "consist,consistP,consistS,"
                    + "prefix,prefixP,prefixS"
                    );

            // This control is executed before the protocol, so nothing is ever
            // seen by this control at round 0
            return false;
        }

        // Need to handle the step manually otherwise last round computations are not always run
        // time() - 1 because observer executed before protocol
        if ((CommonState.getIntTime() - 1) % step == 0) {
            currentRoundStats();
            // Only run these lengthy computation if there has been some action on the network
            if (somethingHappened) {
                insertMessagesDB();
                storeCurrentRoundNbGossipsBetweenClasses();
                storeCurrentRoundDisseminationSecondariesPerMessage();
                // Reset the computation trigger
                somethingHappened = false;
            }
        }

        // Last round
        if (CommonState.getIntTime() == CommonState.getEndTime() - 1) {
            aggregateLatenciesPerClass();
            printNbGossipsBetweenClassesCsv();
            printDisseminationSecondariesPerMessageCsv();
            // Close database connection on last round
            DatabaseHandler.closeConnection();
        }

        // Continue the simulation
        return false;
    }

    /*
     * Private/Protected methods
     */

    private void initVars() {
        scenario = GossipAbstract.getScenario();
        sources = SourceSetter.getNumberSources();
        variables = SourceSetter.getNumberVariables();

        // If it's uniform gossip, nullify the weight of the secondary nodes
        isUniformGossip = ((GossipAbstract) Network.get(0).getProtocol(protocolId))
                .getClasse() == GossipAbstract.CLASS_UNIFORM;
        numberPrimary = Network.size();
        if (!isUniformGossip) {
            numberPrimary = PrimarySetter.getNbPrimaries();
        }
        numberSecondary = Network.size() - numberPrimary;
        density = numberPrimary / Network.size();
    }

    // Compute and print the stats of the current round
    private void currentRoundStats() {
        // Avoid long computation if there has been no action on the network
        if (step == 1 && !somethingHappened && !statsLine.equals("")) {
            // -1 because executed before protocol (=> print stats of the previous round)
            System.out.println(Global.format("%02d," + statsLine, CommonState.getIntTime() - 1));
            return;
        }

        // Gather the messages informations from the nodes
        long messagesSentByPrimary = 0;
        long messagesSentBySecondary= 0;
        long messagesReceivedByPrimary = 0;
        long messagesReceivedBySecondary = 0;
        long primaryWhoReceivedOneMsg = 0;
        long secondaryWhoReceivedOneMsg = 0;
        long primaryWhoReceivedAllMsg = 0;
        long secondaryWhoReceivedAllMsg = 0;

        for (int i = 0; i < Network.size(); i++) {
            GossipAbstract ga = (GossipAbstract) Network.get(i).
                    getProtocol(protocolId);
            long sent = ga.getNumberOfMessagesSent();
            long recv = ga.getNumberOfMessagesReceived()
                    - ga.getNumberOfMessagesSentAsSource();
            long recvDiff = ga.getNumberOfDifferentMessagesReceived();

            // Nodes in uniform gossip are considered as primary
            if (ga.getClasse() == GossipAbstract.CLASS_UNIFORM
                    || ga.getClasse() == GossipAbstract.CLASS_PRIMARY) {
                messagesSentByPrimary += sent;
                messagesReceivedByPrimary += recv;
                if (recv > 0) primaryWhoReceivedOneMsg++;
                if (recvDiff == sources) primaryWhoReceivedAllMsg++;
            } else {
                messagesSentBySecondary += sent;
                messagesReceivedBySecondary += recv;
                if (recv > 0) secondaryWhoReceivedOneMsg++;
                if (recvDiff == sources) secondaryWhoReceivedAllMsg++;
            }
        }

        // Compute all the messages stats, avoid divisions by zero
        double percentagePrimaryReceivedOneMsg = 0;
        double percentageSecondaryReceivedOneMsg = 0;
        double percentagePrimaryReceivedAllMsg = 0;
        double percentageSecondaryReceivedAllMsg = 0;

        if (numberPrimary > 0) {
            percentagePrimaryReceivedOneMsg = 100.0 * primaryWhoReceivedOneMsg / numberPrimary;
            percentagePrimaryReceivedAllMsg = 100.0 * primaryWhoReceivedAllMsg / numberPrimary;
        }
        if (numberSecondary > 0) {
            percentageSecondaryReceivedOneMsg = 100.0 * secondaryWhoReceivedOneMsg / numberSecondary;
            percentageSecondaryReceivedAllMsg = 100.0 * secondaryWhoReceivedAllMsg / numberSecondary;
        }

        // Gather the consistency informations from the nodes
        // Longest common prefix amongst all nodes in the same class
        long consistentPrimary = 0;
        long consistentSecondary = 0;
        long longestPrefixPrimary = Long.MAX_VALUE;
        long longestPrefixSecondary = Long.MAX_VALUE;
        for (int i = 0; i < Network.size(); i++) {
            GossipAbstract ga = (GossipAbstract) Network.get(i).
                    getProtocol(protocolId);

            // Compute the prefix of each node, compare to the one of node's class
            long prefixCurrentNode = 0;

            // Is inconsistent when there is a 0 after a 1 (e.g., [11101000]
            if (scenario == UCAbstract.SCENARIO_1) {
                int lastValue = 1;
                boolean anomaly = false;
                boolean onlyZeros = true;
                for (UCAbstract data: ga.getReplicatedDatas()) {
                    int currentValue = (int) data.query(null);
                    if (lastValue == 0 && currentValue != 0) {
                        anomaly = true;
                        break;
                    }
                    if (currentValue != 0) {
                        onlyZeros = false;
                        prefixCurrentNode++;
                    }
                    lastValue = currentValue;
                }

                // TODO: when only zeros, consider it consistent?
                // if (!anomaly && !onlyZeros) {
                if (!anomaly) {
                    // Update consistency of each class
                    if (ga.getClasse() == GossipAbstract.CLASS_UNIFORM
                            || ga.getClasse() == GossipAbstract.CLASS_PRIMARY) {
                        consistentPrimary++;
                    } else {
                        consistentSecondary++;
                    }
                }
            }

            // Is inconsistent when currentState doesn't contain previousState
            if (scenario == UCAbstract.SCENARIO_2) {
                for (UCAbstract data: ga.getReplicatedDatas()) {
                    @SuppressWarnings("unchecked")
                    List<Object> previousList = (List<Object>) data.getPreviousState();
                    @SuppressWarnings("unchecked")
                    List<Object> currentList = (List<Object>) data.query(null);

                    // The current state must be at least the same length as
                    // the previous state
                    if (currentList.size() < previousList.size()) {
                        continue;
                    }

                    // See if the current state contains the previous one
                    int j = 0;
                    while(j < previousList.size()
                            && previousList.get(j).equals(currentList.get(j))) {
                        j++;
                    }

                    // Parsed all the list and items are equal (or list.size() == 0)
                    if (j == previousList.size()) {
                        if (ga.getClasse() == GossipAbstract.CLASS_UNIFORM
                                || ga.getClasse() == GossipAbstract.CLASS_PRIMARY) {
                            consistentPrimary++;
                        } else {
                            consistentSecondary++;
                        }
                    }
                }
            }

            // Some nodes won't receive messages, ignore when prefix = 0
            if (prefixCurrentNode == 0) {
                continue;
            }

            // Update prefix of each class
            if (ga.getClasse() == GossipAbstract.CLASS_UNIFORM
                    || ga.getClasse() == GossipAbstract.CLASS_PRIMARY) {
                if (prefixCurrentNode < longestPrefixPrimary) {
                    longestPrefixPrimary = prefixCurrentNode;
                }
            } else {
                if (prefixCurrentNode < longestPrefixSecondary) {
                    longestPrefixSecondary = prefixCurrentNode;
                }
            }
        }

        // If the prefix value is still the default one, then it means it hasn't
        // been changed and thus it should be 0
        if (longestPrefixPrimary == Long.MAX_VALUE) {
            longestPrefixPrimary = 0;
        }
        if (longestPrefixSecondary == Long.MAX_VALUE) {
            longestPrefixSecondary = 0;
        }
        // Compute the prefix for P+S differently than for uniform gossip
        long longestPrefixPS;
        if (isUniformGossip) {
            longestPrefixPS = longestPrefixPrimary;
        } else {
            longestPrefixPS = Math.min(longestPrefixPrimary, longestPrefixSecondary);
        }

        // For scenario 2, normalise by the number of variables
        if (scenario == UCAbstract.SCENARIO_2) {
            consistentPrimary /= variables;
            consistentSecondary /= variables;
        }

        // Compute the consistency stats
        double percentagePrimaryConsistent = 0;
        double percentageSecondaryConsistent = 0;
        if (numberPrimary > 0) {
            percentagePrimaryConsistent = 100.0 * consistentPrimary / numberPrimary;
        }
        if (numberSecondary > 0) {
            percentageSecondaryConsistent = 100.0 * consistentSecondary / numberSecondary;
        }

        // Compute the weighted sums
        double percentageAllReceivedOneMsg = percentagePrimaryReceivedOneMsg * density
                + percentageSecondaryReceivedOneMsg * (1-density);
        double percentageAllReceivedAllMsg = percentagePrimaryReceivedAllMsg * density
                + percentageSecondaryReceivedAllMsg * (1-density);
        long messagesSentByAll =
                messagesSentByPrimary + messagesSentBySecondary;
        long messagesReceivedByAll =
                messagesReceivedByPrimary + messagesReceivedBySecondary;
        double percentageAllConsistent =
                percentagePrimaryConsistent * density
                + percentageSecondaryConsistent * (1 - density);

        // Print the round data as a csv line
        statsLine = Global.format(""
                + "%d,%d,%d,"
                + "%f,%f,%f,"
                + "%f,%f,%f,"
                + "%d,%d,%d,"
                + "%d,%d,%d,"
                + "%f,%f,%f,"
                + "%d,%d,%d",

                numberPrimary + numberSecondary,
                numberPrimary,
                numberSecondary,

                percentageAllReceivedOneMsg,
                percentagePrimaryReceivedOneMsg,
                percentageSecondaryReceivedOneMsg,

                percentageAllReceivedAllMsg,
                percentagePrimaryReceivedAllMsg,
                percentageSecondaryReceivedAllMsg,

                messagesSentByAll,
                messagesSentByPrimary,
                messagesSentBySecondary,

                messagesReceivedByAll,
                messagesReceivedByPrimary,
                messagesReceivedBySecondary,

                percentageAllConsistent,
                percentagePrimaryConsistent,
                percentageSecondaryConsistent,

                longestPrefixPS,
                longestPrefixPrimary,
                longestPrefixSecondary
                );

        // -1 because executed before protocol (it's actually the stats
        // of the previous round)
        System.out.println(Global.format("%02d," + statsLine, CommonState.getIntTime() - 1));

        // NOTE: update code for database insertion
        // DatabaseHandler.insertRound();
    }

    // Insert every message sent by every node in the database
    private void insertMessagesDB() {
        for (int i = 0; i < Network.size(); i++) {
            GossipAbstract ga = (GossipAbstract) Network.get(i).
                    getProtocol(protocolId);

            // Insert every message
            for (TupleRowMessageTable row: ga.getMessageStore()) {
                DatabaseHandler.insertMessage(row);
            }

            // Empty temporary store
            ga.emptyMessageStore();
        }
    }

    // Aggregate the latency of delivered messages per node class (Primary/Secondary)
    private void aggregateLatenciesPerClass() {
        // <latency, number of nodes>
        Map<Long, IncrementableCounter> latenciesP = new TreeMap<>();
        Map<Long, IncrementableCounter> latenciesS = new TreeMap<>();
        Map<Long, IncrementableCounter> latenciesPS = new TreeMap<>();

        for (int i = 0; i < Network.size(); i++) {
            GossipAbstract ga = (GossipAbstract) Network.get(i).
                    getProtocol(protocolId);
            Map<Long, IncrementableCounter> latencies;

            // Nodes in uniform gossip are considered as primary
            if (ga.getClasse() == GossipAbstract.CLASS_UNIFORM
                    || ga.getClasse() == GossipAbstract.CLASS_PRIMARY) {
                latencies = latenciesP;
            } else {
                latencies = latenciesS;
            }

            for (Long latency: ga.getLatenciesDeliveredMessages()) {
                // Ignore when latency = 0 (means the node is the source
                if (latency == 0) {
                    continue;
                }

                // Increment the counter for this latency
                IncrementableCounter.incrementMap(latencies, latency);

                // Also increment the counter for P+S
                if(ga.getClasse() == GossipAbstract.CLASS_PRIMARY
                        || ga.getClasse() == GossipAbstract.CLASS_SECONDARY) {
                    IncrementableCounter.incrementMap(latenciesPS, latency);
                }
            }
        }

        // Print the latencies in a csv format
        System.out.println("========== LATENCIES");
        System.out.println("class,latency,nodes");
        if (((GossipAbstract) Network.get(0).getProtocol(protocolId))
                .getClasse() == GossipAbstract.CLASS_UNIFORM) {
            printCountersMap(latenciesP, "Uniform");
        } else {
            printCountersMap(latenciesP, "P");
            printCountersMap(latenciesS, "S");
            printCountersMap(latenciesPS, "P+S");
        }
    }

    // Print a map containing counters
    private void printCountersMap(Map<Long, IncrementableCounter> map, String prefix) {
        for (Entry<Long, IncrementableCounter> entry: map.entrySet()) {
            System.out.println(prefix + "," + entry.getKey() + "," + entry.getValue());
        }
    }

    // For the current round, store the number of gossips made between classes
    private void storeCurrentRoundNbGossipsBetweenClasses() {
        // We need one map per direction (primary/secondary to primary/secondary)
        Map<String, Map<Long, IncrementableCounter>> class2class = new TreeMap<>();

        // <number of different messages sent at once, number of nodes>
        class2class.put("p2p", new TreeMap<Long, IncrementableCounter>());
        class2class.put("p2s", new TreeMap<Long, IncrementableCounter>());
        class2class.put("s2s", new TreeMap<Long, IncrementableCounter>());
        class2class.put("s2p", new TreeMap<Long, IncrementableCounter>());

        for (int i = 0; i < Network.size(); i++) {
            GossipAbstract ga = (GossipAbstract) Network.get(i).
                    getProtocol(protocolId);

            // <direction (p2p, p2s, ...), number of gossips>
            Map<String, IncrementableCounter> map = ga.getNumberGossipsPerClass();

            // Transfer from the node to local maps
            for (String direction: class2class.keySet()) {
                IncrementableCounter.incrementMap(class2class.get(direction),
                        map.get(direction).getValue());
            }

            // Empty the store of this node for next round
            ga.emptyNumberGossipsPerClass();
        }

        // Append the current round to the csv string
        for (String direction: class2class.keySet()) {
            for (Long nbGossips: class2class.get(direction).keySet()) {
                // Ignore when no gossip has been made
                if (nbGossips == 0) {
                    continue;
                }

                long nbNodes = class2class.get(direction).get(nbGossips).getValue();
                nbGossipsBetweenClassesCsv.add(Global.format("%02d,%s,%d,%d",
                        CommonState.getTime() - 1, direction, nbGossips,
                        nbNodes));
            }
        }
    }

    // For the current round, store the dissemination of the secondaries
    // per message
    private void storeCurrentRoundDisseminationSecondariesPerMessage() {
        // No secondaries in uniform gossip
        if (((GossipAbstract) Network.get(0).getProtocol(protocolId))
                .getClasse() == GossipAbstract.CLASS_UNIFORM) {
            return;
        }

        Map<Long, IncrementableCounter> nbNodesPerHash = new HashMap<>();
        for (int i = 0; i < Network.size(); i++) {
            GossipAbstract ga = (GossipAbstract) Network.get(i).
                    getProtocol(protocolId);

            // Ignore primary nodes
            if (ga.getClasse() == GossipAbstract.CLASS_PRIMARY) {
                continue;
            }

            // Parse all the received msg from each node, and increment the
            // counter linked to each msg hash
            for (Long msgHash: ga.getReceivedMessages().keySet()) {
                // Add the yet unknown msg hash to the global list
                // A list is used to preserve the order of msg from one round
                // to another
                if (!listMsgHashes.contains(msgHash)) {
                    listMsgHashes.add(msgHash);
                }
                IncrementableCounter.incrementMap(nbNodesPerHash, msgHash);
            }
        }

        // Always print the msg in the same order, one column per msg hash
        // Create a csv line, append it little by little and add it to the global csv
        String row = Global.format("%02d", CommonState.getTime() - 1);
        int numberMsgPrinted = 0;
        for (Long msgHash: listMsgHashes) {
            if (!nbNodesPerHash.containsKey(msgHash)) {
                System.out.println("----- SHOULD not happen?");
                continue;
            }

            double percentageSecondaryReceived =
                    100.0 * nbNodesPerHash.get(msgHash).getValue() / numberSecondary;

            // Ignore when 0 received (rather print a comma later)
            if (percentageSecondaryReceived == 0) {
                continue;
            }

            row += Global.format(",%f", percentageSecondaryReceived);
            numberMsgPrinted++;
        }

        // Append the extra empty columns for the not-yet received messages
        for (int i = 0; i < sources - numberMsgPrinted; i++) {
            row += ",";
        }

        disseminationSecondariesCsv.add(row);
    }

    // Print the number of gossips made between classes in a csv format
    private void printNbGossipsBetweenClassesCsv() {
        System.out.println("========== GOSSIPS BETWEEN CLASSES");
        System.out.println("round,class2class,gossips,nodes");
        for (String row: nbGossipsBetweenClassesCsv) {
            System.out.println(row);
        }
    }

    // Print the dissemination of the secondaries per message in a csv format
    private void printDisseminationSecondariesPerMessageCsv() {
        System.out.println("========== DISSEMINATION SECONDARIES");
        String header ="round";
        for (int i = 0; i < sources; i++) {
            header += Global.format(",mId%03d", i);
        }
        System.out.println(header);
        for (String row: disseminationSecondariesCsv) {
            System.out.println(row);
        }
    }

    /*
     * Getters & setters
     */
    public static void thereIsAction() {
        somethingHappened = true;
    }
}
