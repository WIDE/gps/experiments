package controls;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import peersim.config.Configuration;
import peersim.core.Control;
import utils.TupleRowMessageTable;

// This class implements Control so that it can read the configuration file
public class DatabaseHandler implements Control {
    // Parameters from config file
    private static final String PAR_DB_URL = "dbUrl";
    private static final String PAR_BATCH_SIZE = "batchSize";
    private static String dbUrl;
    private static int maxBatchSize;

    // Ensure singleton
    private static boolean instantiated = false;
    private static Connection connection;

    // For messages insertions
    private static PreparedStatement messageStatement;
    private static PreparedStatement roundStatement;
    private static int messageBatchSize;
    private static int roundBatchSize;


    // Can't have a singleton, force it to be called only once
    // Peersim will allocate it as a Control, the others classes must
    // access the instance through static references
    public DatabaseHandler(String prefix) {
        // Declare the class as instantiated on first call, error on 2nd+ call
        synchronized (DatabaseHandler.class) {
            if (!instantiated) {
                instantiated = true;
            } else {
                String msg = "Calling " + this.getClass().getName()
                        + " constructor several times.";
                throw new RuntimeException(msg);
            }
        }

        // Read the configuration file
        dbUrl = Configuration.getString(prefix + "." + PAR_DB_URL);
        maxBatchSize = Configuration.getInt(prefix + "." + PAR_BATCH_SIZE);

        // Initialise the database connection and create tables
        try {
            connection = DriverManager.getConnection(dbUrl);
            // Disable auto commit for batches
            connection.setAutoCommit(false);
            createTablesAndStatements();
            messageBatchSize = 0;
            roundBatchSize = 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // To know if the database is in usage or not
    public static boolean isInstantiated() {
        synchronized (DatabaseHandler.class) {
            return instantiated;
        }
    }

    /*
     * Implementation of interfaces
     */

    // Do nothing
    @Override
    public boolean execute() {
        System.err.println("WARNING: database code is out of date! "
                + "Some insertions will fail");
        // Continue the simulation
        return false;
    }

    /*
     * Public methods
     */

    // Create the database tables and the statements to prepare for insertions
    public static void createTablesAndStatements() {
        // Avoid errors if the database is not used
        if (!isInstantiated()) {
            return;
        }

        try {
            Statement table = connection.createStatement();

            table.execute(
                    "CREATE TABLE rounds ("
                            + "round    BIGINT, "
                            + "nodes    BIGINT, "
                            + "nodesP   BIGINT, "
                            + "nodesS   BIGINT, "
                            + "recvOne  DOUBLE, "
                            + "recvOneP DOUBLE, "
                            + "recvOneS DOUBLE, "
                            + "recvAll  DOUBLE, "
                            + "recvAllP DOUBLE, "
                            + "recvAllS DOUBLE, "
                            + "msgSent  BIGINT, "
                            + "msgSentP BIGINT, "
                            + "msgSentS BIGINT, "
                            + "msgRecv  BIGINT, "
                            + "msgRecvP BIGINT, "
                            + "msgRecvS BIGINT, "
                            + "consist  DOUBLE, "
                            + "consistP DOUBLE, "
                            + "consistS DOUBLE, "
                            + "PRIMARY KEY (round))");

            table.execute(
                    "CREATE TABLE messages ("
                            + "source      BIGINT, "
                            + "msgId       BIGINT, "
                            + "send        BIGINT, "
                            + "recv        BIGINT, "
                            + "sendPrimary BOOLEAN, "
                            + "recvPrimary BOOLEAN, "
                            + "roundSent   BIGINT, "
                            + "hop         BIGINT, "
                            + "PRIMARY KEY (source, msgId, send, recv, roundSent))");

            roundStatement = connection.prepareStatement(
                    "INSERT INTO rounds VALUES ("
                            + "?, ?, ?, ?, ?, "
                            + "?, ?, ?, ?, ?, "
                            + "?, ?, ?, ?, ?, "
                            + "?, ?, ?, ?)");

            messageStatement = connection.prepareStatement(
                    "INSERT INTO messages VALUES ("
                            + "?, ?, ?, ?, ?, "
                            + "?, ?, ?)");

            connection.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // Inserts the data of one simulation round
    public static void insertRound(
            long round,
            long nodes, long nodesP, long nodesS,
            double recvOne, double recvOneP, double recvOneS,
            double recvAll, double recvAllP, double recvAllS,
            long msgSent, long msgSentP, long msgSentS,
            long msgRecv, long msgRecvP, long msgRecvS,
            double consist, double consistP, double consistS
            ) {
        // Avoid errors if the database is not used
        if (!isInstantiated()) {
            return;
        }

        try {
            int index = 1;

            roundStatement.setLong(index++, round);
            roundStatement.setLong(index++, nodes);
            roundStatement.setLong(index++, nodesP);
            roundStatement.setLong(index++, nodesS);
            roundStatement.setDouble(index++, recvOne);
            roundStatement.setDouble(index++, recvOneP);
            roundStatement.setDouble(index++, recvOneS);
            roundStatement.setDouble(index++, recvAll);
            roundStatement.setDouble(index++, recvAllP);
            roundStatement.setDouble(index++, recvAllS);
            roundStatement.setLong(index++, msgSent);
            roundStatement.setLong(index++, msgSentP);
            roundStatement.setLong(index++, msgSentS);
            roundStatement.setLong(index++, msgRecv);
            roundStatement.setLong(index++, msgRecvP);
            roundStatement.setLong(index++, msgRecvS);
            roundStatement.setDouble(index++, consist);
            roundStatement.setDouble(index++, consistP);
            roundStatement.setDouble(index++, consistS);
            roundStatement.addBatch();

            // Respect the batch size limit
            roundBatchSize++;
            if (roundBatchSize >= maxBatchSize) {
                roundStatement.executeBatch();
                connection.commit();
                roundStatement.clearBatch();
                roundBatchSize = 0;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // Inserts the data of one message
    public static void insertMessage(TupleRowMessageTable row) {
        // Avoid errors if the database is not used
        if (!isInstantiated()) {
            return;
        }

        try {
            int index = 1;

            messageStatement.setLong(index++, row.getSource());
            messageStatement.setLong(index++, row.getMessageId());
            messageStatement.setLong(index++, row.getSender());
            messageStatement.setLong(index++, row.getReceiver());
            messageStatement.setBoolean(index++, row.isSenderPrimary());
            messageStatement.setBoolean(index++, row.isReceiverPrimary());
            messageStatement.setLong(index++, row.getRoundSent());
            messageStatement.setLong(index++, row.getHop());
            messageStatement.addBatch();

            // Respect the batch size limit
            messageBatchSize++;
            if (messageBatchSize >= maxBatchSize) {
                messageStatement.executeBatch();
                connection.commit();
                messageStatement.clearBatch();
                messageBatchSize = 0;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // Commits the last modifications and close the connection to the database
    public static void closeConnection() {
        // Avoid errors if the database is not used
        if (!isInstantiated()) {
            return;
        }

        if (connection != null) {
            try {
                // Commit the remaining inserts
                if (messageBatchSize != 0) {
                    messageStatement.executeBatch();
                }
                if (roundBatchSize != 0) {
                    roundStatement.executeBatch();
                }
                connection.commit();
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
