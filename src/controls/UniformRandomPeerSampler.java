package controls;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;
import utils.NeighboursLinkable;

public class UniformRandomPeerSampler implements Control {
    // Parameters from config file
    private static final String PAR_LINKABLE_ID_P = "linkableP";
    // = -1 if not set in config file, not always needed by the PMF-based degree setter
    private static final String PAR_VIEW_SIZE = "viewSize";
    private static int linkableIdP;
    private static int viewSize;

    private static boolean init;
    private static List<Node> allNodes;


    public UniformRandomPeerSampler(String prefix) {
        linkableIdP = Configuration.getPid(prefix + "." + PAR_LINKABLE_ID_P);
        viewSize = Configuration.getInt(prefix + "." + PAR_VIEW_SIZE, -1);
        init = true;
        allNodes = new ArrayList<>();
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public boolean execute() {
        // Fill the pool of available nodes at init time
        if (init) {
            init = false;
            fillNodesCollections();
        }
        fillAllNeighbourhoods();
        return false; // Continue the simulation
    }

    /*
     * Private/Protected methods
     */

    protected void fillNodesCollections() {
        for (int i = 0; i < Network.size(); i++) {
            allNodes.add(Network.get(i));
        }
    }

    // Create new neighbourhoods from scratch
    protected void fillAllNeighbourhoods() {
        for (int i = 0; i < Network.size(); i++) {
            NeighboursLinkable neighbours = (NeighboursLinkable)
                    Network.get(i).getProtocol(getLinkableIdP());
            fillOneNeighbourhood(neighbours, NeighboursLinkable.TYPE_UNIFORM, getViewSize(), allNodes);
        }
    }

    protected void fillOneNeighbourhood(NeighboursLinkable neighbours, int type, int nbNeighbours, List<Node> nodePool) {
        preprocessing(neighbours);
        addNeighbours(neighbours, nbNeighbours, nodePool);
        postprocessing(neighbours, nbNeighbours);
        neighbours.setType(type);
    }

    protected void preprocessing(NeighboursLinkable neighbours) {
        neighbours.resetNeighbours();
    }

    // Add the given number of new neighbours to the set, ensure unicity of nodes
    protected void addNeighbours(NeighboursLinkable neighbours, int nbNeighbours, List<Node> nodePool) {
        Set<Integer> indexSet = new HashSet<>(); // has fast contains()
        for (int i = 0; i < nbNeighbours && i < nodePool.size(); i++) {
            int random;
            do {
                random = CommonState.r.nextInt(nodePool.size());
            } while (indexSet.contains(random));
            indexSet.add(random);
            neighbours.addNeighbor(nodePool.get(random));
        }
    }

    protected void postprocessing(NeighboursLinkable neighbours, int nbNeighbours) {
        // Make sure the neighbours are of the right size (not too big)
        if (neighbours.getSize() > nbNeighbours) {
            String msg = "Wrong size of neighbours. Is " + neighbours.getSize()
            + " instead of " + nbNeighbours;
            throw new RuntimeException(this.getClass().getSimpleName() + ": " + msg);
        }
    }

    /*
     * Getters & setters
     */

    protected int getLinkableIdP() {
        return linkableIdP;
    }

    protected int getViewSize() {
        return viewSize;
    }

}
