package controls;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import applicationLayer.Update;
import gossip.GossipAbstract;
import peersim.config.Configuration;
import peersim.config.IllegalParameterException;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import utils.TupleUpdate;

public class SourceSetter implements Control {
    // Parameters from config file
    private static final String PAR_PROTOCOL_ID = "protocol";
    private static final String PAR_SOURCE_CLASS = "sourceClass";
    private static final String PAR_NUMBER_SOURCES = "sources";
    private static final String PAR_NUMBER_VARIABLES = "variables";
    private static final String PAR_DELAY = "delay";
    private static int protocolId;
    private static String sourceClass;
    private static int numberSources;
    private static int numberVariables;
    private static int delay;


    public SourceSetter(String prefix) {
        protocolId = Configuration.getPid(prefix + "." + PAR_PROTOCOL_ID);
        sourceClass = Configuration.getString(prefix + "." + PAR_SOURCE_CLASS);
        numberSources = Configuration.getInt(prefix + "." + PAR_NUMBER_SOURCES);
        numberVariables = Configuration.getInt(prefix + "." + PAR_NUMBER_VARIABLES);
        // Optional parameter, for the PMF-based dataset read from a csv file
        delay = Configuration.getInt(prefix + "." + PAR_DELAY, 1);

        sourceClass = sourceClass.toLowerCase();
        switch (sourceClass) {
        case "p": case "s": case "all":
            break;
        default:
            throw new IllegalParameterException(prefix + "." + PAR_NUMBER_SOURCES,
                    "Was given value " + sourceClass + " but only accepts: p s all");
        }
    }

    /*
     * Implementation of interfaces
     */

    // Only executed at init time
    @Override
    public boolean execute() {
        initQueues();
        List<GossipAbstract> sourcePool = selectPotentialSources();
        List<GossipAbstract> sources = selectSources(sourcePool);
        List<Integer> rounds = selectRounds();
        initUpdates(sources, rounds);
        return false; // Continue the simulation
    }

    // Initialize priority queue for updates
    protected void initQueues() {
        for (int i = 0; i < Network.size(); i++) {
            GossipAbstract node = (GossipAbstract)
                    Network.get(i).getProtocol(protocolId);
            PriorityQueue<TupleUpdate> pqueue = new PriorityQueue<>();
            node.setUpdates(pqueue);
        }
    }

    // Select potential sources based on node class
    protected List<GossipAbstract> selectPotentialSources() {
        List<GossipAbstract> nodes = new ArrayList<>();
        for (int i = 0; i < Network.size(); i++) {
            GossipAbstract node = (GossipAbstract)
                    Network.get(i).getProtocol(protocolId);

            if (GossipAbstract.CLASS_UNIFORM == node.getClasse()
                    || sourceClass.equals("all")) {
                nodes.add(node);
            } else if (GossipAbstract.CLASS_PRIMARY == node.getClasse()
                    && sourceClass.equals("p")) {
                nodes.add(node);
            } else if (GossipAbstract.CLASS_SECONDARY == node.getClasse()
                    && sourceClass.equals("s")) {
                nodes.add(node);
            }
        }
        System.err.println(this.getClass().getSimpleName() + ": number of potential"
                + " sources: " + nodes.size() + " / " + Network.size());
        return nodes;
    }

    // Select actual sources from the source pool
    protected List<GossipAbstract> selectSources(List<GossipAbstract> sourcePool) {
        List<GossipAbstract> sources = new ArrayList<>();
        for (int s = 0; s < getNumberSources(); s++) {
            int r = CommonState.r.nextInt(sourcePool.size());
            sources.add(sourcePool.get(r));
        }
        return sources;
    }

    // Select rounds updates are being created at
    protected List<Integer> selectRounds() {
        List<Integer> rounds = new ArrayList<>();
        int r = 0; // First update on first round
        for (int s = 0; s < getNumberSources(); s++) {
            rounds.add(r);
            r += delay;
        }
        return rounds;
    }

    // Create updates/UC-variables every "delay" rounds
    // If there are more sources than variables, apply in round robin to sources
    protected void initUpdates(List<GossipAbstract> sources, List<Integer> rounds) {
        for (int s = 0; s < sources.size(); s++) {
            GossipAbstract source = sources.get(s);
            int round = rounds.get(s);
            int index = s % numberVariables;

            // value must be != 0
            int value;
            do { value = CommonState.r.nextInt(); } while (value == 0);

            Update update = new Update(value);
            source.addUpdate(new TupleUpdate(round, index, update));
        }
    }

    /*
     * Getters & setters
     */

    public static int getNumberSources() {
        return numberSources;
    }

    public static int getNumberVariables() {
        return numberVariables;
    }
}
