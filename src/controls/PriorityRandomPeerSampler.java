package controls;

import java.util.ArrayList;
import java.util.List;

import gossip.GossipAbstract;
import peersim.config.Configuration;
import peersim.core.Network;
import peersim.core.Node;
import utils.NeighboursLinkable;

public class PriorityRandomPeerSampler extends UniformRandomPeerSampler {
    // Parameters from config file
    private static final String PAR_PROTOCOL_ID = "protocol";
    private static final String PAR_LINKABLE_ID_S = "linkableS";
    private static final String PAR_LINKABLE_ID_PS = "linkablePS";
    private static final String PAR_ALPHA = "alpha";
    private static final String PAR_BETA = "beta";
    private static int protocolId;
    // = -1 if not set in config file, not always needed by the PMF-based degree setter
    private static int linkableIdS;
    // = -1 if not set in config file, not needed for duplicate-based gossip
    private static int linkableIdPS;
    private static int alpha;
    private static int beta;

    private static List<Node> primaryNodes;
    private static List<Node> secondaryNodes;


    public PriorityRandomPeerSampler(String prefix) {
        super(prefix);

        // Static variables
        protocolId = Configuration.getPid(prefix + "." + PAR_PROTOCOL_ID);
        linkableIdS = Configuration.getPid(prefix + "." + PAR_LINKABLE_ID_S, -1);
        linkableIdPS = Configuration.getPid(prefix + "." + PAR_LINKABLE_ID_PS, -1);
        alpha = Configuration.getInt(prefix + "." + PAR_ALPHA, 0);
        beta = Configuration.getInt(prefix + "." + PAR_BETA, 0);
        primaryNodes = new ArrayList<>();
        secondaryNodes = new ArrayList<>();
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public boolean execute() {
        return super.execute();
    }

    /*
     * Private/Protected methods
     */

    // Fill the primary/secondary nodes collections (to know which nodes are P or S)
    protected void fillNodesCollections() {
        for (int i = 0; i < Network.size(); i++) {
            Node node = Network.get(i);
            int classe = ((GossipAbstract) node.getProtocol(protocolId)).getClasse();

            if (classe == GossipAbstract.CLASS_PRIMARY
                    // For the PMF-based degree distribution, it can be used with uniform gossip
                    || classe == GossipAbstract.CLASS_UNIFORM) {
                getPrimaryNodes().add(node);
            } else {
                getSecondaryNodes().add(node);
            }
        }
    }

    // Fill neighboursP, neighboursS and neighboursPS with random nodes
    // Take alpha and beta into account
    protected void fillAllNeighbourhoods() {
        for (int i = 0; i < Network.size(); i++) {
            int type;
            // Fill neighboursP with (viewSize - alpha) P nodes and alpha S nodes
            NeighboursLinkable neighboursP = (NeighboursLinkable)
                    Network.get(i).getProtocol(getLinkableIdP());
            type = NeighboursLinkable.TYPE_PRIMARY;
            if (alpha != 0) type = NeighboursLinkable.TYPE_PRIMARY_WITH_DIVERSITY;
            fillOneMixedNeighbourhood(neighboursP, type, getViewSize(), alpha);

            // Fill neighboursS with beta P nodes and (viewSize - beta) S nodes
            if (linkableIdS != -1) {
                NeighboursLinkable neighboursS = (NeighboursLinkable)
                        Network.get(i).getProtocol(getLinkableIdS());
                type = NeighboursLinkable.TYPE_SECONDARY;
                if (beta != 0) type = NeighboursLinkable.TYPE_SECONDARY_WITH_DIVERSITY;
                fillOneMixedNeighbourhood(neighboursS, type, getViewSize(), getViewSize() - beta);
            }

            // Fill neighboursPS (half P, half S)
            if (linkableIdPS != -1) {
                NeighboursLinkable neighboursPS = (NeighboursLinkable)
                        Network.get(i).getProtocol(getLinkableIdPS());
                type = NeighboursLinkable.TYPE_PRIMARY_SECONDARY;
                fillOneMixedNeighbourhood(neighboursPS, type, getViewSize(), getViewSize() / 2);
            }
        }
    }

    // Fill the given neighbours with random P nodes and sNodes random S nodes
    protected void fillOneMixedNeighbourhood(NeighboursLinkable neighbours, int type, int nbNeighbours, int nbSecondaries) {
        preprocessing(neighbours);
        addNeighbours(neighbours, nbNeighbours - nbSecondaries, getPrimaryNodes());
        addNeighbours(neighbours, nbSecondaries, getSecondaryNodes());
        postprocessing(neighbours, nbNeighbours);
        neighbours.setType(type);
    }

    /*
     * Getters & setters
     */

    protected int getLinkableIdS() {
        return linkableIdS;
    }

    protected int getLinkableIdPS() {
        return linkableIdPS;
    }

    protected List<Node> getPrimaryNodes() {
        return primaryNodes;
    }

    protected List<Node> getSecondaryNodes() {
        return secondaryNodes;
    }
}
