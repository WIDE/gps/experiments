package controls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import gossip.GossipAbstract;
import gossip.UniformGossip;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;

public class PrimarySetter implements Control {
    // Parameters from config file
    private static final String PAR_PROTOCOL_ID = "protocol";
    private static final String PAR_DENSITY = "density";
    private static int protocolId;
    private static int nbPrimaries;


    public PrimarySetter(String prefix) {
        protocolId = Configuration.getPid(prefix + "." + PAR_PROTOCOL_ID);
        double density = Configuration.getDouble(prefix + "." + PAR_DENSITY);
        nbPrimaries = (int) Math.round(density * Network.size());
        System.err.println(this.getClass().getSimpleName() + ": number of primary nodes: " + nbPrimaries);
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public boolean execute() {
        // No need to put primary and secondary nodes in the uniform gossip
        if (Network.get(0).getProtocol(protocolId) instanceof UniformGossip) {
            // Continue the simulation
            return false;
        }

        // Have the nodes index shuffled
        List<Integer> indexList = new ArrayList<>();
        for (int i = 0; i < Network.size(); i++) {
            indexList.add(i);
        }
        Collections.shuffle(indexList, CommonState.r);

        // Set the first nodes of the shuffled index list as primary and
        // the remaining as secondary
        for (int j = 0; j < nbPrimaries; j++) {
            GossipAbstract ga = (GossipAbstract)
                    Network.get(indexList.get(j)).getProtocol(protocolId);
            ga.setClasse(GossipAbstract.CLASS_PRIMARY);
        }
        for (int j = nbPrimaries; j < Network.size(); j++) {
            GossipAbstract ga = (GossipAbstract)
                    Network.get(indexList.get(j)).getProtocol(protocolId);
            ga.setClasse(GossipAbstract.CLASS_SECONDARY);
        }

        // Continue the simulation
        return false;
    }


    /*
     * Getters & setters
     */

    public static int getNbPrimaries() {
        return nbPrimaries;
    }
}
