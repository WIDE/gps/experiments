package gossip;

import peersim.config.Configuration;
import peersim.core.Node;
import utils.NeighboursLinkable;

public abstract class PriorityGossipAbstract extends GossipAbstract {
    // Parameters from config file
    private static final String PAR_LINKABLE_ID_S = "linkableS";
    private static int linkableIdS;

    private NeighboursLinkable neighboursS;


    protected PriorityGossipAbstract(String prefix) {
        super(prefix);

        // Static variables
        linkableIdS = Configuration.getPid(prefix + "." + PAR_LINKABLE_ID_S);

        // These variables will be set later
        this.neighboursS = null;
    }

    // Constructor by copy
    protected PriorityGossipAbstract(PriorityGossipAbstract dolly) {
        super(dolly);

        // Bad cloning but not important, these variables will be set later
        this.neighboursS = dolly.neighboursS;
    }

    /*
     * Private/Protected methods
     */

    // Some initialisation needed on the first round
    @Override
    protected void init(Node node, int protocolId) {
        neighboursS = (NeighboursLinkable) node.getProtocol(linkableIdS);

        // Initialise other stuff
        super.init(node, protocolId);
    }

    /*
     * Getters & setters
     */

    protected NeighboursLinkable getNeighboursS() {
        return neighboursS;
    }

}
