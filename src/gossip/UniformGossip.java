package gossip;

import peersim.core.Node;
import utils.NetworkMessage;

public class UniformGossip extends GossipAbstract {

    public UniformGossip(String prefix) {
        super(prefix);
        setClasse(GossipAbstract.CLASS_UNIFORM);
    }

    // Constructor by copy
    public UniformGossip(UniformGossip dolly) {
        super(dolly);
    }

    // Redefine clone() to have proper collection cloning and no exception
    @Override
    public Object clone() {
        return new UniformGossip(this);
    }

    /*
     * Implementation of interfaces
     */

    // On receive
    @Override
    public void processEvent(Node node, int protocolId, Object event) {
        NetworkMessage msg = (NetworkMessage) event;

        // Update the number of times the message has been received
        long receivedCounter = updateReceivedMessages(msg);

        // Ignore if already received
        if (receivedCounter > 1) {
            return;
        }

        // The message is relevant, the node copies it for further dissemination
        msg = copyMessage(msg);

        // Deliver on first receipt
        deliver(msg);

        // Dissemination
        gossip(msg);
    }

}
