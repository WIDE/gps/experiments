package gossip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;

import applicationLayer.PairAppMessageIndex;
import applicationLayer.UCAbstract;
import applicationLayer.UCInfinity;
import controls.DatabaseHandler;
import controls.Observer;
import controls.SourceSetter;
import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;
import utils.IncrementableCounter;
import utils.NeighboursLinkable;
import utils.NetworkMessage;
import utils.TupleRowMessageTable;
import utils.TupleUpdate;

public abstract class GossipAbstract implements CDProtocol, EDProtocol {
    // Possible node class
    private static final String classes[] =
        {"CLASS_NONE", "CLASS_UNIFORM", "CLASS_PRIMARY", "CLASS_SECONDARY"};
    public static final int CLASS_NONE = 0; // For temporary usage
    public static final int CLASS_UNIFORM = 1;
    public static final int CLASS_PRIMARY = 2;
    public static final int CLASS_SECONDARY = 3;

    // Parameters from config file
    private static final String PAR_TRANSPORT_ID = "transport";
    private static final String PAR_LINKABLE_ID_P = "linkableP";
    private static final String PAR_FANOUT = "fanout";
    private static final String PAR_SCENARIO = "scenario";
    private static int transportId;
    private static int linkableIdP;
    private static int fanout;
    private static int scenario;

    // Variables for the algorithm and statistics
    private boolean init;
    private int classe;
    private long messagesSentAsSourceCounter;
    private long messagesSentCounter;
    private Map<Long, IncrementableCounter> receivedMessages;
    private Collection<Long> latenciesDeliveredMessages;
    private Map<String, IncrementableCounter> nbGossipsPerClass;

    // Temporary store messages before they are inserted in the database
    private Collection<TupleRowMessageTable> messageStore;

    // Peersim objects
    private int protocolId;
    private Node self;
    private Transport transport;
    private NeighboursLinkable neighboursP;

    // Application layer: consistent object(s)
    private List<UCAbstract> replicatedDatas;
    // Contains a list of updates to apply on the specified round
    private Queue<TupleUpdate> updates;


    // Default constructor, used to create the first instance of this class, the
    // other instances are created by cloning
    protected GossipAbstract(String prefix) {
        // Static variables
        transportId = Configuration.getPid(prefix + "." + PAR_TRANSPORT_ID);
        linkableIdP = Configuration.getPid(prefix + "." + PAR_LINKABLE_ID_P);
        fanout = Configuration.getInt(prefix + "." + PAR_FANOUT);
        scenario = Configuration.getInt(prefix + "." + PAR_SCENARIO);

        // Local variables
        this.init = true;
        this.classe = GossipAbstract.CLASS_NONE;
        this.messagesSentAsSourceCounter = 0;
        this.messagesSentCounter = 0;
        this.receivedMessages = factoryReceivedMessagesEmpty();
        this.latenciesDeliveredMessages = factoryLatenciesDeliveredMessageseEmpty();
        this.nbGossipsPerClass = factoryNbGossipsPerClassEmpty();
        this.messageStore = factoryMessageStoreEmpty();

        // These variables will be set later
        this.protocolId = -1;
        this.self = null;
        this.transport = null;
        this.neighboursP = null;
        this.replicatedDatas = null;
        this.updates = null;
    }

    // Constructor by copy
    protected GossipAbstract(GossipAbstract dolly) {
        // Local variables
        this.init = dolly.init;
        this.classe = dolly.classe;
        this.messagesSentAsSourceCounter = dolly.messagesSentAsSourceCounter;
        this.messagesSentCounter = dolly.messagesSentCounter;
        this.receivedMessages = factoryReceivedMessages(dolly.receivedMessages);
        this.latenciesDeliveredMessages = factoryLatenciesDeliveredMessages(dolly.latenciesDeliveredMessages);
        this.nbGossipsPerClass = factoryNbGossipsPerClass(dolly.nbGossipsPerClass);
        this.messageStore = factoryMessageStore(dolly.messageStore);

        // Bad cloning but not important, these variables will be set later
        this.protocolId = dolly.protocolId;
        this.self = dolly.self;
        this.transport = dolly.transport;
        this.neighboursP = dolly.neighboursP;
        this.replicatedDatas = dolly.replicatedDatas;
        this.updates = dolly.updates;
    }

    // Redefine clone() to have no compilation error
    @Override
    public abstract Object clone();

    /*
     * Factories
     */

    private Map<Long, IncrementableCounter> factoryReceivedMessages(Map<Long, IncrementableCounter> dolly) {
        return new HashMap<>(dolly);
    }

    private Map<Long, IncrementableCounter> factoryReceivedMessagesEmpty() {
        return factoryReceivedMessages(Collections.<Long, IncrementableCounter> emptyMap());
    }

    private Collection<Long> factoryLatenciesDeliveredMessages(Collection<Long> dolly) {
        return new ArrayList<>(dolly);
    }

    private Collection<Long> factoryLatenciesDeliveredMessageseEmpty() {
        return factoryLatenciesDeliveredMessages(Collections.<Long> emptyList());
    }

    private Map<String, IncrementableCounter> factoryNbGossipsPerClass(Map<String, IncrementableCounter> dolly) {
        // Here, the type matters
        Map<String, IncrementableCounter> map = new HashMap<>();
        for (Entry<String, IncrementableCounter> entry: dolly.entrySet()) {
            map.put(new String(entry.getKey()),
                    new IncrementableCounter(entry.getValue()));
        }
        return map;
    }

    // Not really empty, it needs to contains some <k, v> by default
    private Map<String, IncrementableCounter> factoryNbGossipsPerClassEmpty() {
        // Here, the type doesn't matter because the map will get copied anyway
        Map<String, IncrementableCounter> map = new HashMap<>();
        map.put("p2p", new IncrementableCounter(0));
        map.put("p2s", new IncrementableCounter(0));
        map.put("s2p", new IncrementableCounter(0));
        map.put("s2s", new IncrementableCounter(0));
        return factoryNbGossipsPerClass(map);
    }

    private Collection<TupleRowMessageTable> factoryMessageStore(Collection<TupleRowMessageTable> dolly) {
        return new ArrayList<>(dolly);
    }

    private Collection<TupleRowMessageTable> factoryMessageStoreEmpty() {
        return factoryMessageStore(Collections.<TupleRowMessageTable> emptyList());
    }

    /*
     * Implementation of interfaces
     */

    // Called by the scheduler every cycle
    @Override
    public void nextCycle(Node node, int protocolId) {
        if (init) {
            init = !init;
            init(node, protocolId);
        }

        // Apply updates of the current round
        TupleUpdate item = updates.peek();
        while (item != null && item.getRound() <= CommonState.getIntTime()) {
            replicatedDatas.get(item.getIndex()).update(item.getUpdate());
            // Remove the update from the queue
            updates.remove();
            // Prepare for next iteration
            item = updates.peek();
        }
    }

    // On receive
    @Override
    public abstract void processEvent(Node node, int protocolId, Object event);

    /*
     * Public methods
     */

    // Create a message from the given content, and gossip it
    public void sendNewMessage(Object content) {
        NetworkMessage msg = new NetworkMessage(self, content);
        msg.setMessageId(messagesSentAsSourceCounter);
        messagesSentAsSourceCounter++;

        // Add it to the received messages set, to not disseminate it again when
        // received from another node
        updateReceivedMessages(msg);
        gossip(msg, neighboursP);
        deliver(msg);
    }

    /*
     * Private/Protected methods
     */

    // Some initialisation needed on the first round
    protected void init(Node node, int protocolId) {
        self = node;
        this.protocolId = protocolId;
        transport = (Transport) node.getProtocol(transportId);
        neighboursP = (NeighboursLinkable) node.getProtocol(linkableIdP);

        // Initial state for the UC-variables
        Object initialState = null;
        if (scenario == UCAbstract.SCENARIO_1) initialState = 0;
        if (scenario == UCAbstract.SCENARIO_2) initialState = new ArrayList<>();

        // Initialize the UC-variables on each node
        replicatedDatas = new ArrayList<>();
        for (int i = 0; i < SourceSetter.getNumberVariables(); i++) {
            UCAbstract data;
            data = new UCInfinity(self.getID(), i, this, scenario, initialState);
            // data = new UCZero(self.getID(), i, this, scenario, initialState); // TODO
            // data = new UCk(self.getID(), i, this, scenario, initialState, 10); // TODO
            replicatedDatas.add(data);
        }
    }

    // Copy a message and update it with the current node's infos
    protected NetworkMessage copyMessage(NetworkMessage msg) {
        // Each receiving node will have the same message instance (to save space)
        // and they duplicate the message only if they gossip it
        // NOTE: check that no node modify messages before copying them
        msg = new NetworkMessage(msg);

        // Update message path and hop counter
        msg.incrementHop();
        msg.addToPath(self);

        return msg;
    }

    // Send the given message to "fanout" nodes in the given neighbours with given tag
    protected void gossip(NetworkMessage msg, NeighboursLinkable neighbours, int tag) {
        msg.setTag(tag);

        // Pick "fanout" nodes randomly from the RPS view by shuffling the view
        // and taking the firsts "fanout" nodes from it
        neighbours.shuffle();
        for (int i = 0; i < fanout && i < neighbours.getSize(); i++) {
            messagesSentCounter++;
            Node receiver = neighbours.getNeighbor(i);
            transport.send(self, receiver, msg, protocolId);
            // Store the message for database insertion
            storeMessage(msg, receiver);
        }

        // Update the map containing the number of gossip made from and to classes
        Map<String, IncrementableCounter> map = nbGossipsPerClass;
        int type = neighbours.getType();
        if (getClasse() == GossipAbstract.CLASS_PRIMARY) {
            if (type == NeighboursLinkable.TYPE_PRIMARY)   map.get("p2p").increment();
            if (type == NeighboursLinkable.TYPE_SECONDARY) map.get("p2s").increment();
        } else if (getClasse() == GossipAbstract.CLASS_SECONDARY) {
            if (type == NeighboursLinkable.TYPE_PRIMARY)   map.get("s2p").increment();
            if (type == NeighboursLinkable.TYPE_SECONDARY) map.get("s2s").increment();
        }
        else if (getClasse() == GossipAbstract.CLASS_UNIFORM) {
            map.get("p2p").increment();
        }

        // Make sure the observer works next round
        Observer.thereIsAction();
    }

    // Send the given message to "fanout" nodes in the given neighbours with default tag
    protected void gossip(NetworkMessage msg, NeighboursLinkable neighbours) {
        gossip(msg, neighbours, NetworkMessage.TAG_NONE);
    }

    // Send the given message to "fanout" nodes in the default neighbours
    protected void gossip(NetworkMessage msg) {
        gossip(msg, neighboursP);
    }

    // Increment by 1 the number of times a message has been received
    protected long updateReceivedMessages(NetworkMessage msg) {
        long hash = Objects.hash(msg.getSource(), msg.getMessageId());

        return IncrementableCounter.incrementMap(receivedMessages, hash);
    }

    // Deliver the message to the application (or print to debug)
    protected void deliver(NetworkMessage msg) {
        if (! (msg.getContent() instanceof PairAppMessageIndex)) {
            throw new RuntimeException("Error: " + this.getClass().getName()
                    + ".deliver(): the message is not of the right class");
        }
        PairAppMessageIndex pair = (PairAppMessageIndex) msg.getContent();
        replicatedDatas.get(pair.getIndex()).onReceive(pair.getAppMessage());

        // Store the latency of the delivered messages
        latenciesDeliveredMessages.add(msg.getHop());

        // Make sure the observer works next round
        Observer.thereIsAction();

        // NOTE: comment to debug
        if (true) return;

        System.out.println(
                "\tRound: " + CommonState.getIntTime()
                + "\t" + self.getID() + "\t<- " + msg
                + " " + getClasseString(getClasse()));
    }

    // Add a message to the message store for database insertion
    private void storeMessage(NetworkMessage msg, Node receiver) {
        // Don't need to store the message if the database isn't used
        if (!DatabaseHandler.isInstantiated()) {
            return;
        }

        boolean senderPrimary = false;
        boolean receiverPrimary = false;

        // Nodes in uniform gossip are considered as primary
        if (getClasse() == GossipAbstract.CLASS_UNIFORM
                || getClasse() == GossipAbstract.CLASS_PRIMARY) {
            senderPrimary = true;
        }

        GossipAbstract ga = (GossipAbstract) receiver.getProtocol(protocolId);
        if (ga.getClasse() == GossipAbstract.CLASS_UNIFORM
                || ga.getClasse() == GossipAbstract.CLASS_PRIMARY) {
            receiverPrimary = true;
        }

        TupleRowMessageTable row = new TupleRowMessageTable(
                msg.getSource().getID(), msg.getMessageId(),
                self.getID(), receiver.getID(),
                senderPrimary, receiverPrimary,
                CommonState.getTime(), msg.getHop());
        messageStore.add(row);
    }

    /*
     * Getters & setters
     */

    public static int getScenario() {
        return scenario;
    }

    protected int getTransportId() {
        return transportId;
    }

    protected int getLinkableIdP() {
        return linkableIdP;
    }

    protected NeighboursLinkable getNeighboursP() {
        return neighboursP;
    }

    // Get the class name from it's integer value
    public String getClasseString(int classe) {
        return classes[classe];
    }

    public int getClasse() {
        return classe;
    }

    public void setClasse(int classe) {
        this.classe = classe;
    }

    public Map<Long, IncrementableCounter> getReceivedMessages() {
        return receivedMessages;
    }

    public long getNumberOfMessagesReceived() {
        long out = 0;
        for (IncrementableCounter v: receivedMessages.values()) {
            out += v.getValue();
        }
        return out;
    }

    public long getNumberOfDifferentMessagesReceived() {
        return receivedMessages.size();
    }

    public Collection<Long> getLatenciesDeliveredMessages() {
        return latenciesDeliveredMessages;
    }

    public Map<String, IncrementableCounter> getNumberGossipsPerClass() {
        return nbGossipsPerClass;
    }

    public void emptyNumberGossipsPerClass() {
        nbGossipsPerClass = factoryNbGossipsPerClassEmpty();
    }

    public long getNumberOfMessagesSent() {
        return messagesSentCounter;
    }

    public long getNumberOfMessagesSentAsSource() {
        return messagesSentAsSourceCounter;
    }

    public Collection<TupleRowMessageTable> getMessageStore() {
        return messageStore;
    }

    public void emptyMessageStore() {
        messageStore = factoryMessageStoreEmpty();
    }

    public List<UCAbstract> getReplicatedDatas() {
        return replicatedDatas;
    }

    public void setUpdates(PriorityQueue<TupleUpdate> updates) {
        this.updates = updates;
    }

    public void addUpdate(TupleUpdate update) {
        this.updates.add(update);
    }
}
