package gossip;

import peersim.config.Configuration;
import peersim.core.Node;
import utils.NeighboursLinkable;
import utils.NetworkMessage;

public class PriorityGossipHop extends PriorityGossipAbstract {
    // Parameters from config file
    private static final String PAR_LINKABLE_ID_PS = "linkablePS";
    private static final String PAR_HOP_THRESHOLD_1 = "hopThreshold1";
    private static final String PAR_HOP_THRESHOLD_2 = "hopThreshold2";
    private static int linkableIdPS;
    private static long hopThreshold1;
    private static long hopThreshold2;

    // Peersim objects
    private NeighboursLinkable neighboursPS;


    public PriorityGossipHop(String prefix) {
        super(prefix);

        // Static variables
        hopThreshold1 = Configuration.getLong(prefix + "." + PAR_HOP_THRESHOLD_1);
        hopThreshold2 = Configuration.getLong(prefix + "." + PAR_HOP_THRESHOLD_2);
        linkableIdPS = Configuration.getPid(prefix + "." + PAR_LINKABLE_ID_PS);

        // These variables will be set later
        this.neighboursPS = null;
    }

    // Constructor by copy
    public PriorityGossipHop(PriorityGossipHop dolly) {
        super(dolly);

        // Bad cloning but not important, these variables will be set later
        this.neighboursPS = dolly.neighboursPS;
    }

    // Redefine clone() to have proper collection cloning and no exception
    @Override
    public Object clone() {
        return new PriorityGossipHop(this);
    }

    /*
     * Implementation of interfaces
     */

    // On receive
    @Override
    public void processEvent(Node node, int protocolId, Object event) {
        NetworkMessage msg = (NetworkMessage) event;

        // Update the number of times the message has been received
        long receivedCounter = updateReceivedMessages(msg);

        // Ignore if already received
        if (receivedCounter > 1) {
            return;
        }

        // The message is relevant, the node copies it for further dissemination
        msg = copyMessage(msg);

        // Deliver on first receipt
        deliver(msg);

        // Dissemination
        if (msg.getHop() < hopThreshold1) {
            // Send to primary if new message
            gossip(msg, getNeighboursP());
        } else if (msg.getHop() < hopThreshold2) {
            // Send to primary & secondary if older message
            gossip(msg, neighboursPS);
        } else {
            // Send to secondary if old message
            gossip(msg, getNeighboursS());
        }
    }

    /*
     * Private/Protected methods
     */

    // Some initialisation needed on the first round
    @Override
    protected void init(Node node, int protocolId) {
        neighboursPS = (NeighboursLinkable) node.getProtocol(linkableIdPS);

        // Initialise other stuff
        super.init(node, protocolId);
    }
}
