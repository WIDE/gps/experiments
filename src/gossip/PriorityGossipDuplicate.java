package gossip;

import peersim.config.Configuration;
import peersim.core.Node;
import utils.NeighboursLinkable;
import utils.NetworkMessage;

public class PriorityGossipDuplicate extends PriorityGossipAbstract {
    // Parameters from config file
    private static final String PAR_TAGS_ACTIVATED = "tagsActivated";
    private static final String PAR_DUP_SEND_TO_P = "dupSendP";
    private static final String PAR_DUP_SEND_TO_S = "dupSendS";
    private static boolean tagsActivated;
    private static int numberDupsToSendToP;
    private static int numberDupsToSendToS;
    private static int maxDup;


    public PriorityGossipDuplicate(String prefix) {
        super(prefix);

        // Static variables
        tagsActivated = Configuration.getBoolean(prefix + "." + PAR_TAGS_ACTIVATED);
        numberDupsToSendToP = Configuration.getInt(prefix + "." + PAR_DUP_SEND_TO_P, 1);
        numberDupsToSendToS = Configuration.getInt(prefix + "." + PAR_DUP_SEND_TO_S, 2);
        maxDup = Math.max(numberDupsToSendToP, numberDupsToSendToS);
    }

    // Constructor by copy
    public PriorityGossipDuplicate(PriorityGossipDuplicate dolly) {
        super(dolly);
    }

    // Redefine clone() to have proper collection cloning and no exception
    @Override
    public Object clone() {
        return new PriorityGossipDuplicate(this);
    }

    /*
     * Implementation of interfaces
     */

    // On receive
    @Override
    public void processEvent(Node node, int pid, Object event) {
        NetworkMessage msg = (NetworkMessage) event;

        // Update the number of times the message has been received
        long receivedCounter = updateReceivedMessages(msg);

        // Ignore too old messages
        if (receivedCounter > maxDup) {
            return;
        }

        // The message is relevant, the node copies it for further dissemination
        msg = copyMessage(msg);

        // Deliver on first receipt
        if (receivedCounter == 1) {
            deliver(msg);
        }

        // Tags are not used, we use the simple algo
        if (msg.getTag() == NetworkMessage.TAG_NONE) {
            // Send to primary if new message
            if (receivedCounter == numberDupsToSendToP) {
                gossip(msg, getNeighboursP());
            }
            // Send to secondary if old message
            if (receivedCounter == numberDupsToSendToS) {
                gossip(msg, getNeighboursS());
            }
        }

        // Tags are used, we use the more complicated algo
        // If alpha = beta = 0, P nodes gossip once to P then to S
        // and S nodes gossip once to S
        else {
            if (receivedCounter == numberDupsToSendToP) {
                if (getClasse() == GossipAbstract.CLASS_PRIMARY) {
                    gossip(msg, getNeighboursP());
                } else {
                    if (msg.getTag() == NetworkMessage.TAG_TO_PRIMARY) {
                        gossip(msg, getNeighboursP());
                    } else if (msg.getTag() == NetworkMessage.TAG_TO_SECONDARY) {
                        gossip(msg, getNeighboursS());
                    }
                }
            }
            if (receivedCounter == numberDupsToSendToS
                    && getClasse() == GossipAbstract.CLASS_PRIMARY) {
                gossip(msg, getNeighboursS());
            }
        }
    }

    /*
     * Private/Protected methods
     */

    // Send the given message to the given neighbours with the tag associated
    // to each neighbourhood (if tags are activated)
    protected void gossip(NetworkMessage msg, NeighboursLinkable neighbours) {
        // Use TAG_NONE by default (desactivate the tags)
        int tag = NetworkMessage.TAG_NONE;

        if (tagsActivated) {
            if (neighbours.equals(getNeighboursP())) {
                tag = NetworkMessage.TAG_TO_PRIMARY;
            } else if (neighbours.equals(getNeighboursS())) {
                tag = NetworkMessage.TAG_TO_SECONDARY;
            }
        }

        gossip(msg, neighbours, tag);
    }

}
