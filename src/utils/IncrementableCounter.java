package utils;

import java.util.Map;

// Avoid doing a lot of hashmap.put() when incrementing counters
public class IncrementableCounter {
    private long counter;

    public IncrementableCounter(long counter) {
        this.counter = counter;
    }

    public IncrementableCounter(IncrementableCounter dolly) {
        this.counter = dolly.counter;
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public String toString() {
        return Long.toString(counter);
    }

    /*
     * Public methods
     */

    // Increment (and create if necessary) the value corresponding to the
    // given key in the given map
    // When this class is used, it's almost always to increment a value within a map
    public static <E> long incrementMap(Map<E, IncrementableCounter> map, E key) {
        IncrementableCounter counter;
        if (map.containsKey(key)) {
            counter = map.get(key);
            counter.increment();
        } else {
            counter = new IncrementableCounter(1);
            map.put(key, counter);
        }
        return counter.getValue();
    }

    /*
     * Getters & setters
     */

    public long getValue() {
        return counter;
    }

    public void increment() {
        counter++;
    }

}
