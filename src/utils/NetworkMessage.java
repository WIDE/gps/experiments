package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import peersim.core.Node;

public class NetworkMessage {
    private static final String tags[] =
        {"TAG_NONE", "TAG_TO_PRIMARY", "TAG_TO_SECONDARY"};
    public static final int TAG_NONE = 0;
    public static final int TAG_TO_PRIMARY = 1;
    public static final int TAG_TO_SECONDARY = 2;

    // Path contains the list of senders, the source is at index 0
    private List<Node> path;
    private long messageId;
    private Object content;
    private int tag;
    private long hop;


    public NetworkMessage(Node src, Object content, int tag) {
        this.path = factoryPathEmpty();
        this.path.add(src);
        this.content = content;
        this.tag = tag;
        this.hop = 0;
    }

    public NetworkMessage(Node src, Object content) {
        this(src, content, TAG_NONE);
    }

    // Constructor by copy
    public NetworkMessage(NetworkMessage dolly) {
        this.path = factoryPath(dolly.path);
        this.messageId = dolly.messageId;
        this.content = dolly.content;
        this.tag = dolly.tag;
        this.hop = dolly.hop;
    }

    // Redefine clone() to have proper collection cloning and no exception
    @Override
    public Object clone() {
        return new NetworkMessage(this);
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public String toString() {
        String out = Global.format("(source: %d,\tsender: %d,"
                + "\tid: %d,\thop:%d,\ttag:%s)",
                getSource().getID(), getLastSender().getID(),
                getMessageId(), getHop(), getTagString(getTag()));

        return out;
    }

    /*
     * Factories
     */

    private List<Node> factoryPath(List<Node> dolly) {
        return new ArrayList<>(dolly);
    }

    private List<Node> factoryPathEmpty() {
        return factoryPath(Collections.<Node> emptyList());
    }

    /*
     * Getters & setters
     */

    public List<Node> getPath() {
        return path;
    }

    public Node getSource() {
        return path.get(0);
    }

    public Node getLastSender() {
        return path.get(path.size()-1);
    }

    public void addToPath(Node node) {
        path.add(node);
    }

    public Object getContent() {
        return content;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public long getHop() {
        return hop;
    }

    public void incrementHop() {
        this.hop++;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    // Get the tag name from it's integer value
    public String getTagString(int tag) {
        return tags[tag];
    }

}
