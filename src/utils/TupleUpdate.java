package utils;

import applicationLayer.Update;

// Used by the source setter and gossip abstract to sort the updates by round
public class TupleUpdate implements Comparable<TupleUpdate> {
    private int round;
    private int index;
    private Update update;
    private int hash;


    public TupleUpdate(int round, int index, Update update) {
        this.round = round;
        this.index = index;
        this.update = update;
        // Cache the hash to avoid computing it in each comparison
        this.hash = update.hashCode();
    }

    /*
     * Implementation of interfaces
     */

    // Order the tuples by round first, then index, then update
    @Override
    public int compareTo(TupleUpdate o) {
        if (o == null) throw new NullPointerException();

        // Order by round
        if (this.round < o.round) return -1;
        if (this.round > o.round) return 1;

        // Same round, order by index
        if (this.index < o.index) return -1;
        if (this.index > o.index) return 1;

        // Same round, same index, order by update hash
        if (this.hash < o.hash) return -1;
        if (this.hash > o.hash) return 1;

        // Rounds, indexes and updates are equal, return 0
        System.err.println("Warning: " + this.getClass().getName() + ".compareTo():"
                + " return 0. Is it normal?");
        return 0;
    }

    /*
     * Getters & setters
     */

    public Integer getRound() {
        return round;
    }

    public int getIndex() {
        return index;
    }

    public Update getUpdate() {
        return update;
    }

}
