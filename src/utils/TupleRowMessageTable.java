package utils;

// Used by gossip abstract to temporarily store messages before database insert
public class TupleRowMessageTable {
    private final long source;
    private final long messageId;
    private final long sender;
    private final long receiver;
    private final boolean senderPrimary;
    private final boolean receiverPrimary;
    private final long roundSent;
    private final long hop;


    public TupleRowMessageTable(long source, long messageId,
            long from, long to,
            boolean fromPrimary, boolean toPrimary,
            long roundSent, long hop) {
        this.source = source;
        this.messageId = messageId;
        this.sender = from;
        this.receiver = to;
        this.senderPrimary = fromPrimary;
        this.receiverPrimary = toPrimary;
        this.roundSent = roundSent;
        this.hop = hop;
    }

    /*
     * Getters & setters
     */

    public long getSource() {
        return source;
    }

    public long getMessageId() {
        return messageId;
    }

    public long getSender() {
        return sender;
    }

    public long getReceiver() {
        return receiver;
    }

    public boolean isSenderPrimary() {
        return senderPrimary;
    }

    public boolean isReceiverPrimary() {
        return receiverPrimary;
    }

    public long getRoundSent() {
        return roundSent;
    }

    public long getHop() {
        return hop;
    }

}
