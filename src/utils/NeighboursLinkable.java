package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.core.Protocol;

public class NeighboursLinkable implements Protocol, Linkable {
    // Diversity goes in pair with (alpha, beta), it means there are some
    // neighbours that are not part of the same class as the majority
    public static final int TYPE_NONE = 0; // For temporary usage
    public static final int TYPE_UNIFORM = 1;
    public static final int TYPE_PRIMARY = 2;
    public static final int TYPE_SECONDARY = 3;
    public static final int TYPE_PRIMARY_WITH_DIVERSITY = 4;
    public static final int TYPE_SECONDARY_WITH_DIVERSITY = 5;
    public static final int TYPE_PRIMARY_SECONDARY = 6;

    private List<Node> neighbours;
    private int type;


    public NeighboursLinkable(String prefix) {
        this.neighbours = factoryNeighboursEmpty();
        this.type = NeighboursLinkable.TYPE_NONE;
    }

    // Constructor by copy
    public NeighboursLinkable(NeighboursLinkable dolly) {
        this.neighbours = factoryNeighbours(dolly.neighbours);
        this.type = dolly.type;
    }

    // Redefine clone() to have proper collection cloning and no exception
    @Override
    public Object clone() {
        return new NeighboursLinkable(this);
    }

    public void shuffle() {
        Collections.shuffle(neighbours, CommonState.r);
    }

    /*
     * Factories
     */

    private List<Node> factoryNeighbours(List<Node> dolly) {
        return new ArrayList<>(dolly);
    }

    private List<Node> factoryNeighboursEmpty() {
        return factoryNeighbours(Collections.<Node> emptyList());
    }

    public void resetNeighbours() {
        neighbours = factoryNeighboursEmpty();
    }

    /*
     * Implementation of interfaces
     */

    @Override
    public void onKill() {
        neighbours = null;
    }

    @Override
    public boolean addNeighbor(Node neighbour) {
        return neighbours.add(neighbour);
    }

    @Override
    public boolean contains(Node neighbour) {
        return neighbours.contains(neighbour);
    }

    @Override
    public int degree() {
        return getSize();
    }

    @Override
    public Node getNeighbor(int i) {
        return neighbours.get(i);
    }

    @Override
    public void pack() {
        System.err.println("Warning: " + this.getClass().getName() + ".pack()"
                + " called. Why?.");
    }

    /*
     * Getters & setters
     */

    // Rename degree() -> getSize(), more natural in our case
    public int getSize() {
        return neighbours.size();
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
