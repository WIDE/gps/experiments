package utils;

import java.util.Locale;

public class Global {
    // Locale.US is used to enforce "." as the decimal separator
    private static final Locale LOCALE = Locale.US;

    // String.Format using Locale.US
    public static String format(String format, Object... args) {
        return String.format(getLocale(), format, args);
    }

    /*
     * Getters & setters
     */

    private static Locale getLocale() {
        return LOCALE;
    }

}
