# GPS/UPS simulation code

This repository contains the [PeerSim](http://peersim.sourceforge.net/) code, the helper scripts and the results used in the Gossip Primary-Secondary paper [[hal]](https://hal.inria.fr/hal-01344138).


## Preparation

The java libraries for PeerSim 1.0.5 are in `lib/`.
The helper scripts written in python3 need the [pandas](http://pandas.pydata.org/pandas-docs/stable/install.html#installing-pandas) library to aggregate the results.

Datasets must be decompressed to run the blockchain simulations:
```
xz -dk blockchain_datasets/datasets/fc21_kiffer_latency.jsonl.xz
```

## Execution

To reproduce the plots used in the paper:

1. copy the configuration files `*.cfg` from the latest results directory (i.e., `results/016` and `results/017`) into `${resDir}`
2. re-run the simulator with these config files
```
bin/02_run_all.py --compile ${resDir}/*cfg
```
3. extract and aggregate the informations
```
bin/03_post_runs.sh ${resDir}
```
4. re-generate the plots in the [git repository containing the plots](https://gitlab.inria.fr/WIDE/gps/plots)

**Blockchain inconsistencies**: the average inconsistency levels reported for Fig 14 require manually setting the threshold of ignored levels (to not account for values too close to 0) in function `bin/merge.py:computeBlockchainEvalStats()`.

**RAM consideration**: round-based simulations performed with a network size of 1 million nodes take up to 20 GB of RAM per simulation/thread and up to 50 GB for blockchain scenarios with a low block frequency. You can choose the number of threads to use in `bin/02_run_all.py`.

Alternatively you can use a smaller network size; a simulation with 10k nodes takes less than 10 seconds.

**Debugging** can done via an already generated config file:
```
bin/compile.sh && bin/run.sh debug.cfg
```


## Files

* `bin/` contains the helper scripts written in bash and python3 that can compile the code, run the simulator in parallel, aggregate the results and generate the plots
* `lib/` contains the jar used by PeerSim (alongside a now unused sqlite library)
* `results/` contains the results and plots obtained throughout this project life
* `src/` contains the code run on the PeerSim framework


## Figures

See this [git repository](https://gitlab.inria.fr/WIDE/gps/plots) to reproduce the plots.


## Code architecture

The code is separated in four packages:

* `applicationLayer` contains the message queues implementing Update Consistency, `UCInfinity` is used in this paper since it is the simplest UC algorithm and since memory is not an issue for our experiments
* `blockchain` contains dataset sampling classes necessary for the blockchain usecase
* `controls` mainly contains the initializers and observers
* `gossip` contains the network layer of the simulation with three main classes:
    * `UniformGossip` is a classic Infect & Die gossip, it serves as our baseline
    * `PriorityGossipDuplicate` is our contribution protocol, it uses the number of duplica of a message *m* received by *n* to decide which class of nodes *n* should gossip *m* to
    * `PriorityGossipHop` is an unused protocol in the paper, it uses the number of hop of a message to determine which class of node should receive it
* `utils` contains various classes that do not fit in the three other categories

**Note**: in PeerSim, only the network layer can generate events; whereas for our experiments, we want the application layer to generate events (to simulate user inputs). To do so, the generated network events are passed to the application layer as if they were user inputs, and the reception of such events by the application triggers the broadcasting process of the updates, which starts the simulation.
